﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using Esmax.Models;
using Esmax.Models.MplanAccion;

namespace Esmax.Controllers.PlanAccion
{
    public class PA
    {
        public string accion { get; set; }
        public string responsable_id { get; set; }
        public int cumplimiento { get; set; }
    }

    public class paUser
    {
        public int plan_accion_id { get; set; }
        public string accion { get; set; }
        public int modulo { get; set; }
        public DateTime cumplimiento { get; set; }
        public string estado { get; set; }
        public string UserName { get; set; }
    }

    public class datos
    {
        public int modulo { get; set; }
        public int total { get; set; }
    }
    
    public class datos2
    {
        public string UserName { get; set; }
        public int total { get; set; }
    }
    public class imagenes
    {
        public HttpPostedFileBase files { get; set; }
    }
    public class MplanAccionController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        // GET: MplanAccion
        [Authorize]
        public ActionResult ingresarPlanAccion()
        {
            return View("ingresarPlanAccion", "_Layout");
        }

        [HttpPost]
        [Authorize]
        public JsonResult addPlanAccion(PA planAccion, List<TareasPlanAccion> tareas)
        {
            var plan = db.PlanAccion.Add(new Models.PlanAccion
            {
                accion = planAccion.accion,
                responsable_id = planAccion.responsable_id,
                cumplimiento = DateTime.Now.AddDays(Convert.ToDouble(planAccion.cumplimiento)),
                created_at = DateTime.Now,
                modulo = 3
            });
            

            foreach (var tarea in tareas)
            {
                db.TareasPlanAccions.Add(new TareasPlanAccion
                {
                    tareas_descripcion = tarea.tareas_descripcion,
                    plan_accion_id = plan.plan_accion_id,
                    created_at = DateTime.Now
                });
                
            }
            db.SaveChanges();
            
            db.Notificaciones.Add(new Notificaciones
            {
                descripcion = "Tienes un nuevo plan de acción. ''" + plan.accion + "''",
                modulo = 3,
                plan_accion_id = plan.plan_accion_id,
                UserId = plan.responsable_id,
                created_at = DateTime.Now
            });
            db.SaveChanges();

            var user = db.Users.Where(x => x.Id == planAccion.responsable_id).FirstOrDefault();
            
            try
            {
                MailMessage correo = new MailMessage();
                correo.From = new MailAddress("sistemapaso@esmax.cl"); //correo que usara la app
                correo.To.Add(user.Email);
                correo.Subject = "Tienes un nuevo plan de accion asociado";
                correo.Body = user.UserName + ", <br/><br/> Tiene un nuevo plan de acción, Número de folio: " + plan.plan_accion_id + " <br/><br/> Haz click aqui para acceder: http://paso.esmax.cl/MplanAccion/ver/"+plan.plan_accion_id+ "<br><br><br> Por favor no responder este correo.";
                correo.IsBodyHtml = true;
                correo.Priority = MailPriority.Normal;

                SmtpClient smtp = new SmtpClient();
                smtp.Host = "smtp.esmax.cl";
                smtp.Port = 25;
                smtp.EnableSsl = true;
                smtp.UseDefaultCredentials = true;
                string sCuentaCorreo = "sistemapaso@esmax.cl";
                string sPasswordCorreo = "";
                smtp.Credentials = new System.Net.NetworkCredential(sCuentaCorreo, sPasswordCorreo);

                smtp.Send(correo);
                ViewBag.Mensaje = "Mensaje enviado correctamente";
            }
            catch (Exception e)
            {
                ViewBag.Error = e.Message;
            }
            
            return Json("saved");
        }

        [Authorize]
        public ActionResult revisar()
        {
            return View("revisar", "_Layout");
        }

        [HttpPost]
        [Authorize]
        public JsonResult getPlanAccion(string id)
        {
            db.Configuration.ProxyCreationEnabled = false;
            List<Models.PlanAccion> lista = db.PlanAccion.Where(x => x.responsable_id == id).Where( e => e.estado == "habilitado").ToList();
            return Json(lista);
        }
        
        
        [HttpPost]
        [Authorize]
        public JsonResult cerrarPlanAccion(int id, string descripcion)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var planAccion = db.PlanAccion.Where(x => x.plan_accion_id == id).FirstOrDefault();
            planAccion.estado = "cerrado";
            planAccion.descripcionCierre = descripcion;
            db.SaveChanges();
            
            return Json("plan de accion cerrado");
        }
        
        [HttpGet]
        [Authorize]
        public ActionResult GraficosPlanAccion()
        {
            return View("Graficos", "_Layout");
        }

        
        [HttpPost]
        [Authorize]
        public JsonResult getTareas(int id)
        {
            db.Configuration.ProxyCreationEnabled = false;
            List<TareasPlanAccion> lista = db.TareasPlanAccions.Where(x => x.plan_accion_id == id).ToList();
            return Json(lista);
        }
        
        
        [HttpPost]
        [Authorize]
        public JsonResult getModulos(string desde, string hasta)
        {
            List<datos> lst = new List<datos>();
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                string query = @"select modulo, COUNT(modulo) as total  from PlanAccions  where created_at >= @desde and created_at <= @hasta group by modulo"; 
                SqlCommand com = new SqlCommand(query, con);
                com.Parameters.AddWithValue("@desde", desde);
                com.Parameters.AddWithValue("@hasta", hasta);
                SqlDataReader rdr = com.ExecuteReader();
                while (rdr.Read())
                {
                    lst.Add(new datos
                    {
                        modulo = Convert.ToInt32(rdr["modulo"] is DBNull ? 0: rdr["modulo"]),
                        total = Convert.ToInt32(rdr["total"] is DBNull ? 0: rdr["total"])
                        
                    });
                }
                return Json(lst);
            } 
        }
        
        [HttpPost]
        [Authorize]
        public JsonResult getUsuariosResponsables(string desde, string hasta)
        {
            List<datos2> lst = new List<datos2>();
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                string query = @"select top 5 ApplicationUsers.UserName, COUNT(PlanAccions.responsable_id) as total from PlanAccions inner join ApplicationUsers ON PlanAccions.responsable_id = ApplicationUsers.Id where created_at >= @desde and created_at <= @hasta group by UserName order by total desc"; 
                SqlCommand com = new SqlCommand(query, con);
                com.Parameters.AddWithValue("@desde", desde);
                com.Parameters.AddWithValue("@hasta", hasta);
                SqlDataReader rdr = com.ExecuteReader();
                while (rdr.Read())
                {
                    lst.Add(new datos2
                    {
                        UserName = Convert.ToString(rdr["UserName"] is DBNull ? 0: rdr["UserName"]),
                        total = Convert.ToInt32(rdr["total"] is DBNull ? 0: rdr["total"])
                        
                    });
                }
                return Json(lst);
            } 
        }
        
        [HttpPost]
        [Authorize]
        public JsonResult getUsuariosResponsablesHabilitados(string desde, string hasta)
        {
            List<datos2> lst = new List<datos2>();
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                string query = @"select top 5 ApplicationUsers.UserName, COUNT(PlanAccions.responsable_id) as total from PlanAccions inner join ApplicationUsers ON PlanAccions.responsable_id = ApplicationUsers.Id where created_at >= @desde and created_at <= @hasta and PlanAccions.estado = @estado group by UserName order by total desc"; 
                SqlCommand com = new SqlCommand(query, con);
                com.Parameters.AddWithValue("@desde", desde);
                com.Parameters.AddWithValue("@hasta", hasta);
                com.Parameters.AddWithValue("@estado", "habilitado");
                SqlDataReader rdr = com.ExecuteReader();
                while (rdr.Read())
                {
                    lst.Add(new datos2
                    {
                        UserName = Convert.ToString(rdr["UserName"] is DBNull ? 0: rdr["UserName"]),
                        total = Convert.ToInt32(rdr["total"] is DBNull ? 0: rdr["total"])
                        
                    });
                }
                return Json(lst);
            } 
        }
        
        [HttpPost]
        [Authorize]
        public JsonResult getUsuariosResponsablesCerrados(string desde, string hasta)
        {
            List<datos2> lst = new List<datos2>();
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                string query = @"select top 5 ApplicationUsers.UserName, COUNT(PlanAccions.responsable_id) as total from PlanAccions inner join ApplicationUsers ON PlanAccions.responsable_id = ApplicationUsers.Id where created_at >= @desde and created_at <= @hasta and PlanAccions.estado = @estado group by UserName order by total desc"; 
                SqlCommand com = new SqlCommand(query, con);
                com.Parameters.AddWithValue("@desde", desde);
                com.Parameters.AddWithValue("@hasta", hasta);
                com.Parameters.AddWithValue("@estado", "cerrado");
                SqlDataReader rdr = com.ExecuteReader();
                while (rdr.Read())
                {
                    lst.Add(new datos2
                    {
                        UserName = Convert.ToString(rdr["UserName"] is DBNull ? 0: rdr["UserName"]),
                        total = Convert.ToInt32(rdr["total"] is DBNull ? 0: rdr["total"])
                        
                    });
                }
                return Json(lst);
            } 
        }
        
        string cs = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        
        [HttpGet]
        [Authorize]
        public JsonResult allPlanAccion()
        {
            List<Models.PlanAccion> lst = new List<Models.PlanAccion>();
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                string query1 = @"SELECT * FROM PlanAccions"; 
                SqlCommand com = new SqlCommand(query1, con);
                SqlDataReader rdr = com.ExecuteReader();
                while (rdr.Read())
                {
                    lst.Add(new Models.PlanAccion
                    {
                        plan_accion_id = Convert.ToInt32(rdr["plan_accion_id"] is DBNull ? 0: rdr["plan_accion_id"]),
                        NivelesOdts_id = Convert.ToInt32(rdr["NivelesOdts_id"] is DBNull ? 0: rdr["NivelesOdts_id"]),
                        causa_raiz_id = Convert.ToInt32(rdr["causa_raiz_id"] is DBNull ? 0: rdr["causa_raiz_id"]),
                        accion = Convert.ToString(rdr["accion"] is DBNull ? 0: rdr["accion"]),
                        responsable_id = Convert.ToString(rdr["responsable_id"] is DBNull ? 0: rdr["responsable_id"]),
                        respuesta_odt_id = Convert.ToInt32(rdr["respuesta_odt_id"] is DBNull ? 0: rdr["respuesta_odt_id"]),
                        modulo = Convert.ToInt32(rdr["modulo"] is DBNull ? 0: rdr["modulo"]),
                        cumplimiento = Convert.ToDateTime(rdr["cumplimiento"] is DBNull ? 0: rdr["cumplimiento"]),
                        created_at = Convert.ToDateTime(rdr["created_at"] is DBNull ? 0: rdr["created_at"]),
                        estado = Convert.ToString(rdr["estado"] is DBNull ? 0: rdr["estado"])
                        
                    });
                }
                return Json(lst, JsonRequestBehavior.AllowGet);
            } 
        }

        [HttpGet]
        [Authorize]
        public JsonResult allPlanAccionMoreUser()
        {
            List<paUser> lst = new List<paUser>();
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                string query1 = @"SELECT plan_accion_id, accion, modulo, cumplimiento, PlanAccions.estado, ApplicationUsers.UserName FROM PlanAccions inner join ApplicationUsers On PlanAccions.responsable_id = ApplicationUsers.Id order by responsable_id";
                SqlCommand com = new SqlCommand(query1, con);
                SqlDataReader rdr = com.ExecuteReader();
                while (rdr.Read())
                {
                    lst.Add(new paUser
                    {
                        plan_accion_id = Convert.ToInt32(rdr["plan_accion_id"] is DBNull ? 0: rdr["plan_accion_id"]),
                        accion = Convert.ToString(rdr["accion"] is DBNull ? 0: rdr["accion"]),
                        modulo = Convert.ToInt32(rdr["modulo"] is DBNull ? 0: rdr["modulo"]),
                        cumplimiento = Convert.ToDateTime(rdr["cumplimiento"] is DBNull ? 0: rdr["cumplimiento"]),
                        estado = Convert.ToString(rdr["estado"] is DBNull ? 0: rdr["estado"]),
                        UserName = Convert.ToString(rdr["UserName"] is DBNull ? 0: rdr["UserName"])

                    });
                }
                return Json(lst, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [Authorize]
        public JsonResult allPlanAccionWithQueryMoreUser(string query)
        {
            List<paUser> lst = new List<paUser>();
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                string query1 = @"SELECT plan_accion_id, accion, modulo, cumplimiento, PlanAccions.estado, ApplicationUsers.UserName FROM PlanAccions inner join ApplicationUsers On PlanAccions.responsable_id = ApplicationUsers.Id " + query + " order by responsable_id";
                SqlCommand com = new SqlCommand(query1, con);
                SqlDataReader rdr = com.ExecuteReader();
                while (rdr.Read())
                {
                    lst.Add(new paUser
                    {
                        plan_accion_id = Convert.ToInt32(rdr["plan_accion_id"] is DBNull ? 0: rdr["plan_accion_id"]),
                        accion = Convert.ToString(rdr["accion"] is DBNull ? 0: rdr["accion"]),
                        modulo = Convert.ToInt32(rdr["modulo"] is DBNull ? 0: rdr["modulo"]),
                        cumplimiento = Convert.ToDateTime(rdr["cumplimiento"] is DBNull ? 0: rdr["cumplimiento"]),
                        estado = Convert.ToString(rdr["estado"] is DBNull ? 0: rdr["estado"]),
                        UserName = Convert.ToString(rdr["UserName"] is DBNull ? 0: rdr["UserName"])
                    });
                }
                return Json(lst);
            }
        }
        
        [HttpPost]
        [Authorize]
        public JsonResult allPlanAccionWithQuery(string query)
        {
            List<Models.PlanAccion> lst = new List<Models.PlanAccion>();
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                string query1 = @"SELECT * FROM PlanAccions " + query + " order by estado desc"; 
                SqlCommand com = new SqlCommand(query1, con);
                SqlDataReader rdr = com.ExecuteReader();
                while (rdr.Read())
                {
                    lst.Add(new Models.PlanAccion
                    {
                        plan_accion_id = Convert.ToInt32(rdr["plan_accion_id"] is DBNull ? 0: rdr["plan_accion_id"]),
                        NivelesOdts_id = Convert.ToInt32(rdr["NivelesOdts_id"] is DBNull ? 0: rdr["NivelesOdts_id"]),
                        causa_raiz_id = Convert.ToInt32(rdr["causa_raiz_id"] is DBNull ? 0: rdr["causa_raiz_id"]),
                        accion = Convert.ToString(rdr["accion"] is DBNull ? 0: rdr["accion"]),
                        responsable_id = Convert.ToString(rdr["responsable_id"] is DBNull ? 0: rdr["responsable_id"]),
                        respuesta_odt_id = Convert.ToInt32(rdr["respuesta_odt_id"] is DBNull ? 0: rdr["respuesta_odt_id"]),
                        modulo = Convert.ToInt32(rdr["modulo"] is DBNull ? 0: rdr["modulo"]),
                        cumplimiento = Convert.ToDateTime(rdr["cumplimiento"] is DBNull ? 0: rdr["cumplimiento"]),
                        created_at = Convert.ToDateTime(rdr["created_at"] is DBNull ? 0: rdr["created_at"]),
                        estado = Convert.ToString(rdr["estado"] is DBNull ? 0: rdr["estado"])
                        
                    });
                }
                return Json(lst);
            } 
        }
        
        
        [HttpPost]
        [Authorize]
        public JsonResult UploadEvidence()
        {
            var id1 = Request.Params["id"];
            var idPlan1 = Request.Params["idPlan"];
            var texto = Request.Params["descripcion"];
            var id = Convert.ToInt32(id1);
            var idPlan = Convert.ToInt32(idPlan1);
            var count = Request.Files.AllKeys.Length;
            string path = Server.MapPath("~/Content/Images/");
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            
            

            if (id != 0)
            {
                var datos = db.DatosEvidencia.Add(new DatosEvidencia
                {
                    tareas_id = id,
                    Descripcion = texto,
                    created_at = DateTime.Now,
                });
                db.SaveChanges();
                for (int i = 0; i < count; i++)
                {
                    var data = Request.Files["file" + i];
                    var time = DateTime.Now.Hour + "" + DateTime.Now.Minute + "" + DateTime.Now.Second;
                    var nombre = time + data.FileName;
                    data.SaveAs(path + Path.GetFileName(nombre));
                    db.EvidenciaTareas.Add(new EvidenciaTarea
                    {
                        archivo = nombre,
                        datosEvidenciaId = datos.datosEvidenciaId,
                        created_at = DateTime.Now,
                    });
                    db.SaveChanges();
                }

                
                var tarea = db.TareasPlanAccions.Where(x => x.tareas_id == id).FirstOrDefault();
                tarea.estado = "cerrado";
                db.SaveChanges();
            }
            else
            {
                var datos = db.DatosEvidencia.Add(new DatosEvidencia
                {
                    plan_accion_id = idPlan,
                    Descripcion = texto,
                    created_at = DateTime.Now,
                });
                db.SaveChanges();

                for (int i = 0; i < count; i++)
                {
                    var data = Request.Files["file" + i];
                    var time = DateTime.Now.Hour + "" + DateTime.Now.Minute + "" + DateTime.Now.Second;
                    var nombre = time + data.FileName;
                    data.SaveAs(path + Path.GetFileName(nombre));
                    db.EvidenciaTareas.Add(new EvidenciaTarea
                    {
                        archivo = nombre,
                        datosEvidenciaId = datos.datosEvidenciaId,
                        created_at = DateTime.Now,
                    });
                    db.SaveChanges();
                } 
            }
            
            
            return Json("ok");
        }
        
        [HttpGet]
        [Authorize]
        public ActionResult ver(int id)
        {
            ViewBag.planAccionId = id;
            return View("ver","_Layout");
        }
        
        [HttpGet]
        [Authorize]
        public ActionResult verCerrado(int id)
        {
            ViewBag.planAccionId = id;
            return View("verCerrado","_Layout");
        }
        
        [HttpPost]
        [Authorize]
        public JsonResult findPlanAccion(int id)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var lista = db.PlanAccion.Where(x => x.plan_accion_id == id).FirstOrDefault();
            return Json(lista);
        }
        [HttpPost]
        [Authorize]
        public JsonResult findPlanAccion_report(int id)
        {

            db.Configuration.ProxyCreationEnabled = false;
            var pa_odt = db.PlanAccion.Where(x => x.plan_accion_id == id).FirstOrDefault();
            return Json(pa_odt);

        }
        
        [HttpPost]
        [Authorize]
        public JsonResult findEvidencia(int plan_id)
        {

            db.Configuration.ProxyCreationEnabled = false;
        
            var evidencia = db.DatosEvidencia.Where( u => u.plan_accion_id == plan_id).FirstOrDefault();
            return Json(evidencia);

        }
        
        [HttpPost]
        [Authorize]
        public JsonResult findEvidenciaFotos(int evidencia_id)
        {

            db.Configuration.ProxyCreationEnabled = false;
        
            var evidencia = db.EvidenciaTareas.Where( u => u.datosEvidenciaId == evidencia_id).ToList();
            return Json(evidencia);

        }
        
        [HttpGet]
        [Authorize]
        public FileResult download(int id)
        {
            var file = db.EvidenciaTareas.Where(x => x.EvidenciaTareaId == id).FirstOrDefault();
            var FileVirtualPath = "~/Content/Images/" + file.archivo;
            return File(FileVirtualPath, "application/force- download", Path.GetFileName(FileVirtualPath));
        }
        
        [HttpPost]
        [Authorize]
        public JsonResult findDatosEvidencia(int tarea_id)
        {

            db.Configuration.ProxyCreationEnabled = false;
        
            var evidencia = db.DatosEvidencia.Where( u => u.tareas_id == tarea_id).FirstOrDefault();
            return Json(evidencia);

        }
        
        [HttpPost]
        [Authorize]
        public JsonResult findCausa(int id)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var causa = db.CausaRaiz.Where(x => x.causa_raiz_id == id).FirstOrDefault();
            return Json(causa);
        }
        
        [HttpGet]
        [Authorize]
        public ActionResult calendar()
        {
            
            return View("Calendar","_Layout");
        }

        [HttpPost]
        [Authorize]
        public JsonResult Update(int id, string accion, string responsable)
        {
            var planAccion = db.PlanAccion.Where(x => x.plan_accion_id == id).FirstOrDefault();
            planAccion.accion = accion;
            planAccion.responsable_id = responsable;
            db.SaveChanges();
            return Json("saved");
        }
        
        [HttpPost]
        [Authorize]
        public JsonResult Update2(int id, string accion, string responsable)
        {
            var planAccion = db.PlanAccion.Where(x => x.plan_accion_id == id).FirstOrDefault();
            planAccion.accion = accion;
            planAccion.responsable_id = responsable;
            db.SaveChanges();
            return Json("saved");
        }

        [HttpPost]
        [Authorize]
        public ActionResult Uploads(List<HttpPostedFileBase> files)
        {
            List<HttpPostedFileBase> file2 = new List<HttpPostedFileBase> {Request.Files["files"]};
            return Json("saved");
        }

    }
}