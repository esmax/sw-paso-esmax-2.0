using System.Linq;
using System.Web.Mvc;
using Esmax.Models;

namespace Esmax.Controllers
{
    public class AdminController : Controller
    {
        
        private ApplicationDbContext db = new ApplicationDbContext();
        // GET
        [Authorize]
        public ActionResult Registrar()
        {
            ViewBag.result = TempData["result"];
            return View("Registrar", "_Layout");
        }

        [Authorize]
        public ActionResult Modificar()
        {
            ViewBag.result = TempData["result"];
            return View("Modificar", "_Layout");
        }
        
        [Authorize]
        public ActionResult Gestionar()
        {
            ViewBag.result = TempData["result"];
            return View("Gestionar", "_Layout");
        }
        
        [HttpGet]
        [Authorize]
        public ActionResult findUser(string id)
        {
            //var user = db.Users.Where(u => u.Id == id);
            ViewBag.userId1 = id;
            return View("Modificar", "_Layout");
        }
        
        [HttpPost]
        [Authorize]
        public JsonResult getuser(string id)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var user = db.Users.Where(u => u.Id == id).FirstOrDefault();

            return Json(user);
        }

        [HttpPost]
        [Authorize]
        public JsonResult ActualizarEstado(string id)
        {
            var user = db.Users.Where(x => x.Id == id).FirstOrDefault();
            if (user.Estado == "habilitado")
            {
                user.Estado = "deshabilitado";
                db.SaveChanges();

            }
            else
                user.Estado = "habilitado";
            db.SaveChanges();
            return Json("Updated");
        }
        [Authorize]
        public ActionResult redirigirMod(){
            return View("Gestionar", "_Layout");
        }
    }
    
}