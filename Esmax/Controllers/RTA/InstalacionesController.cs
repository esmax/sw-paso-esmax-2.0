using System.Web.Helpers;
using System.Web.Mvc;
using Esmax.Models;
using System.Linq;

namespace Esmax.Controllers.RTA
{
    public class InstalacionesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        
        [HttpGet]
        [Authorize]
        public ActionResult GetInstalacion()
        {
            db.Configuration.ProxyCreationEnabled = false;
            var instalaciones = db.Instalaciones.ToList();
            return Json(instalaciones, JsonRequestBehavior.AllowGet);
        } 
    }
}