using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using Esmax.Models;
using Esmax.Models.RTA;
using Microsoft.AspNet.Identity;

namespace Esmax.Controllers.RTA
{
    public class RTAController : Controller
    {
        // GET
        [Authorize]
        public ActionResult CrearRTA()
        {
            return View("CrearRTA", "_Layout");
        }

        [Authorize]
        public ActionResult RevisarRTA()
        {
            return View("RevisarRTA", "_Layout");
        }

        private ApplicationDbContext db = new ApplicationDbContext();

        [Authorize]
        public ActionResult allrta()
        {
            db.Configuration.ProxyCreationEnabled = false;
            List<RespuestaRTA> lista = db.RespuestaRta.ToList();
            return Json(lista, JsonRequestBehavior.AllowGet);
        }

        //TRAE RESPUESTA RTA SEGUN ID
        [HttpPost]
        [Authorize]
        public JsonResult buscarrta(int id)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var rta = db.RespuestaRta.Where(x => x.id_rta == id).FirstOrDefault();
            return Json(rta);
        }

        //TRAE ETAPA 1 SEGUN ID
        [HttpPost]
        [Authorize]
        public JsonResult buscaretapa1(int id)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var etapa1 = db.etapa1.Where(x => x.etapa1_id == id).FirstOrDefault();
            return Json(etapa1);
        }
        string cs = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        [HttpPost]
        [Authorize]
        public ActionResult allrtaWithQuery(string query)
        {
            List<RespuestaRTA> lst = new List<RespuestaRTA>();
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                string query1 = @"SELECT * FROM RespuestaRTA " + query;
                SqlCommand com = new SqlCommand(query1, con);
                SqlDataReader rdr = com.ExecuteReader();
                while (rdr.Read())
                {
                    lst.Add(new RespuestaRTA
                    {
                        id_rta = Convert.ToInt32(rdr["id_rta"] is DBNull ? 0 : rdr["id_rta"]),
                        etapa1_id = Convert.ToInt32(rdr["etapa1_id"] is DBNull ? 0 : rdr["etapa1_id"]),
                        etapa2_id = Convert.ToInt32(rdr["etapa2_id"] is DBNull ? 0 : rdr["etapa2_id"]),
                        etapa3_id = Convert.ToInt32(rdr["etapa3_id"] is DBNull ? 0 : rdr["etapa3_id"]),
                        etapa4_id = Convert.ToInt32(rdr["etapa4_id"] is DBNull ? 0 : rdr["etapa4_id"]),
                        etapa5_id = Convert.ToInt32(rdr["etapa5_id"] is DBNull ? 0 : rdr["etapa5_id"]),
                        etapa6_id = Convert.ToInt32(rdr["etapa6_id"] is DBNull ? 0 : rdr["etapa6_id"]),
                        responsable_actual =
                            Convert.ToString(rdr["responsable_actual"] is DBNull ? 0 : rdr["responsable_actual"])
                    });
                }
                return Json(lst);
            }
        }
        [HttpGet]
        [Authorize]
        public ActionResult responderRTA(int id)
        {
            ViewBag.RTAId = id;
            ViewBag.respuesta = 1;
            return View("CrearRTA", "_Layout");
        }

        [HttpGet]
        [Authorize]
        public ActionResult verRTA(int id)
        {
            ViewBag.RTAId = id;
            ViewBag.respuesta = 0;
            return View("CrearRTA", "_Layout");
        }
        [HttpPost]
        [Authorize]
        public ActionResult Etapa1()
        {
            int responsable1_id = Convert.ToInt32(Request.Params["responsable1_id"]);
            string responsable1 = Request.Params["responsable1"];
            //Niveles organizacionales
            int holdingId = Convert.ToInt32(Request.Params["holdingId"]);
            string nombre_holding = Request.Params["nombre_holding"];
            int negocioId = Convert.ToInt32(Request.Params["negocioId"]);
            string nombre_negocio = Request.Params["nombre_negocio"];
            int gerenciaId = Convert.ToInt32(Request.Params["gerenciaId"]);
            string nombre_gerencia = Request.Params["nombre_gerencia"];
            int areaId = Convert.ToInt32(Request.Params["areaId"]);
            string nombre_area = Request.Params["nombre_area"];
            //Clasificaciones
            string identificado = Request.Params["identificado"];
            string clasificacion = Request.Params["clasificacion"];
            string tipo = Request.Params["tipo"];
//            string afeccion = Request.Params["afeccion"];
            int medio_ambiente= Convert.ToInt32(Request.Params["medio_ambiente"]);
            int personas= Convert.ToInt32(Request.Params["personas"]);
            int patrimonio= Convert.ToInt32(Request.Params["patrimonio"]);
            int proceso_producto= Convert.ToInt32(Request.Params["proceso_producto"]);
            string clase = Request.Params["clase"];
            //FECHA HORA Y LUGAR
            DateTime fecharta = Convert.ToDateTime(Request.Params["fecharta"]);
            string horarta = Request.Params["horarta"];
            /*int instalacionId = Convert.ToInt32(Request.Params["instalacionId"]);
            string nombre_instalacion = Request.Params["nombre_instalacion"];*/
            string lugar = Request.Params["lugar"];
            //Accidente
            int accidente = Convert.ToInt32(Request.Params["accidente"]);
            //Descripcion
            string descripcion = Request.Params["descripcion"];
            //Accion inmediata
            string acc_inme = Request.Params["acc_inme"];
            //Responsable 2
            int responsable2 = Convert.ToInt32(Request.Params["responsable2"]);
            string nombre_responsable2 = Request.Params["nombre_responsable2"];

            var count = Request.Files.AllKeys.Length;
            Nullable<int> vehiculoId = null;
            string tipo_vehiculo = null;
            if (accidente == 1)
            {
               vehiculoId = Convert.ToInt32(Request.Params["vehiculoId"]);
               tipo_vehiculo = Request.Params["tipo_vehiculo"];
            }
            var e1 = db.etapa1.Add(new etapa1
            {
                responsable1_id = responsable1_id,
                responsable1 = responsable1,
                holding_id = holdingId,
                holding_nombre = nombre_holding,
                negocio_id = negocioId,
                negocio_nombre = nombre_negocio,
                gerencia_id = gerenciaId,
                gerencia_nombre = nombre_gerencia,
                area_id = areaId,
                area_nombre = nombre_area,
                identificado_como = identificado,
                clasificacion = clasificacion,
                tipo = tipo,
//                afeccion_anomalia = afeccion,
                medio_ambiente = medio_ambiente,
                personas = personas,
                patrimonio = patrimonio,
                procesos_productos = proceso_producto,
                clase = clase,
                fecha_etapa1 = fecharta,
                hora_etapa1 = horarta,
                /*instalacion_id = instalacionId,
                instalacion_nombre = nombre_instalacion,*/
                lugar = lugar,
                accidente = accidente,
                vehiculo_id = vehiculoId,
                vehiculo_tipo = tipo_vehiculo,
                descripcion = descripcion,
                accion_inmediata = acc_inme,
                responsable2_id = responsable2,
                responsable2_nombre = nombre_responsable2,
                created_at = DateTime.Now,
            });
            db.SaveChanges();

            string path = Server.MapPath("~/Content/Archivos/RTA/");
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            for (int i = 0; i < count; i++)
            {
                var data = Request.Files["file" + i];
                var time = DateTime.Now.Hour + "" + DateTime.Now.Minute + "" + DateTime.Now.Second;
                var nombre = time + data.FileName;
                data.SaveAs(path + Path.GetFileName(nombre));
                db.ArchivoRta.Add(new ArchivoRTA
                {
                    id_etapa1 = e1.etapa1_id,
                    nombre_archivo = nombre,
                    created_at = DateTime.Now,
                });
                db.SaveChanges();
            }
            var respuesta = db.RespuestaRta.Add(new RespuestaRTA
            {
                etapa1_id = e1.etapa1_id,
                responsable_actual = nombre_responsable2,
                responsable_actual_id = responsable2,
                etapa_actual = "Gravedad",
                estado = "Reportado",
                created_at = DateTime.Now,
            });
            db.SaveChanges();
            //Notificación
            db.Notificaciones.Add(new Notificaciones
            {
                descripcion = "Te han asignado un rta: 'Gravedad'",
                modulo = 5,
                cierre = 2,
                rta = respuesta.id_rta,
                UserId = responsable2.ToString(),
                created_at = DateTime.Now
            });
            db.SaveChanges();
            return Json("OK ETAPA1");
        }

        [HttpPost]
        [Authorize]
        public JsonResult getFilesRTA(int id)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var files = db.ArchivoRta.Where(x => x.id_etapa1 == id).ToList();
            return Json(files);
        }

        [HttpPost]
        [Authorize]
        public JsonResult getFilesRTA3(int id)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var files = db.ArchivoRta.Where(x => x.id_etapa3 == id).ToList();
            return Json(files);
        }

        [HttpGet]
        [Authorize]
        public FileResult downloadRTA(int id)
        {
            var file = db.ArchivoRta.Where(x => x.id_archivo == id).FirstOrDefault();
            var FileVirtualPath = "~/Content/Archivos/RTA/" + file.nombre_archivo;
            return File(FileVirtualPath, "application/force- download", Path.GetFileName(FileVirtualPath));
        }

        [HttpPost]
        [Authorize]
        public JsonResult Etapa2(etapa2 etapa2, List<comision> comision, int id)
        {
            var e2 = db.etapa2.Add(new etapa2
            {
//                gravedad = etapa2.gravedad,
                medio_ambiente_gravedad = etapa2.medio_ambiente_gravedad,
                patrimonio_gravedad = etapa2.patrimonio_gravedad,
                procesos_productos_gravedad = etapa2.procesos_productos_gravedad,
                personas_gravedad = etapa2.personas_gravedad,
                responsable3_id = etapa2.responsable3_id,
                responsable3_nombre = etapa2.responsable3_nombre,
                created_at = DateTime.Now,
            });
            db.SaveChanges();
            foreach (var com in comision)
            {
                db.comision.Add(new comision
                {
                    etapa2_id = e2.etapa2_id,
                    usuario_id = com.usuario_id,
                    usuario_nombre = com.usuario_nombre,
                    created_at = DateTime.Now,
                });
                db.SaveChanges();
            }

            var Rta = db.RespuestaRta.FirstOrDefault(x => x.id_rta == id);
            Rta.etapa2_id = e2.etapa2_id;
            Rta.responsable_actual_id = Convert.ToInt32(e2.responsable3_id);
            Rta.responsable_actual = e2.responsable3_nombre;
            Rta.etapa_actual = "An\xe1lisis de la anomal\U000000EDa";
            Rta.estado = "Investigado";
            db.SaveChanges();

            //Notificación
            db.Notificaciones.Add(new Notificaciones
            {
                descripcion = "Te han asignado un rta: 'An\xe1lisis de la anomal\U000000EDa'",
                modulo = 5,
                cierre = 2,
                rta = Rta.id_rta,
                UserId = etapa2.responsable3_id,
                created_at = DateTime.Now
            });
            db.SaveChanges();

            return Json("OK ETAPA2");
        }

        //TRAE ETAPA 2 SEGUN ID
        [HttpPost]
        [Authorize]
        public JsonResult buscaretapa2(int id)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var etapa2 = db.etapa2.Where(x => x.etapa2_id == id).FirstOrDefault();
            return Json(etapa2);
        }

        [HttpPost]
        [Authorize]
        public JsonResult getComision(int id)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var comision = db.comision.Where(x => x.etapa2_id == id).ToList();
            return Json(comision);
        }
        
        [HttpPost]
        [Authorize]
        public ActionResult Etapa3r(etapa3 etapa3, List<Causas_inmediatas> causa_inmediata1,
            List<Causas_basicas> causa_basica1,
            List<Acciones_corr_prev> accion_corr_prev1, int id)
        {
            
            var Rta = db.RespuestaRta.FirstOrDefault(x => x.id_rta == id);
            
            
            var e3r = db.etapa3.FirstOrDefault(x => x.etapa3_id == Rta.etapa3_id);
            e3r.tipo_metodologia = etapa3.tipo_metodologia;
            e3r.analisis = etapa3.analisis;
            e3r.responsable4_id = etapa3.responsable4_id;
            e3r.responsable4_nombre = etapa3.responsable4_nombre;
            e3r.created_at = DateTime.Now;
            e3r.aprobacion = "aprobado";
            db.SaveChanges();

            var deletes = db.Causasinmediatas.Where(x => x.etapa3_id == e3r.etapa3_id).ToList();
            foreach (var delete in deletes)
            {
                db.Causasinmediatas.Remove(delete);
                db.SaveChanges();
            }
            
            foreach (var cin in causa_inmediata1)
            {
                db.Causasinmediatas.Add(new Causas_inmediatas
                {
                    etapa3_id = e3r.etapa3_id,
                    tipo = cin.tipo,
                    Causa = cin.Causa,
                    descripcion = cin.descripcion,
                    created_at = DateTime.Now,
                });
                db.SaveChanges();
            }
            
            var deletes2 = db.CausasBasicas.Where(x => x.etapa3_id == e3r.etapa3_id).ToList();
            foreach (var delete2 in deletes2)
            {
                db.CausasBasicas.Remove(delete2);
                db.SaveChanges();
            }

            foreach (var cba in causa_basica1)
            {
                db.CausasBasicas.Add(new Causas_basicas
                {
                    etapa3_id = e3r.etapa3_id,
                    Tipo_causa_basica = cba.Tipo_causa_basica,
                    Causa = cba.Causa,
                    Descripcion = cba.Descripcion,
                    created_at = DateTime.Now,
                });
                db.SaveChanges();
            }
            var deletes3 = db.Acciones_corr_prev.Where(x => x.etapa3_id == e3r.etapa3_id).ToList();
            foreach (var delete3 in deletes3)
            {
                db.Acciones_corr_prev.Remove(delete3);
                db.SaveChanges();
            }
            foreach (var acp in accion_corr_prev1)
            {
                db.Acciones_corr_prev.Add(new Acciones_corr_prev
                {
                    etapa3_id = e3r.etapa3_id,
                    tipo_accion = acp.tipo_accion,
                    accion = acp.accion,
                    responsable_id = acp.responsable_id,
                    responsable_nombre = acp.responsable_nombre,
                    tipo_evidencia = acp.tipo_evidencia,
                    fecha_cumplimiento = acp.fecha_cumplimiento,
                    created_at = DateTime.Now,
                });
                db.SaveChanges();
            }

            Rta.responsable_actual_id = Convert.ToInt32(e3r.responsable4_id);
            Rta.responsable_actual = e3r.responsable4_nombre;
            Rta.etapa_actual = "Aprobaci\xf3n";
            Rta.estado = "En aprobaci\xf3n";
            db.SaveChanges();

            //Notificación
            db.Notificaciones.Add(new Notificaciones
            {
                descripcion = "Te han asignado un rta para: 'Aprobaci\xf3n'",
                modulo = 5,
                cierre = 2,
                rta = Rta.id_rta,
                UserId = etapa3.responsable4_id.ToString(),
                created_at = DateTime.Now
            });
            db.SaveChanges();
            return Json(e3r.etapa3_id);
        }

        [HttpPost]
        [Authorize]
        public ActionResult Etapa3(etapa3 etapa3, List<Causas_inmediatas> causa_inmediata1,
            List<Causas_basicas> causa_basica1, List<Acciones_corr_prev> accion_corr_prev1, int id)
        {
            var e3 = db.etapa3.Add(new etapa3
            {
                tipo_metodologia = etapa3.tipo_metodologia,
                analisis = etapa3.analisis,
                responsable4_id = etapa3.responsable4_id,
                responsable4_nombre = etapa3.responsable4_nombre,
                created_at = DateTime.Now,
                aprobacion = "aprobado",
            });
            db.SaveChanges();

            foreach (var cin in causa_inmediata1)
            {
                db.Causasinmediatas.Add(new Causas_inmediatas
                {
                    etapa3_id = e3.etapa3_id,
                    tipo = cin.tipo,
                    Causa = cin.Causa,
                    descripcion = cin.descripcion,
                    created_at = DateTime.Now,
                });
                db.SaveChanges();
            }

            foreach (var cba in causa_basica1)
            {
                db.CausasBasicas.Add(new Causas_basicas
                {
                    etapa3_id = e3.etapa3_id,
                    Tipo_causa_basica = cba.Tipo_causa_basica,
                    Causa = cba.Causa,
                    Descripcion = cba.Descripcion,
                    created_at = DateTime.Now,
                });
                db.SaveChanges();
            }

            foreach (var acp in accion_corr_prev1)
            {
                db.Acciones_corr_prev.Add(new Acciones_corr_prev
                {
                    etapa3_id = e3.etapa3_id,
                    tipo_accion = acp.tipo_accion,
                    accion = acp.accion,
                    responsable_id = acp.responsable_id,
                    responsable_nombre = acp.responsable_nombre,
                    tipo_evidencia = acp.tipo_evidencia,
                    fecha_cumplimiento = acp.fecha_cumplimiento,
                    created_at = DateTime.Now,
                });
                db.SaveChanges();
            }

            var Rta = db.RespuestaRta.FirstOrDefault(x => x.id_rta == id);
            Rta.etapa3_id = e3.etapa3_id;
            Rta.responsable_actual_id = Convert.ToInt32(e3.responsable4_id);
            Rta.responsable_actual = e3.responsable4_nombre;
            Rta.etapa_actual = "Aprobaci\xf3n";
            Rta.estado = "En aprobaci\xf3n";
            db.SaveChanges();

            //Notificación
            db.Notificaciones.Add(new Notificaciones
            {
                descripcion = "Te han asignado un rta para: 'Aprobaci\xf3n'",
                modulo = 5,
                cierre = 2,
                rta = Rta.id_rta,
                UserId = etapa3.responsable4_id.ToString(),
                created_at = DateTime.Now
            });
            db.SaveChanges();

            return Json(e3.etapa3_id);
        }

        [HttpPost]
        [Authorize]
        public JsonResult saveFiles()
        {
            int length = Convert.ToInt32(Request.Params["fileLength"]);
            int id = Convert.ToInt32(Request.Params["id_etapa3"]);
            string path = Server.MapPath("~/Content/Archivos/RTA/");
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            for (int i = 0; i < length; i++)
            {
                var data = Request.Files["file" + i];
                var time = DateTime.Now.Hour + "" + DateTime.Now.Minute + "" + DateTime.Now.Second;
                var nombre = time + data.FileName;
                data.SaveAs(path + Path.GetFileName(nombre));
                db.ArchivoRta.Add(new ArchivoRTA
                {
                    id_etapa3 = id,
                    nombre_archivo = nombre,
                    created_at = DateTime.Now,
                });
                db.SaveChanges();
            }

            return Json("saved");
        }


        [HttpPost]
        [Authorize]
        public JsonResult modNotificacion(int id)
        {
            if (User.Identity.IsAuthenticated)
            {
                db.Configuration.ProxyCreationEnabled = false;
                var rta = db.Notificaciones.FirstOrDefault(x => x.rta == id);
                if (rta.UserId == User.Identity.GetUserId())
                {
                    rta.estado = "Leido";
                    db.SaveChanges();
                    return Json(rta);
                }
            }

            return Json("error");
        }

        //TRAE ETAPA 3 SEGUN ID
        [HttpPost]
        [Authorize]
        public JsonResult buscaretapa3(int id)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var etapa3 = db.etapa3.Where(x => x.etapa3_id == id).FirstOrDefault();
            return Json(etapa3);
        }

        //TRAE CAUSAS INMEDIATAS SEGUN ID ETAPA 3
        [HttpPost]
        [Authorize]
        public JsonResult buscarCausaInmediata(int id)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var causa = db.Causasinmediatas.Where(x => x.etapa3_id == id).ToList();
            return Json(causa);
        }

        //TRAE CAUSAS BASICAS SEGUN ID ETAPA 3
        [HttpPost]
        [Authorize]
        public JsonResult buscarCausaBasica(int id)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var causa = db.CausasBasicas.Where(x => x.etapa3_id == id).ToList();
            return Json(causa);
        }

        //TRAE Acciones correctivas y preventivas SEGUN ID ETAPA 3
        [HttpPost]
        [Authorize]
        public JsonResult buscarAccion_correctiva_preventiva(int id)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var acciones = db.Acciones_corr_prev.Where(x => x.etapa3_id == id).ToList();
            return Json(acciones);
        }
//        ETAPA 4
        [HttpPost]
        [Authorize]
        public JsonResult Etapa4(etapa4 etapa4, List<Models.PlanAccion> plan_de_accion, int respuesta_rta_id, 
            string aprobacion, List<Acciones_corr_prev> aprobados, List<Acciones_corr_prev> rechazados)
        {
            if (aprobacion == "btn_aprobar")
            {
                var e4 = db.etapa4.Add(new etapa4
                {
                    responsable5_id = etapa4.responsable5_id,
                    responsable5_nombre = etapa4.responsable5_nombre,
                    created_at = DateTime.Now,
                });
                db.SaveChanges();
                
                var Rta = db.RespuestaRta.FirstOrDefault(x => x.id_rta == respuesta_rta_id);
                Rta.etapa4_id = e4.etapa4_id;
                Rta.responsable_actual_id = Convert.ToInt32(e4.responsable5_id);
                Rta.responsable_actual = e4.responsable5_nombre;
                Rta.etapa_actual = "Costos";
                Rta.estado = "Aprobado";
                db.SaveChanges();
                
                var e3 = db.etapa3.FirstOrDefault(x => x.etapa3_id == Rta.etapa3_id);
                e3.aprobacion = "aprobado";
                db.SaveChanges();

                foreach (var p in plan_de_accion)
                {
                    var pa= db.PlanAccion.Add(new Models.PlanAccion
                    {
                        respuesta_rta_id = respuesta_rta_id,
                        accion = p.accion,
                        modulo = 5,
                        responsable_id = p.responsable_id,
                        responsable_nombre = p.responsable_nombre,
                        cumplimiento = p.cumplimiento,
                        tipo_accion_rta = p.tipo_accion_rta,
                        tipo_evidencia = p.tipo_evidencia,
                        created_at = DateTime.Now,
                    });
                    db.SaveChanges();
                    foreach (var a in aprobados)
                    {
                        var acc_corr_prev = db.Acciones_corr_prev.FirstOrDefault(x => x.id_accion == a.id_accion);
                        acc_corr_prev.estado = "aprobado";
                        db.SaveChanges();
                    }
                    foreach (var r in rechazados)
                    {
                        var acc_corr_prev = db.Acciones_corr_prev.FirstOrDefault(x => x.id_accion == r.id_accion);
                        acc_corr_prev.estado = "rechazado";
                        acc_corr_prev.justificacion = r.justificacion;
                        db.SaveChanges();
                    }
                    //Notificación
                    db.Notificaciones.Add(new Notificaciones
                    {
                        descripcion = "Te han asignado un plan de acción asociado a: 'RTA'",
                        modulo = 5,
                        cierre = 2,
                        rta = Rta.id_rta,
                        UserId = pa.responsable_id,
                        created_at = DateTime.Now
                    });
                    db.SaveChanges();
                }
                //Notificación
                db.Notificaciones.Add(new Notificaciones
                {
                    descripcion = "Te han asignado un rta: 'Costos'",
                    modulo = 5,
                    cierre = 2,
                    rta = Rta.id_rta,
                    UserId = e4.responsable5_id,
                    created_at = DateTime.Now
                });
                db.SaveChanges();
                return Json("OK ETAPA5");
            }
            if (aprobacion == "btn_rechazar")
            {
                var Rta = db.RespuestaRta.FirstOrDefault(x => x.id_rta == respuesta_rta_id);
                Rta.etapa_actual = "An\xe1lisis de la anomal\U000000EDa";
                Rta.estado = "Investigado";
                db.SaveChanges();
                
                var e3 = db.etapa3.FirstOrDefault(x => x.etapa3_id == Rta.etapa3_id);
                e3.aprobacion = "rechazado";
                db.SaveChanges();
    
                //Notificación
                db.Notificaciones.Add(new Notificaciones
                {
                    descripcion = "Se ha rechazado un rta: 'Por favor reevaluar'",
                    modulo = 5,
                    cierre = 2,
                    rta = Rta.id_rta,
                    UserId = Convert.ToString(Rta.responsable_actual_id),
                    created_at = DateTime.Now
                });
                db.SaveChanges();

                return Json("RECHAZADO: VUELTA A ETAPA 3");
            }
            return Json("NINGUNO ETAPA 5");
        }
        
        //TRAE ETAPA 4 SEGUN ID
        [HttpPost]
        [Authorize]
        public JsonResult buscaretapa4(int id)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var etapa4 = db.etapa4.Where(x => x.etapa4_id == id).FirstOrDefault();
            return Json(etapa4);
        }

        [HttpPost]
        [Authorize]
        public JsonResult Etapa5(etapa5 etapa5, int id, string plazo_verificacion)
        {
                var e5 = db.etapa5.Add(new etapa5
                {
                    costos_parada = etapa5.costos_parada,
                    costos_acciones_inmediatas = etapa5.costos_acciones_inmediatas,
                    costos_acciones_correctivas = etapa5.costos_acciones_correctivas,
                    costos_acciones_preventivas = etapa5.costos_acciones_preventivas,
                    otros_costos = etapa5.otros_costos,
                    total_costos = etapa5.total_costos,
                    plazo_verificacion = DateTime.Now.AddDays(Double.Parse(plazo_verificacion.ToString())) ,
                    responsable6_id = etapa5.responsable6_id,
                    responsable6_nombre = etapa5.responsable6_nombre,
                    created_at = DateTime.Now,
                });
                db.SaveChanges();
                
                var Rta = db.RespuestaRta.FirstOrDefault(x => x.id_rta == id);
                Rta.etapa5_id = e5.etapa5_id;
                Rta.responsable_actual_id = Convert.ToInt32(e5.responsable6_id);
                Rta.responsable_actual = e5.responsable6_nombre;
                Rta.etapa_actual = "Eficacia";
                Rta.estado = "Costos asignados";
                db.SaveChanges();
                //Notificación
                db.Notificaciones.Add(new Notificaciones
                {
                    descripcion = "Te han asignado un rta: 'Eficacia'",
                    modulo = 5,
                    cierre = 2,
                    rta = Rta.id_rta,
                    UserId = e5.responsable6_id.ToString(),
                    created_at = DateTime.Now
                });
                db.SaveChanges();
                return Json("OK ETAPA5");
        }
        
        //TRAE ETAPA 5 SEGUN ID
        [HttpPost]
        [Authorize]
        public JsonResult buscaretapa5(int id)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var etapa5 = db.etapa5.Where(x => x.etapa5_id == id).FirstOrDefault();
            return Json(etapa5);
        }
        [HttpPost]
        [Authorize]
        public JsonResult Etapa6(etapa6 etapa6, int id)
        {
            var e6 = db.etapa6.Add(new etapa6
            {
                eficacia_rta= etapa6.eficacia_rta,
                plazo_cumplido=etapa6.plazo_cumplido,
                comentarios=etapa6.comentarios,
                created_at = DateTime.Now,
            });
            db.SaveChanges();
            var Rta = db.RespuestaRta.FirstOrDefault(x => x.id_rta == id);
            Rta.etapa6_id = e6.etapa6_id;
            Rta.etapa_actual = "Cerrado";
            Rta.responsable_actual_id = null;
            Rta.responsable_actual = null;
            Rta.estado = "Cerrado con tratamiento "+etapa6.eficacia_rta;
            db.SaveChanges();
            return Json("RTA CERRADO");
        }
        
        
        //TRAE ETAPA 5 SEGUN ID
        [HttpPost]
        [Authorize]
        public JsonResult buscaretapa6(int id)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var etapa6 = db.etapa6.Where(x => x.etapa6_id == id).FirstOrDefault();
            return Json(etapa6);
        }
    }
}


