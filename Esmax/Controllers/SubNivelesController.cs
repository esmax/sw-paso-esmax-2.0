using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;

namespace Esmax.Models
{
    public class SubNivelesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        
        [HttpGet]
        [Authorize]
        public ActionResult getSubNiveles()
        {
            db.Configuration.ProxyCreationEnabled = false;
            List<Sub_niveles> subNiveles = db.SubNiveles.ToList<Sub_niveles>();
            return Json(subNiveles, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        [Authorize]
        public ActionResult getOdtSubNiveles(int id)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var odt = db.Odts.GroupJoin(db.OdtSubNiveles, od => od.odts_id,
                osn => osn.odts_id,  (od, osn) => new {od, osn}).FirstOrDefault(x => x.od.odts_id == id);
            return Json(odt, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        [Authorize]
        public ActionResult addSubNivel(Sub_niveles subNiveles)
        {
            var saved = db.SubNiveles.Add(new Sub_niveles
            {
                sub_niveles_nombre = subNiveles.sub_niveles_nombre,
                sub_niveles_codigo = subNiveles.sub_niveles_codigo,
                niveles_id = subNiveles.niveles_id,
                created_at = DateTime.Now
            });
            db.SaveChanges();
            return Json(saved, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [Authorize]
        public JsonResult modificarSubNivel(Sub_niveles subNiveles)
        {
            db.Entry(subNiveles).State = EntityState.Modified;
            db.SaveChanges();

            return Json("modified", JsonRequestBehavior.AllowGet);
        }
        
        [HttpPost]
        [Authorize]
        public JsonResult findSubNiveles(int id)
        {
            db.Configuration.ProxyCreationEnabled = false;
            List<PlanAccion_OdtSubNiveles> subNiveles = db.PlanAccion_OdtSubNiveles.Where(x => x.plan_accion_id == id).ToList();
            return Json(subNiveles);
        }
    }
}