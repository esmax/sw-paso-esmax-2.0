using System.Linq;
using System.Web.Mvc;
using Esmax.Models;


namespace Esmax.Controllers
{
    public class AreaController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
                        
        [HttpGet]
        [Authorize]
        public ActionResult GetArea()
        {
            db.Configuration.ProxyCreationEnabled = false;
            var areas = db.Areas.ToList();
            return Json(areas, JsonRequestBehavior.AllowGet);
        } 
    }
}