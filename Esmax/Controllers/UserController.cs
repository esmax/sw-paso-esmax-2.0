using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using System.Text.RegularExpressions;
using Esmax.Models;

namespace Esmax.Controllers
{
    
    public class user2{
        public int ID { get; set; }
        public string UserName { get; set; }

    }
    public class UserController : Controller
        {
            private ApplicationDbContext db = new ApplicationDbContext();
            private ApplicationSignInManager _signInManager;
            private ApplicationUserManager _userManager;
            
            
            public class usersEsmax
            {
                public string id_user { get; set; }
                public string chave { get; set; }
                public string rut { get; set; }
                public string cargo { get; set; }
                public DateTime fecha_ingreso { get; set; }
                public DateTime fecha_nacimiento { get; set; }
                public string rol { get; set; }
                public string email { get; set; }
                public string phoneNumber { get; set; }
                public int holding_id { get; set; }
                public int negocio_id { get; set; }
                public int gerencia_id { get; set; }
                public int area_id { get; set; }
                public string userName { get; set; }
            }
            public ApplicationUserManager UserManager
            {
                
                get
                {
                    return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
                }
                private set
                {
                    _userManager = value;
                }
            }
            [HttpGet]
            [Authorize]
            public ActionResult allUsers()
            {
                //var user = UserManager.Users.ToList();
                db.Configuration.ProxyCreationEnabled = false;
                List<ApplicationUser> user = db.Users.ToList();
    
                return Json(user, JsonRequestBehavior.AllowGet);
            }
            
            
            [HttpPost]
            [Authorize]
            public ActionResult eArea(string id)
            {
                var user = db.Users.Where(x => x.Id ==  id).FirstOrDefault();
                return Json(user, JsonRequestBehavior.AllowGet);
            }
            
            [HttpPost]
            [Authorize]
            public async Task<ActionResult> Register(usersEsmax datosUser)
            {
      
                var pass = "esmax2019";
                var user = new ApplicationUser { UserName = datosUser.userName, Email = datosUser.email, Rut = datosUser.rut, Id = datosUser.id_user, 
                    Cargo= datosUser.cargo, Fecha_Ingreso = datosUser.fecha_ingreso, Fecha_Nacimiento = datosUser.fecha_nacimiento, 
                    Rol = datosUser.rol, Chave = datosUser.chave, PhoneNumber = datosUser.phoneNumber, holding_id = datosUser.holding_id,
                    negocio_id = datosUser.negocio_id, gerencia_id = datosUser.gerencia_id,
                    area_id = datosUser.area_id
                };
                var result = await UserManager.CreateAsync(user, pass);
                if (result.Succeeded)
                {
                    // Para obtener más información sobre cómo habilitar la confirmación de cuentas y el restablecimiento de contraseña, visite https://go.microsoft.com/fwlink/?LinkID=320771
                    // Enviar correo electrónico con este vínculo
                    // string code = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);
                    // var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
                    // await UserManager.SendEmailAsync(user.Id, "Confirmar cuenta", "Para confirmar la cuenta, haga clic <a href=\"" + callbackUrl + "\">aquí</a>");

                    TempData.Add("result", "created");
                    return RedirectToAction("Registrar", "Admin");
                }
                TempData.Add("result", "error");
                return RedirectToAction("Registrar", "Admin");

                
                // Si llegamos a este punto, es que se ha producido un error y volvemos a mostrar el formulario


            }

            [HttpPost]
            [Authorize]
            public JsonResult getObservador(string id)
            {
                var obs = db.Users.Where(u => u.Id == id).FirstOrDefault();
                
                return Json(obs, JsonRequestBehavior.AllowGet);
            }
            
            [HttpPost]
            [Authorize]
            public JsonResult modificarUser(ApplicationUser datos)
            {
                db.Entry(datos).State = EntityState.Modified;
                db.SaveChanges();

                return Json("modified", JsonRequestBehavior.AllowGet);
            }

            [HttpPost]
            [Authorize]
            public JsonResult getUser(string id)
            {
                db.Configuration.ProxyCreationEnabled = false;
                var user = db.Users.Where(u => u.Id == id).FirstOrDefault();
                return Json(user, JsonRequestBehavior.AllowGet);
            }

            [HttpPost]
            [Authorize]
            public JsonResult changePassword(string oldPassword, string newPassword)
            {
                var idUser = User.Identity.GetUserId();

                var a = UserManager.ChangePassword(idUser, oldPassword, newPassword);

                if (a.Succeeded)
                {
                    return Json("ok");
                }
                else
                {
                    return Json(a.Errors);
                }
                
            }
            
            [HttpPost]
            [Authorize]
            public JsonResult getNotification(string id)
            {
                db.Configuration.ProxyCreationEnabled = false;
                
                List<Notificaciones> notificaciones = db.Notificaciones.Where(x => x.UserId == id).OrderByDescending(c => c.notificacionesId).ToList();

                return Json(notificaciones);

            }
            
            [HttpPost]
            [Authorize]
            public JsonResult changeEstadoNotification(string id, int plan_id)
            {

                if (User.Identity.IsAuthenticated)
                {
                    if (User.Identity.GetUserId() == id)
                    {
                        db.Configuration.ProxyCreationEnabled = false;
                
                        var notificacion = db.Notificaciones.Where( u => u.plan_accion_id == plan_id).FirstOrDefault();
                        notificacion.estado = "Leido";
                        db.SaveChanges();

                        
                    }
                }
                return Json("OK");
            }
            
            
            
            [HttpGet]
            [Authorize]
            public ActionResult ModificarCuenta()
            {
                return RedirectToAction("ModificarCuenta", "Account");

            }
            
            [HttpPost]
            [Authorize]
            public JsonResult modCuenta(string email, string user, string id)
            {

                var emailConfirmed = email_bien_escrito(email);

                if (emailConfirmed == false)
                {
                    return Json("Error email no valido");
                }
                else
                {
                    db.Configuration.ProxyCreationEnabled = false;
                    var usuario = db.Users.Where(x => x.Id == id).FirstOrDefault();
                    usuario.Email = email;
                    usuario.Usuario = user;
                    var respuesta = db.SaveChanges();

                    return Json(respuesta);
                }
                
            }
            
            [Authorize]
            private Boolean email_bien_escrito(String email)
            {
                String expresion;
                expresion = "\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*";
                if (Regex.IsMatch(email,expresion))
                {
                    if (Regex.Replace(email, expresion, String.Empty).Length == 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            

        }
}