﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using Esmax.Models;
using Esmax.Models.CheckList;

namespace Esmax.Controllers.CheckList
{

    public class plan
    {
        public int actividad_id { get; set; }
        public string responsable_id { get; set; }
        public string accion { get; set; }
        public int cumplimiento { get; set; }
    }
    public class Datos
    {
        public string nombre { get; set; }
        public int total { get; set; }
    }
    public class CheckListController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        // GET: CheckList
        [Authorize]
        public ActionResult CrearCheckList()
        {
            return View("CrearCheckList", "_Layout");
        }
        
        [Authorize]
        public ActionResult ver(int id)
        {
            ViewBag.CheckListId = id;
            return View("VerCheckList", "_Layout");
        }

        [HttpPost]
        [Authorize]
        public JsonResult saveCheckList(Models.CheckList.CheckList checkList, int anonimo, List<Actividades> actividades, List<Preguntas> preguntas)
        {
            var ch = db.CheckList.Add(new Models.CheckList.CheckList
            {
                CheckListNombre = checkList.CheckListNombre,
                CheckListDescripcion = checkList.CheckListDescripcion,
                PlanAccion = anonimo,
                categorias_id = checkList.categorias_id,
                created_at = DateTime.Now
            });
            db.SaveChanges();

            var index = 1;

            foreach (var actividad in actividades)
            {
                var act = db.Actividades.Add(new Actividades
                {
                    ActividadNombre = actividad.ActividadNombre,
                    CheckListId = ch.CheckListId,
                    created_at = DateTime.Now,
                    Porcentaje = actividad.Porcentaje
                });
                db.SaveChanges();
                
                foreach (var pregunta in preguntas)
                {
                    if (pregunta.ActividadId == index )
                    {
                        db.Preguntas.Add(new Preguntas
                        {
                            pregunta = pregunta.pregunta,
                            ActividadId = act.ActividadId,
                            created_at = DateTime.Now,
                            Procentaje = pregunta.Procentaje
                        });
                        db.SaveChanges();
                    }
                }

                index += 1;
            }
            return Json("ok");
        }

        [HttpGet]
        [Authorize]
        public ActionResult ingresarCheckList()
        {
            return View("IngresarCheckList", "_Layout");
        }
        
        [HttpGet]
        [Authorize]
        public ActionResult listRespuestas()
        {
            return View("RevisarCheckList", "_Layout");
        }
        
        [HttpGet]
        [Authorize]
        public ActionResult graficossCheckList()
        {
            return View("GraficosCheckList", "_Layout");
        }
        
        [HttpGet]
        [Authorize]
        public ActionResult getCheckList()
        {
            db.Configuration.ProxyCreationEnabled = false;
            List<Models.CheckList.CheckList> checkList = db.CheckList.ToList();
            return Json(checkList, JsonRequestBehavior.AllowGet);
        }
        
        [HttpPost]
        [Authorize]
        public ActionResult getCheckListById(int id)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var checkList = db.CheckList.Where(x => x.CheckListId == id).FirstOrDefault();
            return Json(checkList);
        }
        string cs = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

        [HttpPost]
        [Authorize]
        public JsonResult findCheckListWhereCategorias(string query)
        {
            List<Models.CheckList.CheckList> lst = new List<Models.CheckList.CheckList>();
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                string query1 = @"SELECT * FROM CheckLists " + query + " and estado = 'habilitado'"; 
                SqlCommand com = new SqlCommand(query1, con);
                SqlDataReader rdr = com.ExecuteReader();
                while (rdr.Read())
                {
                    lst.Add(new Models.CheckList.CheckList
                    {
                        CheckListId = Convert.ToInt32(rdr["CheckListId"] is DBNull ? 0: rdr["CheckListId"]),
                        CheckListNombre = Convert.ToString(rdr["CheckListNombre"] is DBNull ? 0: rdr["CheckListNombre"]),
                        categorias_id = Convert.ToInt32(rdr["categorias_id"] is DBNull ? 0: rdr["categorias_id"]),
                        PlanAccion = Convert.ToInt32(rdr["PlanAccion"] is DBNull ? 0: rdr["PlanAccion"]),
                        
                    });
                }
                return Json(lst);
            } 
        }
        
        [HttpGet]
        [Authorize]
        public ActionResult responderCheckList(int id)
        {
            ViewBag.checkListId = id;
            var checkList = db.CheckList.Where(x => x.CheckListId == id).FirstOrDefault();

            if (checkList.PlanAccion == 0)
            {
                return View("ResponderCheckList","_Layout");
            }
            else
            {
                return View("ResponderCheckListPlanAccion","_Layout");
            }
            
        }
        
        [HttpGet]
        [Authorize]
        public ActionResult getCategorias()
        {
            db.Configuration.ProxyCreationEnabled = false;
            List<CategoriasC> categorias = db.CategoriasC.ToList<CategoriasC>();

            return Json(categorias, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [Authorize]
        public JsonResult createCategoria(string nombre)
        {
            db.CategoriasC.Add(new CategoriasC
            {
                categorias_nombre = nombre,
                created_at = DateTime.Now
            });
            db.SaveChanges();

            return Json("created");
        }

        [HttpPost]
        [Authorize]
        public JsonResult getActividades(int id)
        {
            db.Configuration.ProxyCreationEnabled = false;
            List<Actividades> lista = db.Actividades.Where(x => x.CheckListId == id).ToList();
            return Json(lista);
        }
        
        [HttpPost]
        [Authorize]
        public JsonResult getPreguntas(string query)
        {
            List<Preguntas> lst = new List<Preguntas>();
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                string query1 = @"SELECT * FROM Preguntas " + query; 
                SqlCommand com = new SqlCommand(query1, con);
                SqlDataReader rdr = com.ExecuteReader();
                while (rdr.Read())
                {
                    lst.Add(new Preguntas
                    {
                        PreguntaId = Convert.ToInt32(rdr["PreguntaId"] is DBNull ? 0: rdr["PreguntaId"]),
                        pregunta = Convert.ToString(rdr["pregunta"] is DBNull ? 0: rdr["pregunta"]),
                        ActividadId = Convert.ToInt32(rdr["ActividadId"] is DBNull ? 0: rdr["ActividadId"]),
                        Procentaje =  Convert.ToInt32(rdr["Procentaje"] is DBNull ? 0: rdr["Procentaje"]),
                        
                    });
                }
                return Json(lst);
            } 
        }
        
        [HttpPost]
        [Authorize]
        public JsonResult CheckListSimple(DatosGeneralesCheckList datosGenerales1, List<rActividades> actividades1, RespuestaCheckList datosRespuesta)
        {

            var dg = db.DatosGeneralesCheckList.Add(new DatosGeneralesCheckList
            {
                Observador_id = datosGenerales1.Observador_id,
                fecha = datosGenerales1.fecha,
                hora = datosGenerales1.hora,
                holding_id = datosGenerales1.holding_id,
                negocio_id = datosGenerales1.negocio_id,
                gerencia_id = datosGenerales1.gerencia_id,
                area_id = datosGenerales1.area_id,
                observador_nombre = datosGenerales1.observador_nombre,
                anonimo = datosGenerales1.anonimo,
                created_at = DateTime.Now
            });
            db.SaveChanges();
            
            var cl = db.CheckList.FirstOrDefault(x => x.CheckListId == datosRespuesta.CheckListId);
            var cat = db.CategoriasC.FirstOrDefault(x => x.categorias_id == cl.categorias_id);
            
            var rsp = db.RespuestaCheckList.Add(new RespuestaCheckList
            {
                holding_nombre = datosRespuesta.holding_nombre,
                negocio_nombre = datosRespuesta.negocio_nombre,
                gerencia_nombre = datosRespuesta.gerencia_nombre,
                area_nombre = datosRespuesta.area_nombre,
                observador_nombre = datosRespuesta.observador_nombre,
                datosGeneralesCheckListId = dg.datosGeneralesCheckListId,
                CheckListId = datosRespuesta.CheckListId,
                CheckListNombre = cl.CheckListNombre,
                categorias_nombre = cat.categorias_nombre,
                created_at = DateTime.Now
            });
            db.SaveChanges();

            foreach (var actividad in actividades1)
            {
                db.rActividades.Add(new rActividades
                {
                    ActividadId = actividad.ActividadId,
                    PreguntaId = actividad.PreguntaId,
                    na = actividad.na,
                    si = actividad.si,
                    no = actividad.no,
                    comentario = actividad.comentario,
                    pregunta = actividad.pregunta, 
                    respuestaCheckListId = rsp.respuestaCheckListId, 
                    created_at = DateTime.Now
                });
                db.SaveChanges();
            }
            return Json("saved");
        }
        
        [HttpPost]
        [Authorize]
        public JsonResult CheckListPlanAccion(DatosGeneralesCheckList datosGenerales1, List<rActividades> actividades1, RespuestaCheckList datosRespuesta, List<plan> planAccion, double totalAprobado)
        {

            var dg = db.DatosGeneralesCheckList.Add(new DatosGeneralesCheckList
            {
                Observador_id = datosGenerales1.Observador_id,
                fecha = datosGenerales1.fecha,
                hora = datosGenerales1.hora,
                holding_id = datosGenerales1.holding_id,
                negocio_id = datosGenerales1.negocio_id,
                gerencia_id = datosGenerales1.gerencia_id,
                area_id = datosGenerales1.area_id,
                observador_nombre = datosGenerales1.observador_nombre,
                created_at = DateTime.Now
            });
            db.SaveChanges();

            var cl = db.CheckList.FirstOrDefault(x => x.CheckListId == datosRespuesta.CheckListId);
            
            var cat = db.CategoriasC.FirstOrDefault(x => x.categorias_id == cl.categorias_id);
            
            var rsp = db.RespuestaCheckList.Add(new RespuestaCheckList
            {
                holding_nombre = datosRespuesta.holding_nombre,
                negocio_nombre = datosRespuesta.negocio_nombre,
                gerencia_nombre = datosRespuesta.gerencia_nombre,
                area_nombre = datosRespuesta.area_nombre,
                observador_nombre = datosRespuesta.observador_nombre,
                datosGeneralesCheckListId = dg.datosGeneralesCheckListId,
                CheckListId = datosRespuesta.CheckListId,
                CheckListNombre = cl.CheckListNombre,
                categorias_nombre = cat.categorias_nombre,
                created_at = DateTime.Now,
                Porcentaje = totalAprobado
            });
            db.SaveChanges();

            foreach (var actividad in actividades1)
            {
                db.rActividades.Add(new rActividades
                {
                    ActividadId = actividad.ActividadId,
                    PreguntaId = actividad.PreguntaId,
                    na = actividad.na,
                    si = actividad.si,
                    no = actividad.no,
                    comentario = actividad.comentario,
                    pregunta = actividad.pregunta, 
                    respuestaCheckListId = rsp.respuestaCheckListId, 
                    created_at = DateTime.Now
                });
                db.SaveChanges();
            }

            foreach (var item in planAccion)
            {
                var pa = db.PlanAccion.Add(new Models.PlanAccion
                {
                    actividad_id = item.actividad_id,
                    responsable_id = item.responsable_id,
                    accion = item.accion,
                    cumplimiento = datosGenerales1.fecha.AddDays(item.cumplimiento),
                    modulo = 4,
                    CheckListId = rsp.respuestaCheckListId,
                    created_at = DateTime.Now
                });
                db.SaveChanges();
            
            
            /*if (planAccion.accion != null || planAccion.accion == "")
            {
                var pa = db.PlanAccion.Add(new Models.PlanAccion
                {
                    responsable_id = planAccion.responsable_id,
                    accion = planAccion.accion,
                    cumplimiento = datosGenerales1.fecha.AddDays(planAccion.cumplimiento),
                    modulo = 4,
                    CheckListId = rsp.respuestaCheckListId,
                    created_at = DateTime.Now
                
                });
                db.SaveChanges();*/
                
                db.Notificaciones.Add(new Notificaciones
                {
                    descripcion = "Tienes un nuevo plan de acción asociado a CHECK LIST. ''" + pa.accion + "''",
                    modulo = 4,
                    plan_accion_id = pa.plan_accion_id,
                    UserId = pa.responsable_id,
                    created_at = DateTime.Now
                });
                db.SaveChanges();
            
                var user = db.Users.Where(x => x.Id == pa.responsable_id).FirstOrDefault();
                try
                {
                    MailMessage correo = new MailMessage();
                    correo.From = new MailAddress("sistemapaso@esmax.cl"); //correo que usara la app
                    correo.To.Add(user.Email);
                    correo.Subject = "Tienes un nuevo plan de accion asociado";
                    correo.Body = user.UserName + ", <br/><br/> Tiene un nuevo plan de acción asociado a CHECK LIST, Número de folio: " + pa.plan_accion_id + " <br/><br/> Haz click aqui para acceder: http://paso.esmax.cl/MplanAccion/ver/"+pa.plan_accion_id+ "<br><br><br> Por favor no responder este correo.";
                    correo.IsBodyHtml = true;
                    correo.Priority = MailPriority.Normal;

                    SmtpClient smtp = new SmtpClient();
                    smtp.Host = "smtp.esmax.cl";
                    smtp.Port = 25;
                    smtp.EnableSsl = true;
                    smtp.UseDefaultCredentials = true;
                    string sCuentaCorreo = "sistemapaso@esmax.cl";
                    string sPasswordCorreo = "";
                    smtp.Credentials = new System.Net.NetworkCredential(sCuentaCorreo, sPasswordCorreo);

                    smtp.Send(correo);
                    ViewBag.Mensaje = "Mensaje enviado correctamente";
                }
                catch (Exception e)
                {
                    ViewBag.Error = e.Message;
                }
            }
            
            return Json("saved");
        }
        
        [HttpPost]
        [Authorize]
        public JsonResult findRespuestaQuery(string query)
        {
            List<RespuestaCheckList> lst = new List<RespuestaCheckList>();
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                string query1 = @"SELECT * FROM RespuestaCheckLists " + query; 
                SqlCommand com = new SqlCommand(query1, con);
                SqlDataReader rdr = com.ExecuteReader();
                while (rdr.Read())
                {
                    lst.Add(new RespuestaCheckList
                    {
                        respuestaCheckListId = Convert.ToInt32(rdr["respuestaCheckListId"] is DBNull ? 0: rdr["respuestaCheckListId"]),
                        created_at = Convert.ToDateTime(rdr["created_at"] is DBNull ? 0: rdr["created_at"]),
                        holding_nombre = Convert.ToString(rdr["holding_nombre"] is DBNull ? 0: rdr["holding_nombre"]),
                        datosGeneralesCheckListId = Convert.ToInt32(rdr["datosGeneralesCheckListId"] is DBNull ? 0: rdr["datosGeneralesCheckListId"]),
                        observador_nombre = Convert.ToString(rdr["observador_nombre"] is DBNull ? 0: rdr["negocio_nombre"]),
                        negocio_nombre = Convert.ToString(rdr["negocio_nombre"] is DBNull ? 0: rdr["negocio_nombre"]),
                        gerencia_nombre = Convert.ToString(rdr["gerencia_nombre"] is DBNull ? 0: rdr["gerencia_nombre"]),
                        area_nombre = Convert.ToString(rdr["area_nombre"] is DBNull ? 0: rdr["area_nombre"]),
                        categorias_nombre = Convert.ToString(rdr["categorias_nombre"] is DBNull ? 0: rdr["categorias_nombre"]),
                        CheckListId = Convert.ToInt32(rdr["CheckListId"] is DBNull ? 0: rdr["CheckListId"]),
                        CheckListNombre = Convert.ToString(rdr["CheckListNombre"] is DBNull ? 0: rdr["CheckListNombre"]),
                        
                    });
                }
                return Json(lst);
            } 
        }
        
        
        [HttpGet]
        [Authorize]
        public JsonResult findRespuestaSinQuery()
        {
            db.Configuration.ProxyCreationEnabled = false;
            var rsp = db.RespuestaCheckList.ToList();

            return Json(rsp, JsonRequestBehavior.AllowGet);
        }
        
        [HttpPost]
        [Authorize]
        public JsonResult findCheckList(int id)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var checkList = db.RespuestaCheckList.FirstOrDefault(x => x.respuestaCheckListId == id);
            return Json(checkList);
        }
        
        [HttpPost]
        [Authorize]
        public JsonResult getDatosGenerales(int id)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var checkList = db.DatosGeneralesCheckList.FirstOrDefault(x => x.datosGeneralesCheckListId == id);
            return Json(checkList);
        }
        
        [HttpPost]
        [Authorize]
        public JsonResult rActividades(int id)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var checkList = db.rActividades.Where(x => x.respuestaCheckListId == id).ToList();
            return Json(checkList);
        }
        
        [HttpPost]
        [Authorize]
        public JsonResult Actividades(int id)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var checkList = db.Actividades.FirstOrDefault(x => x.ActividadId == id);
            return Json(checkList);
        }
        
        [HttpPost]
        [Authorize]
        public JsonResult planAccion(int id)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var checkList = db.PlanAccion.FirstOrDefault(x => x.CheckListId == id);
            return Json(checkList);
        }

        [HttpPost]
        [Authorize]
        public JsonResult getPlanes(int respuestaCheckListId)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var planes = (from p in db.PlanAccion
                where p.CheckListId == respuestaCheckListId
                select p).ToList();

            foreach (var item in planes)
            {
                var nombre = (from u in db.Users
                              where u.Id == item.responsable_id
                              select u.UserName).FirstOrDefault();

                item.responsable_id = nombre;
            }

            return Json(planes);
        }
        
        [HttpPost]
        [Authorize]
        public JsonResult getCheckListFecha(string desde, string hasta)
        {
            List<Datos> lst = new List<Datos>();
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                string query = @"select CheckListNombre, COUNT(CheckListId) as total  from RespuestaCheckLists  where created_at >= @desde and created_at <= @hasta group by CheckListNombre, CheckListId;"; 
                SqlCommand com = new SqlCommand(query, con);
                com.Parameters.AddWithValue("@desde", desde);
                com.Parameters.AddWithValue("@hasta", hasta);
                SqlDataReader rdr = com.ExecuteReader();
                while (rdr.Read())
                {
                    lst.Add(new Datos
                    {
                        nombre = Convert.ToString(rdr["CheckListNombre"] is DBNull ? 0: rdr["CheckListNombre"]),
                        total = Convert.ToInt32(rdr["total"] is DBNull ? 0: rdr["total"])
                        
                    });
                }
                return Json(lst);
            } 
        }
        
        [HttpPost]
        [Authorize]
        public JsonResult getRespuestaHolding(string desde, string hasta)
        {
            List<Datos> lst = new List<Datos>();
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                string query = @"select holding_nombre, COUNT(holding_nombre) as total from RespuestaCheckLists where created_at >= @desde and created_at <= @hasta group by holding_nombre order by total desc "; 
                SqlCommand com = new SqlCommand(query, con);
                
                com.Parameters.AddWithValue("@desde", desde);
                com.Parameters.AddWithValue("@hasta", hasta);
                SqlDataReader rdr = com.ExecuteReader();
                while (rdr.Read())
                {
                    lst.Add(new Datos
                    {
                        nombre = Convert.ToString(rdr["holding_nombre"] is DBNull ? 0: rdr["holding_nombre"]),
                        total = Convert.ToInt32(rdr["total"] is DBNull ? 0: rdr["total"]),
                        
                    });
                }
                return Json(lst);
            } 
        }
        
        [HttpPost]
        [Authorize]
        public JsonResult getRespuestaGerencia(string desde, string hasta)
        {
            List<Datos> lst = new List<Datos>();
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                string query = @"select gerencia_nombre, COUNT(gerencia_nombre) as total from RespuestaCheckLists where created_at >= @desde and created_at <= @hasta group by gerencia_nombre order by total desc "; 
                SqlCommand com = new SqlCommand(query, con);
                com.Parameters.AddWithValue("@desde", desde);
                com.Parameters.AddWithValue("@hasta", hasta);
                SqlDataReader rdr = com.ExecuteReader();
                while (rdr.Read())
                {
                    lst.Add(new Datos
                    {
                        nombre = Convert.ToString(rdr["gerencia_nombre"] is DBNull ? 0: rdr["gerencia_nombre"]),
                        total = Convert.ToInt32(rdr["total"] is DBNull ? 0: rdr["total"]),
                        
                    });
                }
                return Json(lst);
            } 
        }
        [HttpPost]
        [Authorize]
        public JsonResult getUsuariosResponden(string desde, string hasta)
        {
            List<Datos> lst = new List<Datos>();
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                string query = @"select observador_nombre, COUNT(observador_nombre) as total from RespuestaCheckLists where created_at >= @desde and created_at <= @hasta group by observador_nombre order by total desc "; 
                SqlCommand com = new SqlCommand(query, con);
                com.Parameters.AddWithValue("@desde", desde);
                com.Parameters.AddWithValue("@hasta", hasta);
                SqlDataReader rdr = com.ExecuteReader();
                while (rdr.Read())
                {
                    lst.Add(new Datos
                    {
                        nombre = Convert.ToString(rdr["observador_nombre"] is DBNull ? 0: rdr["observador_nombre"]),
                        total = Convert.ToInt32(rdr["total"] is DBNull ? 0: rdr["total"]),
                        
                    });
                }
                return Json(lst);
            } 
        }
        
        [HttpPost]
        [Authorize]
        public JsonResult getRespuestaNegocio(string desde, string hasta)
        {
            List<Datos> lst = new List<Datos>();
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                string query = @"select negocio_nombre, COUNT(negocio_nombre) as total from RespuestaCheckLists where created_at >= @desde and created_at <= @hasta group by negocio_nombre order by total desc "; 
                SqlCommand com = new SqlCommand(query, con);
                com.Parameters.AddWithValue("@desde", desde);
                com.Parameters.AddWithValue("@hasta", hasta);
                SqlDataReader rdr = com.ExecuteReader();
                while (rdr.Read())
                {
                    lst.Add(new Datos
                    {
                        nombre = Convert.ToString(rdr["negocio_nombre"] is DBNull ? 0: rdr["negocio_nombre"]),
                        total = Convert.ToInt32(rdr["total"] is DBNull ? 0: rdr["total"]),
                        
                    });
                }
                return Json(lst);
            } 
        }
        
        [HttpPost]
        [Authorize]
        public JsonResult getRespuestaArea(string desde, string hasta)
        {
            List<Datos> lst = new List<Datos>();
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                string query = @"select area_nombre, COUNT(area_nombre) as total from RespuestaCheckLists where created_at >= @desde and created_at <= @hasta group by area_nombre order by total desc "; 
                SqlCommand com = new SqlCommand(query, con);
                com.Parameters.AddWithValue("@desde", desde);
                com.Parameters.AddWithValue("@hasta", hasta);
                SqlDataReader rdr = com.ExecuteReader();
                while (rdr.Read())
                {
                    lst.Add(new Datos
                    {
                        nombre = Convert.ToString(rdr["area_nombre"] is DBNull ? 0: rdr["area_nombre"]),
                        total = Convert.ToInt32(rdr["total"] is DBNull ? 0: rdr["total"]),
                        
                    });
                }
                return Json(lst);
            } 
        }
        
        [HttpGet]
        [Authorize]
        public ActionResult deshabilitar(int id)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var CheckList = db.CheckList.FirstOrDefault(o => o.CheckListId == id);

            if (CheckList.estado == "habilitado")
            {
                CheckList.estado = "deshabilitado";
                db.SaveChanges();
            }else if (CheckList.estado == "deshabilitado")
            {
                CheckList.estado = "habilitado";
                db.SaveChanges();
            }

            return RedirectToAction("ingresarCheckList", "CheckList");
        }
        
        [HttpGet]
        [Authorize]
        public ActionResult modificarCheckList(int id)
        {
            ViewBag.idMod = id;
            return View("modificarCheckList","_Layout");
        }
        
        [HttpPost]
        [Authorize]
        public JsonResult UpdateCheckList(int idCL, int CategoriaId, string titulo, string descripcion)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var checkList = db.CheckList.FirstOrDefault(x => x.CheckListId == idCL);
            checkList.categorias_id = CategoriaId;
            checkList.CheckListNombre = titulo;
            checkList.CheckListDescripcion = descripcion;
            db.SaveChanges();
            
            return Json(checkList);
        }

        public JsonResult SetPorcentaje(int idActividad, int idPregunta)
        {
            db.Configuration.ProxyCreationEnabled = false;

            //var respuesta = db.rActividades.FirstOrDefault(x => x.ActividadId == idActividad);
            var respuesta = (from x in db.rActividades
                            where x.ActividadId == idActividad
                            where x.PreguntaId == idPregunta
                            select x).FirstOrDefault();
            
            return Json(respuesta);
        }

    }
}