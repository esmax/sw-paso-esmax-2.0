using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Esmax.Models
{
    public class CategoriasController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        [HttpGet]
        [Authorize]
        public ActionResult getCategorias()
        {
            db.Configuration.ProxyCreationEnabled = false;
            List<Categorias> categorias = db.Categorias.ToList<Categorias>();

            return Json(categorias, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [Authorize]
        public JsonResult createCategoria(string nombre)
        {
            db.Categorias.Add(new Categorias
            {
                categorias_nombre = nombre,
                created_at = DateTime.Now
            });
            db.SaveChanges();

            return Json("created");
        }
    }
}