using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Esmax.Models;
using Microsoft.AspNet.Identity;
using System.Globalization;
using System.Security.Claims;
using System.Web;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;






namespace Esmax.Controllers
{
    public class ApiController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        string cs = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

        //Obtener odts 
        public JsonResult getOdt()
        {
            db.Configuration.LazyLoadingEnabled = false;
            var odts = db.Odts.ToList();
            return Json(odts, JsonRequestBehavior.AllowGet);
        }
        
        //Obtener Categorias
        public JsonResult getCategorias()
        {
            db.Configuration.LazyLoadingEnabled = false;
            var categorias = db.Categorias.ToList();
            return Json(categorias, JsonRequestBehavior.AllowGet);
        }
        
        //Obtener OdtTareas
        public JsonResult getOdtTareas()
        {
            List<Context.OdtTareas> lst = new List<Context.OdtTareas>();
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                string query1 = @"select OdtTareas_id, odts_id, OdtTareas.tareas_id, Tareas.tareas_titulo from OdtTareas Inner Join Tareas On OdtTareas.tareas_id = Tareas.tareas_id"; 
                SqlCommand com = new SqlCommand(query1, con);
                SqlDataReader rdr = com.ExecuteReader();
                while (rdr.Read())
                {
                    lst.Add(new Context.OdtTareas
                    {
                        OdtTareas_id = Convert.ToInt32(rdr["OdtTareas_id"] is DBNull ? 0: rdr["OdtTareas_id"]),
                        odts_id = Convert.ToInt32(rdr["odts_id"] is DBNull ? 0: rdr["odts_id"]),
                        tareas_id = Convert.ToInt32(rdr["tareas_id"] is DBNull ? 0: rdr["tareas_id"]),
                        tareas_titulo = Convert.ToString(rdr["tareas_titulo"] is DBNull ? 0: rdr["tareas_titulo"]),
                        
                    });
                }
                return Json(lst, JsonRequestBehavior.AllowGet);
            } 
        }
        
        //Obtener OdtSubTareas
        public JsonResult getOdtSubTareas()
        {
            List<Context.OdtSubTareas> lst = new List<Context.OdtSubTareas>();
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                string query1 = @"select OdtSubTareas_id, odts_id, OdtSubTareas.sub_tareas_id, Sub_tareas.sub_tareas_nombre, Sub_tareas.tareas_id from OdtSubTareas Inner Join Sub_tareas On OdtSubTareas.sub_tareas_id = Sub_tareas.sub_tareas_id"; 
                SqlCommand com = new SqlCommand(query1, con);
                SqlDataReader rdr = com.ExecuteReader();
                while (rdr.Read())
                {
                    lst.Add(new Context.OdtSubTareas
                    {
                        OdtSubTareas_id = Convert.ToInt32(rdr["OdtSubTareas_id"] is DBNull ? 0: rdr["OdtSubTareas_id"]),
                        odts_id = Convert.ToInt32(rdr["odts_id"] is DBNull ? 0: rdr["odts_id"]),
                        sub_tareas_id = Convert.ToInt32(rdr["sub_tareas_id"] is DBNull ? 0: rdr["sub_tareas_id"]),
                        sub_tareas_nombre = Convert.ToString(rdr["sub_tareas_nombre"] is DBNull ? 0: rdr["sub_tareas_nombre"]),
                        tareas_id = Convert.ToInt32(rdr["tareas_id"] is DBNull ? 0: rdr["tareas_id"]),
                        
                    });
                }
                return Json(lst, JsonRequestBehavior.AllowGet);
            } 
        }
        
        //Obtener OdtSubNiveles
        public JsonResult getOdtSubNiveles()
        {
            List<Context.OdtSubNiveles> lst = new List<Context.OdtSubNiveles>();
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                string query1 = @"select OdtSubNiveles_id, odts_id, OdtSubNiveles.sub_niveles_id, Sub_niveles.sub_niveles_nombre, Sub_niveles.sub_niveles_codigo, Sub_niveles.niveles_id from OdtSubNiveles Inner Join Sub_niveles On OdtSubNiveles.sub_niveles_id = Sub_niveles.sub_niveles_id;"; 
                SqlCommand com = new SqlCommand(query1, con);
                SqlDataReader rdr = com.ExecuteReader();
                while (rdr.Read())
                {
                    lst.Add(new Context.OdtSubNiveles
                    {
                        OdtSubNiveles_id = Convert.ToInt32(rdr["OdtSubNiveles_id"] is DBNull ? 0: rdr["OdtSubNiveles_id"]),
                        odts_id = Convert.ToInt32(rdr["odts_id"] is DBNull ? 0: rdr["odts_id"]),
                        sub_niveles_id = Convert.ToInt32(rdr["sub_niveles_id"] is DBNull ? 0: rdr["sub_niveles_id"]),
                        sub_niveles_nombre = Convert.ToString(rdr["sub_niveles_nombre"] is DBNull ? 0: rdr["sub_niveles_nombre"]),
                        sub_niveles_codigo = Convert.ToString(rdr["sub_niveles_codigo"] is DBNull ? 0: rdr["sub_niveles_codigo"]),
                        niveles_id = Convert.ToInt32(rdr["niveles_id"] is DBNull ? 0: rdr["niveles_id"]),
                        
                    });
                }
                return Json(lst, JsonRequestBehavior.AllowGet);
            } 
        }
        
        //Obtener NivelesOdts
        public JsonResult getNivelesOdts()
        {
            List<Context.NivelesOdt> lst = new List<Context.NivelesOdt>();
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                string query1 = @"select NivelesOdts_id, odts_id, NivelesOdts.niveles_id, Niveles.niveles_titulo, Niveles.niveles_codigo from NivelesOdts Inner Join Niveles On NivelesOdts.niveles_id = Niveles.niveles_id;"; 
                SqlCommand com = new SqlCommand(query1, con);
                SqlDataReader rdr = com.ExecuteReader();
                while (rdr.Read())
                {
                    lst.Add(new Context.NivelesOdt
                    {
                        NivelesOdts_id = Convert.ToInt32(rdr["NivelesOdts_id"] is DBNull ? 0: rdr["NivelesOdts_id"]),
                        odts_id = Convert.ToInt32(rdr["odts_id"] is DBNull ? 0: rdr["odts_id"]),
                        niveles_id = Convert.ToInt32(rdr["niveles_id"] is DBNull ? 0: rdr["niveles_id"]),
                        niveles_titulo = Convert.ToString(rdr["niveles_titulo"] is DBNull ? 0: rdr["niveles_titulo"]),
                        niveles_codigo = Convert.ToString(rdr["niveles_codigo"] is DBNull ? 0: rdr["niveles_codigo"]),
                        
                    });
                }
                return Json(lst, JsonRequestBehavior.AllowGet);
            } 
        }

        public JsonResult getTareas()
        {
            db.Configuration.LazyLoadingEnabled = false;
            var tareas = db.Tareas.ToList();
            return Json(tareas, JsonRequestBehavior.AllowGet);
        }

        public JsonResult getSubTareas()
        {
            db.Configuration.LazyLoadingEnabled = false;
            var subTareas = db.SubTareas.ToList();
            return Json(subTareas, JsonRequestBehavior.AllowGet);
        }

        public JsonResult getNiveles()
        {
            db.Configuration.LazyLoadingEnabled = false;
            var niveles = db.Niveles.ToList();
            return Json(niveles, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getSubNiveles()
        {
            db.Configuration.LazyLoadingEnabled = false;
            var subNiveles = db.SubNiveles.ToList();
            return Json(subNiveles, JsonRequestBehavior.AllowGet);
        }

        public JsonResult getUsers()
        {
            List<Context.Users> lst = new List<Context.Users>();
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                string query1 = @"select Id, UserName, PhoneNumber, holding_id, negocio_id, gerencia_id, area_id, Cargo, Rol, Email from ApplicationUsers"; 
                SqlCommand com = new SqlCommand(query1, con);
                SqlDataReader rdr = com.ExecuteReader();
                while (rdr.Read())
                {
                    lst.Add(new Context.Users
                    {
                        idUser = Convert.ToInt32(rdr["Id"] is DBNull ? 0: rdr["Id"]),
                        UserName = Convert.ToString(rdr["UserName"] is DBNull ? 0: rdr["UserName"]),
                        PhoneNumber = Convert.ToString(rdr["PhoneNumber"] is DBNull ? 0: rdr["PhoneNumber"]),
                        holding_id = Convert.ToInt32(rdr["holding_id"] is DBNull ? 0: rdr["holding_id"]),
                        negocio_id = Convert.ToInt32(rdr["negocio_id"] is DBNull ? 0: rdr["negocio_id"]),
                        gerencia_id = Convert.ToInt32(rdr["gerencia_id"] is DBNull ? 0: rdr["gerencia_id"]),
                        area_id = Convert.ToInt32(rdr["area_id"] is DBNull ? 0: rdr["area_id"]),
                        Cargo = Convert.ToString(rdr["Cargo"] is DBNull ? 0: rdr["Cargo"]),
                        Rol = Convert.ToString(rdr["Rol"] is DBNull ? 0: rdr["Rol"]),
                        Email = Convert.ToString(rdr["Email"] is DBNull ? 0: rdr["Email"]),
                        
                    });
                }
                return Json(lst, JsonRequestBehavior.AllowGet);
            } 
        }

        public JsonResult getHolding()
        {
            List<Context.Holding> lst = new List<Context.Holding>();
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                string query1 = @"select holding_id, holding_nombre from Holdings";
                SqlCommand com = new SqlCommand(query1, con);
                SqlDataReader rdr = com.ExecuteReader();
                while (rdr.Read())
                {
                    lst.Add(new Context.Holding
                    {
                        holding_id = Convert.ToInt32(rdr["holding_id"] is DBNull ? 0 : rdr["holding_id"]),
                        holding_nombre = Convert.ToString(rdr["holding_nombre"] is DBNull ? 0 : rdr["holding_nombre"]),

                    });
                }
                return Json(lst, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult getNegocio()
        {
            List<Context.Negocio> lst = new List<Context.Negocio>();
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                string query1 = @"select negocio_id, negocio_nombre, holding_id from Negocios";
                SqlCommand com = new SqlCommand(query1, con);
                SqlDataReader rdr = com.ExecuteReader();
                while (rdr.Read())
                {
                    lst.Add(new Context.Negocio
                    {
                        negocio_id = Convert.ToInt32(rdr["negocio_id"] is DBNull ? 0 : rdr["negocio_id"]),
                        negocio_nombre = Convert.ToString(rdr["negocio_nombre"] is DBNull ? 0 : rdr["negocio_nombre"]),
                        holding_id = Convert.ToInt32(rdr["holding_id"] is DBNull ? 0 : rdr["holding_id"]),

                    });
                }
                return Json(lst, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult getGerencia()
        {
            List<Context.Gerencia> lst = new List<Context.Gerencia>();
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                string query1 = @"select gerencia_id, gerencia_nombre, negocio_id from Gerencias";
                SqlCommand com = new SqlCommand(query1, con);
                SqlDataReader rdr = com.ExecuteReader();
                while (rdr.Read())
                {
                    lst.Add(new Context.Gerencia
                    {
                        gerencia_id = Convert.ToInt32(rdr["gerencia_id"] is DBNull ? 0 : rdr["gerencia_id"]),
                        gerencia_nombre = Convert.ToString(rdr["gerencia_nombre"] is DBNull ? 0 : rdr["gerencia_nombre"]),
                        negocio_id = Convert.ToInt32(rdr["negocio_id"] is DBNull ? 0 : rdr["negocio_id"]),                      

                    });
                }
                return Json(lst, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult getArea()
        {
            List<Context.Area> lst = new List<Context.Area>();
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                string query1 = @"select Areas.area_id, Areas.area_nombre, Areas.responsable_id, Areas.gerencia_id, ApplicationUsers.UserName, ApplicationUsers.Cargo  from Areas inner join ApplicationUsers On Areas.responsable_id = ApplicationUsers.Id";
                SqlCommand com = new SqlCommand(query1, con);
                SqlDataReader rdr = com.ExecuteReader();
                while (rdr.Read())
                {
                    lst.Add(new Context.Area
                    {
                        area_id = Convert.ToInt32(rdr["area_id"] is DBNull ? 0 : rdr["area_id"]),
                        area_nombre = Convert.ToString(rdr["area_nombre"] is DBNull ? 0 : rdr["area_nombre"]),
                        gerencia_id = Convert.ToInt32(rdr["gerencia_id"] is DBNull ? 0 : rdr["gerencia_id"]),
                        responsable_id = Convert.ToString(rdr["responsable_id"] is DBNull ? 0 : rdr["responsable_id"]),                                               
                        UserName = Convert.ToString(rdr["UserName"] is DBNull ? 0 : rdr["UserName"]),                                               
                        Cargo = Convert.ToString(rdr["Cargo"] is DBNull ? 0 : rdr["Cargo"]),                                               
                    });
                }
                return Json(lst, JsonRequestBehavior.AllowGet);
            }
        }

        internal class Helper
        {
            public static string EncodePassword(string originalPassword)
            {
                SHA1 sha1 = new SHA1CryptoServiceProvider();

                byte[] inputBytes = (new UnicodeEncoding()).GetBytes(originalPassword);
                byte[] hash = sha1.ComputeHash(inputBytes);

                return Convert.ToBase64String(hash);
            }
        }
        private ApplicationSignInManager _signInManager;
        public ApiController()
        {
        }

        public ApiController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            
            SignInManager = signInManager;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }
        [HttpPost]
        [AllowAnonymous]
        public async Task<JsonResult> postLoginAsync(string chave, string password)
        {
            var user = db.Users.FirstOrDefault(u => u.Chave == chave);

            var result = await SignInManager.PasswordSignInAsync(user.UserName, password, false,
                        shouldLockout: false);
            Context.Users lst = new Context.Users();
            switch (result)
            {
                case SignInStatus.Success:
                    using (SqlConnection con = new SqlConnection(cs))
                    {
                        con.Open();
                        string query1 = @"select Id, UserName, PhoneNumber, holding_id, negocio_id, gerencia_id, area_id, Cargo, Rol, Email from ApplicationUsers where Chave = '" + chave + "'";
                        SqlCommand com = new SqlCommand(query1, con);
                        SqlDataReader rdr = com.ExecuteReader();
                        while (rdr.Read())
                        {

                            lst.idUser = Convert.ToInt32(rdr["Id"] is DBNull ? 0 : rdr["Id"]);
                            lst.UserName = Convert.ToString(rdr["UserName"] is DBNull ? 0 : rdr["UserName"]);
                            lst.PhoneNumber = Convert.ToString(rdr["PhoneNumber"] is DBNull ? 0 : rdr["PhoneNumber"]);
                            lst.holding_id = Convert.ToInt32(rdr["holding_id"] is DBNull ? 0 : rdr["holding_id"]);
                            lst.negocio_id = Convert.ToInt32(rdr["negocio_id"] is DBNull ? 0 : rdr["negocio_id"]);
                            lst.gerencia_id = Convert.ToInt32(rdr["gerencia_id"] is DBNull ? 0 : rdr["gerencia_id"]);
                            lst.area_id = Convert.ToInt32(rdr["area_id"] is DBNull ? 0 : rdr["area_id"]);
                            lst.Cargo = Convert.ToString(rdr["Cargo"] is DBNull ? 0 : rdr["Cargo"]);
                            lst.Rol = Convert.ToString(rdr["Rol"] is DBNull ? 0 : rdr["Rol"]);
                            lst.Email = Convert.ToString(rdr["Email"] is DBNull ? 0 : rdr["Email"]);


                        }
                        return Json(lst, JsonRequestBehavior.AllowGet);
                    }
                case SignInStatus.LockedOut:
                    return Json(2);
                case SignInStatus.RequiresVerification:
                    return Json(3);
                case SignInStatus.Failure:
                    return Json(4);
                default:
                    return Json("ERROR");
            }

        }
    }
}