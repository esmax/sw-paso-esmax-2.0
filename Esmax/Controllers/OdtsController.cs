using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Helpers;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Esmax.Models
{
    public class OdtsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        [Authorize]
        public ActionResult Redirigir()
        {
            return View("ingresarOdt", "_Layout");
        }
        [Authorize]
        public ActionResult Index()
        {
            return View("Index","_Layout");
        }

        [HttpPost]
        [Authorize]
        public JsonResult addOdt(
            Odts odt,
            List<Tareas> tareas, 
            List<Sub_tareas> subTareas,
            List<Niveles> niveles,
            List<Sub_niveles> subNiveles
            )
        {

            //Crear Odt y guardar odt creada en una variable para utilizar su id
            var odt_guardada = db.Odts.Add(new Odts
            {
                odts_tipo = odt.odts_tipo,
                odts_descripcion = odt.odts_descripcion,
                categorias_id = odt.categorias_id,
                created_at = DateTime.Now
            });
            db.SaveChanges();

            //Guardar OdtTatreas
            foreach (var tarea in tareas)
            {
                db.OdtTareas.Add(new OdtTareas
                {
                    odts_id = odt_guardada.odts_id,
                    tareas_id = tarea.tareas_id,
                });  
                db.SaveChanges();
            }
            
            //Guardar OdtSubTareas
            foreach (var subTarea in subTareas)
            {
                db.OdtSubTareas.Add(new OdtSubTareas
                {
                    odts_id = odt_guardada.odts_id,
                    sub_tareas_id = subTarea.sub_tareas_id,
                });  
                db.SaveChanges();
            }
            
            //Guardar OdtNiveles
            foreach (var nivel in niveles)
            {
                db.NivelesOdts.Add(new NivelesOdts
                {
                    odts_id = odt_guardada.odts_id,
                    niveles_id = nivel.niveles_id,
                });  
                db.SaveChanges();
            }
            
            //Guardar OdtSubNiveles
            foreach (var subNivel in subNiveles)
            {
                db.OdtSubNiveles.Add(new OdtSubNiveles
                {
                    odts_id = odt_guardada.odts_id,
                    sub_niveles_id = subNivel.sub_niveles_id,
                });  
                db.SaveChanges();
            }
             
            
            return Json("saved", JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult revisar()
        {
            return View("revisar","_Layout");
        }

        [HttpGet]
        [Authorize]
        public ActionResult getOdt()
        {
            db.Configuration.ProxyCreationEnabled = false;
            List<Odts> odts = db.Odts.ToList();
            return Json(odts, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [Authorize]
        public JsonResult findOdt(int id)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var odt = db.Odts.Where(x => x.odts_id == id).FirstOrDefault();
            return Json(odt);
        }

        [HttpGet]
        [Authorize]
        public ActionResult ingresarOdt(int id)
        {
            ViewBag.odt_id = id;
            return View("ingresarOdt","_Layout");
        }

        [HttpPost]
        [Authorize]
        public JsonResult datosOdt(int id)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var odt = db.Odts.GroupJoin(db.OdtTareas, od => od.odts_id,
                ot => ot.odts_id,  (od, ot) => new {od, ot}).FirstOrDefault(x => x.od.odts_id == id);
            return Json(odt, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        [Authorize]
        public ActionResult modificarOdt(int id)
        {
            ViewBag.id = id;
            return View("modificarOdt","_Layout");
        }
        
        [HttpPost]
        [Authorize]
        public JsonResult findOdtWhereCategorias(int id)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var odt = db.Odts.Where(x => x.categorias_id == id).ToList();
            return Json(odt);
        }
        

        [Authorize]
        public JsonResult editOdt(int id)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var odt1 = db.Odts.Where(x => x.odts_id == id);
            return Json(odt1, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult roles()
        {
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(db));
            
            //crear rol
            //var resultado = roleManager.Create(new IdentityRole("ADMINISTRADOR"));
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));
            var id = User.Identity.GetUserId();
            //agregar rol a usuario
            var resultado = userManager.AddToRole(id, "ADMINISTRADOR");
            return Json(resultado, JsonRequestBehavior.AllowGet);
            
            
        }

        [HttpPost]
        [Authorize]
        public JsonResult modificarOdt(Odts odts)
        {
            db.Entry(odts).State = EntityState.Modified;
            db.SaveChanges();

            return Json("modified", JsonRequestBehavior.AllowGet);
        }
        
        [HttpGet]
        [Authorize]
        public ActionResult deshabilitar(int id)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var odt = db.Odts.Where(o => o.odts_id == id).FirstOrDefault();

            if (odt.estado == "habilitado")
            {
                odt.estado = "deshabilitado";
                db.SaveChanges();
            }else if (odt.estado == "deshabilitado")
            {
                odt.estado = "habilitado";
                db.SaveChanges();
            }

            return RedirectToAction("revisar", "Odts");
        }
        
        [Authorize]
        public ActionResult GraficosOdt()
        {
            return View("GraficosOdt", "_Layout");
        }
    }
}