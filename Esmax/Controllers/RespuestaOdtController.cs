﻿using Esmax.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;

namespace Esmax.Controllers
{
    public partial class sn
    {
        public int NivelesOdts_id { get; set; }
        public int OdtSubNiveles_id { get; set; }
        public string sub_niveles_codigo { get; set; }
        public string sub_niveles_nombre { get; set; }

    }
    public class usuarios
    {
        public string nombre_user { get; set; }
        public int total_odt { get; set; }
    }

    public class causaDatos
    {
        public int id { get; set; }
        public int total { get; set; }
    }

    public class odt
    {
        public string odtName { get; set; }
        public int total { get; set; }
    }
    
    public class gerencias
    {
        public string nombre { get; set; }
        public int total { get; set; }
    }
    
    
    public class planAccion
    {
        public int NivelesOdts_id { get; set; }
        public int causa_raiz_id { get; set; }
        public string accion { get; set; }
        public string responsable_id { get; set; }
        public int respuesta_odt_id { get; set; }
        public int cumplimiento { get; set; }

    }

    public class datosGenerales
    {
        public int holding_id { get; set; }
        public int negocio_id { get; set; }
        public int gerencia_id { get; set; }
        public int area_id { get; set; }
        public string observador { get; set; }
        public int anonimo { get; set; }
        public string ec { get; set; }
        public string empresa { get; set; }
        public string contratista { get; set; }
        public string trabajo { get; set; }
        public string turno { get; set; }
        public string observado_id { get; set; }
        public DateTime fecha { get; set; }
        public string hora { get; set; }
    }

    public class tareas
    {
        public tareas()
        {
            
        }

        public int OdtTareas_id { get; set; }
        public int tareas_id { get; set; }
        public string tareas_titulo { get; set; }

    }

    public class respuesta
    {
        public int id { get; set; }
        public string holdingName { get; set; }
        public string negocioName { get; set; }
        public string gerenciaName { get; set; }
        public string areaName { get; set; }
    }


    
    public class RespuestaOdtController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        [HttpPost]
        [AllowAnonymous]
        public JsonResult Respuesta(
                int id,
                respuesta respuestOdt1,
                datosGenerales datosGenerales,
                List<Condiciones> condiciones,
                List<Observaciones> observaciones,
                Analisis analisis,
                List<planAccion> planAccion,
                List<sn> subNiveles,
                List<Desvios> desvios,
                List<tareas> tareas
                
            )
        {
            db.Configuration.ProxyCreationEnabled = false;
            var odt = db.Odts.Where(x => x.odts_id == id).FirstOrDefault();
            var categoria = db.Categorias.Where(x => x.categorias_id == odt.categorias_id).FirstOrDefault();

            //Crear RespuestaOdt y guardar en una variable para utilizar su id
            var respuestaOdt = db.RespuestaOdts.Add(new RespuestaOdt
            {
                odts_id = id,
                odtName = odt.odts_tipo,
                categoriaName = categoria.categorias_nombre,
                categoriaId = categoria.categorias_id,
                holdingName = respuestOdt1.holdingName,
                negocioName = respuestOdt1.negocioName,
                gerenciaName = respuestOdt1.gerenciaName,
                areaName = respuestOdt1.areaName,
                created_at = datosGenerales.fecha
            });
            db.SaveChanges();

            //Guardar DatosGenerales
            db.DatosGenerales.Add(new DatosGenerales
            {
                holding_id = datosGenerales.holding_id,
                negocio_id = datosGenerales.negocio_id,
                gerencia_id = datosGenerales.gerencia_id,
                area_id = datosGenerales.area_id,
                observador_id = datosGenerales.observador,
                anonimo = datosGenerales.anonimo,
                observado_id = datosGenerales.observado_id,
                respuesta_odt_id = respuestaOdt.respuesta_odt_id,
                fecha = datosGenerales.fecha,
                ec = datosGenerales.ec,
                empresa=datosGenerales.empresa,
                contratista=datosGenerales.contratista,
                trabajo = datosGenerales.trabajo,
                turno = datosGenerales.turno,
                created_at = DateTime.Now,
                hora = datosGenerales.hora
            });
            db.SaveChanges();
            
            foreach (var tarea in tareas)
            {
                db.TareasCondiciones.Add(new TareasCondiciones
                {
                    OdtTareas_id = tarea.OdtTareas_id,
                    tareas_id = tarea.tareas_id,
                    tareas_titulo = tarea.tareas_titulo,
                    respuesta_odt_id = respuestaOdt.respuesta_odt_id,
                });
                db.SaveChanges();
            }

            //Guardar Condiciones
            foreach (var condicion in condiciones)
            {
                db.Condiciones.Add(new Condiciones
                {
                    OdtSubTareas_id = condicion.OdtSubTareas_id,
                    valor = condicion.valor,
                    respuesta_odt_id = respuestaOdt.respuesta_odt_id,
                    created_at = DateTime.Now
                });
                db.SaveChanges();
            }

            //Guardar Observaciones
            foreach (var observacion in observaciones)
            {
                db.Observaciones.Add(new Observaciones
                {
                    OdtSubNiveles_id = observacion.OdtSubNiveles_id,
                    NA = observacion.NA,
                    correcto = observacion.correcto,
                    desvio = observacion.desvio,
                    comentario = observacion.comentario,
                    respuesta_odt_id = respuestaOdt.respuesta_odt_id,
                    created_at = DateTime.Now,
                    sub_niveles_codigo = observacion.sub_niveles_codigo,
                    sub_niveles_nombre = observacion.sub_niveles_nombre,
                    sub_niveles_id = observacion.sub_niveles_id,
                    niveles_id = observacion.niveles_id
                });
                db.SaveChanges();
            }

            //Guardar Analisis
            db.Analisis.Add(new Analisis
            {
                comentarios = analisis.comentarios,
                conclusiones = analisis.conclusiones,
                retroalimentacion = analisis.retroalimentacion,
                fecha_sesion = analisis.fecha_sesion,
                respuesta_odt_id = respuestaOdt.respuesta_odt_id,
                created_at = DateTime.Now
            });
            db.SaveChanges();

            if (planAccion != null)
            {
                //Guardar PlanAccion
                foreach (var plan in planAccion)
                {
                    var pa = db.PlanAccion.Add(new Models.PlanAccion
                    {
                        NivelesOdts_id = plan.NivelesOdts_id,
                        causa_raiz_id = plan.causa_raiz_id,
                        accion = plan.accion,
                        responsable_id = plan.responsable_id,
                        cumplimiento = datosGenerales.fecha.AddDays(Convert.ToDouble(plan.cumplimiento)),
                        respuesta_odt_id = respuestaOdt.respuesta_odt_id,
                        created_at = DateTime.Now,
                        modulo = 1
                    });
                    db.SaveChanges();

                    foreach (var sub in subNiveles)
                    {
                        if (sub.NivelesOdts_id == pa.NivelesOdts_id)
                        {
                            db.PlanAccion_OdtSubNiveles.Add(new PlanAccion_OdtSubNiveles
                            {
                                OdtSubNiveles_id = sub.OdtSubNiveles_id,
                                plan_accion_id = pa.plan_accion_id,
                                NivelesOdts_id = sub.NivelesOdts_id,
                                sub_niveles_codigo = sub.sub_niveles_codigo,
                                sub_niveles_nombre = sub.sub_niveles_nombre
                            });
                            db.SaveChanges();
                        }
                    }

                    db.Notificaciones.Add(new Notificaciones
                    {
                        descripcion = "Tienes un nuevo plan de acción asociado a ODT. ''" + pa.accion + "''",
                        modulo = 1,
                        plan_accion_id = pa.plan_accion_id,
                        UserId = pa.responsable_id,
                        created_at = DateTime.Now
                    });
                    db.SaveChanges();

                    var user = db.Users.Where(x => x.Id == pa.responsable_id).FirstOrDefault();
                    try
                    {
                        MailMessage correo = new MailMessage();
                        correo.From = new MailAddress("sistemapaso@esmax.cl"); //correo que usara la app
                        correo.To.Add(user.Email);
                        correo.Subject = "Tienes un nuevo plan de accion asociado";
                        correo.Body = user.UserName +
                                      ", <br/><br/> Tiene un nuevo plan de acción asociado a ODT, Número de folio: " +
                                      pa.plan_accion_id +
                                      " <br/><br/> Haz click aqui para acceder: http://paso.esmax.cl/MplanAccion/ver/" +
                                      pa.plan_accion_id + "<br><br><br> Por favor no responder este correo.";
                        correo.IsBodyHtml = true;
                        correo.Priority = MailPriority.Normal;

                        SmtpClient smtp = new SmtpClient();
                        smtp.Host = "smtp.esmax.cl";
                        smtp.Port = 25;
                        smtp.EnableSsl = true;
                        smtp.UseDefaultCredentials = true;
                        string sCuentaCorreo = "sistemapaso@esmax.cl";
                        string sPasswordCorreo = "";
                        smtp.Credentials = new System.Net.NetworkCredential(sCuentaCorreo, sPasswordCorreo);

                        smtp.Send(correo);
                        ViewBag.Mensaje = "Mensaje enviado correctamente";
                    }
                    catch (Exception e)
                    {
                        ViewBag.Error = e.Message;
                    }
                }

                foreach (var desvio in desvios)
                {
                    db.Desvios.Add(new Desvios
                    {
                        niveles_id = desvio.niveles_id,
                        NivelesOdts_id = desvio.NivelesOdts_id,
                        respuesta_odt_id = respuestaOdt.respuesta_odt_id
                    });
                    db.SaveChanges();
                }
            }
            return Json("saved", JsonRequestBehavior.AllowGet);
        }
        
        [HttpPost]
        [Authorize]
        public JsonResult Respuesta2(
                int id,
                respuesta respuestOdt1,
                datosGenerales datosGenerales,
                List<Condiciones> condiciones,
                List<Observaciones> observaciones,
                Analisis analisis,
                List<planAccion> planAccion,
                List<sn> subNiveles,
                List<Desvios> desvios,
                List<tareas> tareas
                
            )
        {
            db.Configuration.ProxyCreationEnabled = false;
            var odt = db.Odts.Where(x => x.odts_id == id).FirstOrDefault();
            var categoria = db.Categorias.Where(x => x.categorias_id == odt.categorias_id).FirstOrDefault();

            //Crear RespuestaOdt y guardar en una variable para utilizar su id
            var respuestaOdt = db.RespuestaOdts.Add(new RespuestaOdt
            {
                odts_id = id,
                odtName = odt.odts_tipo,
                categoriaName = categoria.categorias_nombre,
                categoriaId = categoria.categorias_id,
                holdingName = respuestOdt1.holdingName,
                negocioName = respuestOdt1.negocioName,
                gerenciaName = respuestOdt1.gerenciaName,
                areaName = respuestOdt1.areaName,
                created_at = datosGenerales.fecha
            });
            db.SaveChanges();

            //Guardar DatosGenerales
            db.DatosGenerales.Add(new DatosGenerales
            {
                holding_id = datosGenerales.holding_id,
                negocio_id = datosGenerales.negocio_id,
                gerencia_id = datosGenerales.gerencia_id,
                area_id = datosGenerales.area_id,
                observador_id = datosGenerales.observador,
                anonimo = datosGenerales.anonimo,
                observado_id = datosGenerales.observado_id,
                respuesta_odt_id = respuestaOdt.respuesta_odt_id,
                fecha = datosGenerales.fecha,
                ec = datosGenerales.ec,
                empresa=datosGenerales.empresa,
                contratista=datosGenerales.contratista,
                trabajo = datosGenerales.trabajo,
                turno = datosGenerales.turno,
                created_at = DateTime.Now,
                hora = datosGenerales.hora
            });
            db.SaveChanges();
            
            foreach (var tarea in tareas)
            {
                db.TareasCondiciones.Add(new TareasCondiciones
                {
                    OdtTareas_id = tarea.OdtTareas_id,
                    tareas_id = tarea.tareas_id,
                    tareas_titulo = tarea.tareas_titulo,
                    respuesta_odt_id = respuestaOdt.respuesta_odt_id,
                });
                db.SaveChanges();
            }

            //Guardar Condiciones
            foreach (var condicion in condiciones)
            {
                db.Condiciones.Add(new Condiciones
                {
                    OdtSubTareas_id = condicion.OdtSubTareas_id,
                    valor = condicion.valor,
                    respuesta_odt_id = respuestaOdt.respuesta_odt_id,
                    created_at = DateTime.Now
                });
                db.SaveChanges();
            }

            //Guardar Observaciones
            foreach (var observacion in observaciones)
            {
                db.Observaciones.Add(new Observaciones
                {
                    OdtSubNiveles_id = observacion.OdtSubNiveles_id,
                    NA = observacion.NA,
                    correcto = observacion.correcto,
                    desvio = observacion.desvio,
                    comentario = observacion.comentario,
                    respuesta_odt_id = respuestaOdt.respuesta_odt_id,
                    created_at = DateTime.Now,
                    sub_niveles_codigo = observacion.sub_niveles_codigo,
                    sub_niveles_nombre = observacion.sub_niveles_nombre,
                    sub_niveles_id = observacion.sub_niveles_id,
                    niveles_id = observacion.niveles_id
                });
                db.SaveChanges();
            }

            //Guardar Analisis
            db.Analisis.Add(new Analisis
            {
                comentarios = analisis.comentarios,
                conclusiones = analisis.conclusiones,
                retroalimentacion = analisis.retroalimentacion,
                fecha_sesion = analisis.fecha_sesion,
                respuesta_odt_id = respuestaOdt.respuesta_odt_id,
                created_at = DateTime.Now
            });
            db.SaveChanges();

            if (planAccion != null)
            {
                //Guardar PlanAccion
                foreach (var plan in planAccion)
                {
                    var pa = db.PlanAccion.Add(new Models.PlanAccion
                    {
                        NivelesOdts_id = plan.NivelesOdts_id,
                        causa_raiz_id = plan.causa_raiz_id,
                        accion = plan.accion,
                        responsable_id = plan.responsable_id,
                        cumplimiento = datosGenerales.fecha.AddDays(Convert.ToDouble(plan.cumplimiento)),
                        respuesta_odt_id = respuestaOdt.respuesta_odt_id,
                        created_at = DateTime.Now,
                        modulo = 1
                    });
                    db.SaveChanges();

                    foreach (var sub in subNiveles)
                    {
                        if (sub.NivelesOdts_id == pa.NivelesOdts_id)
                        {
                            db.PlanAccion_OdtSubNiveles.Add(new PlanAccion_OdtSubNiveles
                            {
                                OdtSubNiveles_id = sub.OdtSubNiveles_id,
                                plan_accion_id = pa.plan_accion_id,
                                NivelesOdts_id = sub.NivelesOdts_id,
                                sub_niveles_codigo = sub.sub_niveles_codigo,
                                sub_niveles_nombre = sub.sub_niveles_nombre
                            });
                            db.SaveChanges();
                        }
                    }

                    db.Notificaciones.Add(new Notificaciones
                    {
                        descripcion = "Tienes un nuevo plan de acción asociado a ODT. ''" + pa.accion + "''",
                        modulo = 1,
                        plan_accion_id = pa.plan_accion_id,
                        UserId = pa.responsable_id,
                        created_at = DateTime.Now
                    });
                    db.SaveChanges();

                    var user = db.Users.Where(x => x.Id == pa.responsable_id).FirstOrDefault();
                    try
                    {
                        MailMessage correo = new MailMessage();
                        correo.From = new MailAddress("paso.esmax@gmail.com"); //correo que usara la app
                        correo.To.Add(user.Email);
                        correo.Subject = "Tienes un nuevo plan de accion asociado";
                        correo.Body = user.UserName +
                                      ", <br/><br/> Tiene un nuevo plan de acción asociado a ODT, Número de folio: " +
                                      pa.plan_accion_id +
                                      " <br/><br/> Haz click aqui para acceder: http://paso.esmax.cl/MplanAccion/ver/" +
                                      pa.plan_accion_id + "<br><br><br> Por favor no responder este correo.";
                        correo.IsBodyHtml = true;
                        correo.Priority = MailPriority.Normal;

                        SmtpClient smtp = new SmtpClient();
                        smtp.Host = "smtp.gmail.com";
                        smtp.Port = 25;
                        smtp.EnableSsl = true;
                        smtp.UseDefaultCredentials = true;
                        string sCuentaCorreo = "paso.esmax@gmail.com";
                        string sPasswordCorreo = "esmax2019";
                        smtp.Credentials = new System.Net.NetworkCredential(sCuentaCorreo, sPasswordCorreo);

                        smtp.Send(correo);
                        ViewBag.Mensaje = "Mensaje enviado correctamente";
                    }
                    catch (Exception e)
                    {
                        ViewBag.Error = e.Message;
                    }
                }

                foreach (var desvio in desvios)
                {
                    db.Desvios.Add(new Desvios
                    {
                        niveles_id = desvio.niveles_id,
                        NivelesOdts_id = desvio.NivelesOdts_id,
                        respuesta_odt_id = respuestaOdt.respuesta_odt_id
                    });
                    db.SaveChanges();
                }
            }
            return Json("saved", JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        [Authorize]
        public ActionResult listRespuestas()
        {
            return View("listRespuestas","_Layout");
        }

        [HttpGet]
        [Authorize]
        public ActionResult allRespuestas()
        {
            db.Configuration.ProxyCreationEnabled = false;
            List<RespuestaOdt> lista = db.RespuestaOdts.ToList();
                      

            return Json(lista, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [Authorize]
        public JsonResult findRespuestasF(DateTime desde, DateTime hasta)
        {
            db.Configuration.LazyLoadingEnabled = false;
            List<RespuestaOdt> respuesta = db.RespuestaOdts.Where(x => x.created_at >= desde).Where(x => x.created_at <= hasta).ToList();
            return Json(respuesta);
        }
        
        [HttpGet]
        [Authorize]
        public ActionResult ver(int id)
        {
            ViewBag.respuestaId = id;
            return View("ver","_Layout");
        }
        
        [HttpPost]
        [Authorize]
        public JsonResult getRespuesta(int id)
        {
            db.Configuration.LazyLoadingEnabled = false;
            var respuesta = db.RespuestaOdts.Where(x => x.respuesta_odt_id == id).FirstOrDefault();
            return Json(respuesta);
        }
        
        [HttpPost]
        [Authorize]
        public JsonResult getRespuestaFecha(DateTime desde, DateTime hasta)
        {
            
            db.Configuration.LazyLoadingEnabled = false;
            List<RespuestaOdt> respuesta = db.RespuestaOdts.Where(x => x.created_at >= desde).Where(x => x.created_at <= hasta).ToList();
            return Json(respuesta);
        }
        string cs = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        [HttpPost]
        [Authorize]
        public JsonResult getRespuestaCausa(string desde, string hasta)
        {
            List<causaDatos> lst = new List<causaDatos>();
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                string query = @"SELECT causa_raiz_id, COUNT(causa_raiz_id) as total FROM PlanAccions where created_at >= @desde AND created_at <= @hasta and modulo = 1  group by causa_raiz_id"; 
                SqlCommand com = new SqlCommand(query, con);
                com.Parameters.AddWithValue("@desde", desde);
                com.Parameters.AddWithValue("@hasta", hasta);
                SqlDataReader rdr = com.ExecuteReader();
                while (rdr.Read())
                {
                    lst.Add(new causaDatos
                    {
                        id = Convert.ToInt32(rdr["causa_raiz_id"] is DBNull ? 0: rdr["causa_raiz_id"]),
                        total = Convert.ToInt32(rdr["total"] is DBNull ? 0: rdr["total"]),
                        
                    });
                }
                return Json(lst);
            } 
        }
        
        
        [HttpPost]
        [Authorize]
        public JsonResult getOdtFecha(string desde, string hasta)
        {
            List<odt> lst = new List<odt>();
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                string query = @"select odtName, COUNT(odts_id) as total  from RespuestaOdts  where created_at >= @desde and created_at <= @hasta group by odts_id, odtName;"; 
                SqlCommand com = new SqlCommand(query, con);
                com.Parameters.AddWithValue("@desde", desde);
                com.Parameters.AddWithValue("@hasta", hasta);
                SqlDataReader rdr = com.ExecuteReader();
                while (rdr.Read())
                {
                    lst.Add(new odt
                    {
                        odtName = Convert.ToString(rdr["odtName"] is DBNull ? 0: rdr["odtName"]),
                        total = Convert.ToInt32(rdr["total"] is DBNull ? 0: rdr["total"])
                        
                    });
                }
                return Json(lst);
            } 
        }

        [HttpPost]
        [Authorize]
        public JsonResult getRespuestaGerencia(string desde, string hasta)
        {
            List<gerencias> lst = new List<gerencias>();
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                string query = @"select Gerencias.gerencia_nombre, COUNT(DatosGenerales.gerencia_id) as total from Gerencias INNER JOIN DatosGenerales On Gerencias.gerencia_id = DatosGenerales.gerencia_id where DatosGenerales.created_at >= @desde and DatosGenerales.created_at <= @hasta group by Gerencias.gerencia_id, Gerencias.gerencia_nombre order by total desc "; 
                SqlCommand com = new SqlCommand(query, con);
                com.Parameters.AddWithValue("@desde", desde);
                com.Parameters.AddWithValue("@hasta", hasta);
                SqlDataReader rdr = com.ExecuteReader();
                while (rdr.Read())
                {
                    lst.Add(new gerencias
                    {
                        nombre = Convert.ToString(rdr["gerencia_nombre"] is DBNull ? 0: rdr["gerencia_nombre"]),
                        total = Convert.ToInt32(rdr["total"] is DBNull ? 0: rdr["total"]),
                        
                    });
                }
                return Json(lst);
            } 
        }
        
        [HttpPost]
        [Authorize]
        public JsonResult getRespuestaHolding(string desde, string hasta)
        {
            List<gerencias> lst = new List<gerencias>();
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                string query = @"select Holdings.holding_nombre, COUNT(DatosGenerales.holding_id) as total from DatosGenerales INNER JOIN Holdings On DatosGenerales.holding_id = Holdings.holding_id where DatosGenerales.created_at >= @desde and DatosGenerales.created_at <= @hasta group by DatosGenerales.holding_id, Holdings.holding_nombre order by total desc "; 
                SqlCommand com = new SqlCommand(query, con);
                
                com.Parameters.AddWithValue("@desde", desde);
                com.Parameters.AddWithValue("@hasta", hasta);
                SqlDataReader rdr = com.ExecuteReader();
                while (rdr.Read())
                {
                    lst.Add(new gerencias
                    {
                        nombre = Convert.ToString(rdr["holding_nombre"] is DBNull ? 0: rdr["holding_nombre"]),
                        total = Convert.ToInt32(rdr["total"] is DBNull ? 0: rdr["total"]),
                        
                    });
                }
                return Json(lst);
            } 
        }
        
        [HttpPost]
        [Authorize]
        public JsonResult getRespuestaNegocio(string desde, string hasta)
        {
            List<gerencias> lst = new List<gerencias>();
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                string query = @"select Negocios.negocio_nombre, COUNT(DatosGenerales.negocio_id) as total from DatosGenerales INNER JOIN Negocios On DatosGenerales.negocio_id = Negocios.negocio_id where DatosGenerales.created_at >= @desde and DatosGenerales.created_at <= @hasta group by DatosGenerales.negocio_id, Negocios.negocio_nombre order by total desc "; 
                SqlCommand com = new SqlCommand(query, con);
                com.Parameters.AddWithValue("@desde", desde);
                com.Parameters.AddWithValue("@hasta", hasta);
                SqlDataReader rdr = com.ExecuteReader();
                while (rdr.Read())
                {
                    lst.Add(new gerencias
                    {
                        nombre = Convert.ToString(rdr["negocio_nombre"] is DBNull ? 0: rdr["negocio_nombre"]),
                        total = Convert.ToInt32(rdr["total"] is DBNull ? 0: rdr["total"]),
                        
                    });
                }
                return Json(lst);
            } 
        }
        
        [HttpPost]
        [Authorize]
        public JsonResult getRespuestaArea(string desde, string hasta)
        {
            List<gerencias> lst = new List<gerencias>();
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                string query = @"select Areas.area_nombre, COUNT(DatosGenerales.area_id) as total from DatosGenerales INNER JOIN Areas On DatosGenerales.area_id = Areas.area_id where DatosGenerales.created_at >= @desde and DatosGenerales.created_at <= @hasta group by DatosGenerales.area_id, Areas.area_nombre order by total desc "; 
                SqlCommand com = new SqlCommand(query, con);
                com.Parameters.AddWithValue("@desde", desde);
                com.Parameters.AddWithValue("@hasta", hasta);
                SqlDataReader rdr = com.ExecuteReader();
                while (rdr.Read())
                {
                    lst.Add(new gerencias
                    {
                        nombre = Convert.ToString(rdr["area_nombre"] is DBNull ? 0: rdr["area_nombre"]),
                        total = Convert.ToInt32(rdr["total"] is DBNull ? 0: rdr["total"]),
                        
                    });
                }
                return Json(lst);
            } 
        }
        
        [HttpPost]
        [Authorize]
        public JsonResult getUsuariosResponden(string desde, string hasta)
        {
            List<usuarios> lst = new List<usuarios>();
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                string query = @"select DatosGenerales.observador_id,  ApplicationUsers.UserName, COUNT(DatosGenerales.observador_id) as total from DatosGenerales Inner Join ApplicationUsers On DatosGenerales.observador_id = ApplicationUsers.Id where DatosGenerales.created_at >= @desde and DatosGenerales.created_at <= @hasta group by observador_id, UserName order by total desc"; 
                SqlCommand com = new SqlCommand(query, con);
                com.Parameters.AddWithValue("@desde", desde);
                com.Parameters.AddWithValue("@hasta", hasta);
                SqlDataReader rdr = com.ExecuteReader();
                while (rdr.Read())
                {
                    lst.Add(new usuarios
                    {
                        nombre_user = Convert.ToString(rdr["UserName"] is DBNull ? 0: rdr["UserName"]),
                        total_odt = Convert.ToInt32(rdr["total"] is DBNull ? 0: rdr["total"]),
                        
                    });
                }
                return Json(lst);
            } 
        }
        
        
        
        
        
        [HttpPost]
        [Authorize]
        public JsonResult datosRespuesta(int id)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var rsp = db.DatosGenerales.Where(r => r.respuesta_odt_id == id).FirstOrDefault();
            

            return Json(rsp, JsonRequestBehavior.AllowGet);
        }
        
        [HttpPost]
        [Authorize]
        public JsonResult getCondiciones(int id)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var rsp = db.Condiciones.Where(r => r.respuesta_odt_id == id);

            return Json(rsp, JsonRequestBehavior.AllowGet);
        }
        
        [HttpGet]
        [Authorize]
        public JsonResult getOdtCondiciones()
        {
            db.Configuration.ProxyCreationEnabled = false;
            var rsp = db.OdtSubTareas.ToList();

            return Json(rsp, JsonRequestBehavior.AllowGet);
        }
        
        [HttpPost]
        [Authorize]
        public JsonResult getDesvios(int id)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var rsp = db.Desvios.Where(r => r.respuesta_odt_id == id);

            return Json(rsp, JsonRequestBehavior.AllowGet);
        }
        
        [HttpPost]
        [Authorize]
        public JsonResult getObservaciones(int id)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var rsp = db.Observaciones.Where(r => r.respuesta_odt_id == id);

            return Json(rsp, JsonRequestBehavior.AllowGet);
        }
        
        [HttpGet]
        [Authorize]
        public JsonResult getOSN()
        {
            db.Configuration.ProxyCreationEnabled = false;
            var rsp = db.OdtSubNiveles.ToList();

            return Json(rsp, JsonRequestBehavior.AllowGet);
        }
        
        [HttpGet]
        [Authorize]
        public JsonResult getPO()
        {
            db.Configuration.ProxyCreationEnabled = false;
            var rsp = db.PlanAccion_OdtSubNiveles.ToList();

            return Json(rsp, JsonRequestBehavior.AllowGet);
        }
        
        [HttpPost]
        [Authorize]
        public JsonResult getPlan(int id)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var rsp = db.PlanAccion.Where(r => r.respuesta_odt_id == id);

            return Json(rsp, JsonRequestBehavior.AllowGet);
        }
        
        [HttpPost]
        [Authorize]
        public JsonResult getTareasC(int id)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var rsp = db.TareasCondiciones.Where(r => r.respuesta_odt_id == id);

            return Json(rsp, JsonRequestBehavior.AllowGet);
        }
        
        [HttpPost]
        [Authorize]
        public JsonResult findRespuestaQuery(string query)
        {
            List<RespuestaOdt> lst = new List<RespuestaOdt>();
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                string query1 = @"SELECT * FROM RespuestaOdts " + query; 
                SqlCommand com = new SqlCommand(query1, con);
                SqlDataReader rdr = com.ExecuteReader();
                while (rdr.Read())
                {
                    lst.Add(new RespuestaOdt
                    {
                        respuesta_odt_id = Convert.ToInt32(rdr["respuesta_odt_id"] is DBNull ? 0: rdr["respuesta_odt_id"]),
                        odts_id = Convert.ToInt32(rdr["odts_id"] is DBNull ? 0: rdr["odts_id"]),
                        created_at = Convert.ToDateTime(rdr["created_at"] is DBNull ? 0: rdr["created_at"]),
                        holdingName = Convert.ToString(rdr["holdingName"] is DBNull ? 0: rdr["holdingName"]),
                        odtName = Convert.ToString(rdr["odtName"] is DBNull ? 0: rdr["odtName"]),
                        categoriaName = Convert.ToString(rdr["categoriaName"] is DBNull ? 0: rdr["categoriaName"]),
                        categoriaId = Convert.ToInt32(rdr["categoriaId"] is DBNull ? 0: rdr["categoriaId"]),
                        negocioName = Convert.ToString(rdr["negocioName"] is DBNull ? 0: rdr["negocioName"]),
                        gerenciaName = Convert.ToString(rdr["gerenciaName"] is DBNull ? 0: rdr["gerenciaName"]),
                        areaName = Convert.ToString(rdr["areaName"] is DBNull ? 0: rdr["areaName"]),
                        
                    });
                }
                return Json(lst);
            } 
        }
        
        
        [HttpGet]
        [Authorize]
        public JsonResult findRespuestaSinQuery()
        {
            db.Configuration.ProxyCreationEnabled = false;
            var rsp = db.RespuestaOdts.ToList();

            return Json(rsp, JsonRequestBehavior.AllowGet);
        }
    }
}