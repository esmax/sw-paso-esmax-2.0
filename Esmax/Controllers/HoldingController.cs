using System.Linq;
using System.Web.Mvc;

using Esmax.Models;

namespace Esmax.Controllers
{
    public class HoldingController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
                       
       [HttpGet]
       [Authorize]
       public ActionResult GetHolding()
       {
           db.Configuration.ProxyCreationEnabled = false;
           var holdings = db.Holdings.ToList();
           return Json(holdings, JsonRequestBehavior.AllowGet);
       } 
    }
}