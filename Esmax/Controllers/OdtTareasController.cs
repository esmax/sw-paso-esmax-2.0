using System.Collections.Generic;
using System.Web.Mvc;

namespace Esmax.Models
{
    public class OdtTareasController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        
        [HttpPost]
        [Authorize]
        public ActionResult addOdtTareas(List<Tareas> tareas)
        {

            return Json(tareas, JsonRequestBehavior.AllowGet);
        }
    }
}