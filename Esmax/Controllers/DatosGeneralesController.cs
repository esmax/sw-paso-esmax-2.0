using System.Linq;
using System.Web.Mvc;
using Esmax.Models;

namespace Esmax.Controllers
{
    public class DatosGeneralesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        [HttpPost]
        [Authorize]
        public JsonResult findDatosGenerales(int id)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var datos = db.DatosGenerales.Where(x => x.respuesta_odt_id == id).FirstOrDefault();
            return Json(datos);
        }
        
    }
}