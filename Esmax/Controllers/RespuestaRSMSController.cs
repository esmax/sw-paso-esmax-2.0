using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using Esmax.Models;
using Esmax.Models.RSMS;

namespace Esmax.Controllers
{
    public class RespuestaRSMSController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        [Authorize]
        public ActionResult allrsms()
        {
            db.Configuration.ProxyCreationEnabled = false;
            List<RespuestaRsms> lista = db.RespuestaRsms.ToList();
                      

            return Json(lista, JsonRequestBehavior.AllowGet);
        }
        string cs = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        [HttpPost]
        [Authorize]
        public ActionResult allrsmsWithQuery(string query)
        {
            List<RespuestaRsms> lst = new List<RespuestaRsms>();
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                string query1 = @"SELECT * FROM RespuestaRsms " + query; 
                SqlCommand com = new SqlCommand(query1, con);
                SqlDataReader rdr = com.ExecuteReader();
                while (rdr.Read())
                {
                    lst.Add(new RespuestaRsms
                    {
                        respuestarsms_id = Convert.ToInt32(rdr["respuestarsms_id"] is DBNull ? 0: rdr["respuestarsms_id"]),
                        categorizacion_id = Convert.ToInt32(rdr["categorizacion_id"] is DBNull ? 0: rdr["categorizacion_id"]),
                        antecedentes_id = Convert.ToInt32(rdr["antecedentes_id"] is DBNull ? 0: rdr["antecedentes_id"]),
                        descripcion_id = Convert.ToInt32(rdr["descripcion_id"] is DBNull ? 0: rdr["descripcion_id"]),
                        seguridad1 = Convert.ToInt32(rdr["seguridad1"] is DBNull ? 0: rdr["seguridad1"]),
                        medio_ambiente1 = Convert.ToInt32(rdr["medio_ambiente1"] is DBNull ? 0: rdr["medio_ambiente1"]),
                        salud1 = Convert.ToInt32(rdr["salud1"] is DBNull ? 0: rdr["salud1"]),
                        accion_insegura1 = Convert.ToInt32(rdr["accion_insegura1"] is DBNull ? 0: rdr["accion_insegura1"]),
                        condicion_insegura1 = Convert.ToInt32(rdr["condicion_insegura1"] is DBNull ? 0: rdr["condicion_insegura1"]),
                        evento_positivo1 = Convert.ToInt32(rdr["evento_positivo1"] is DBNull ? 0: rdr["evento_positivo1"]),
                        leve1 = Convert.ToInt32(rdr["leve1"] is DBNull ? 0: rdr["leve1"]),
                        moderado1 = Convert.ToInt32(rdr["moderado1"] is DBNull ? 0: rdr["moderado1"]),
                        inaceptable1 = Convert.ToInt32(rdr["inaceptable1"] is DBNull ? 0: rdr["inaceptable1"]),
                        fecha1 = Convert.ToDateTime(rdr["fecha1"] is DBNull ? 0: rdr["fecha1"]),
                        informante_nombre = Convert.ToString(rdr["informante_nombre"] is DBNull ? 0: rdr["informante_nombre"]),
                        holding_nombre = Convert.ToString(rdr["holding_nombre"] is DBNull ? 0: rdr["holding_nombre"]),
                        negocio_nombre = Convert.ToString(rdr["negocio_nombre"] is DBNull ? 0: rdr["negocio_nombre"]),
                        gerencia_nombre = Convert.ToString(rdr["gerencia_nombre"] is DBNull ? 0: rdr["gerencia_nombre"]),
                        area_nombre = Convert.ToString(rdr["area_nombre"] is DBNull ? 0: rdr["area_nombre"])
                        
                    });
                }
                return Json(lst);
            } 
        }
        
        
        
        [HttpPost]
        [Authorize]
        public JsonResult buscarrsms(int id)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var rsms = db.RespuestaRsms.Where(x => x.respuestarsms_id == id).FirstOrDefault();
            return Json(rsms);
        }
        
        [HttpPost]
        [Authorize]
        public JsonResult getAntecedentes(int id)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var antecedentes = db.Antecedentes.Where(x => x.antecedentes_id == id).FirstOrDefault();
            return Json(antecedentes);
        }
        
        [HttpPost]
        [Authorize]
        public JsonResult getNegocio(int id)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var negocio = db.Negocios.Where(x => x.negocio_id == id).FirstOrDefault();
            return Json(negocio);
        }
        
        [HttpPost]
        [Authorize]
        public JsonResult getGerencia(int id)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var gerencia = db.Gerencias.Where(x => x.gerencia_id == id).FirstOrDefault();
            return Json(gerencia);
        }
        
        [HttpPost]
        [Authorize]
        public JsonResult getDescripcion(int id)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var descripcion = db.Descripcion.Where(x => x.descripcion_id == id).FirstOrDefault();
            return Json(descripcion);
        }
        
        [HttpPost]
        [Authorize]
        public JsonResult getFiles(int id)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var files = db.Archivo.Where(x => x.id_descripcion == id).ToList();
            return Json(files);
        }
        
       [HttpGet]
       [Authorize]
        public FileResult download(int id)
        {
            var file = db.Archivo.Where(x => x.id_archivo == id).FirstOrDefault();
            var FileVirtualPath = "~/Content/Images/" + file.nombre_archivo;
            return File(FileVirtualPath, "application/force- download", Path.GetFileName(FileVirtualPath));
        }
        
        
    }
}