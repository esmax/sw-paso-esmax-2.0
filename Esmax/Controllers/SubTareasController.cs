using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Helpers;
using System.Web.Mvc;

namespace Esmax.Models
{
    public class SubTareasController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        
        [Authorize]
        public ActionResult getSubTareas()
        {
            db.Configuration.ProxyCreationEnabled = false;
            List<Sub_tareas> Sub_tareas = db.SubTareas.ToList<Sub_tareas>();
            return Json(Sub_tareas, JsonRequestBehavior.AllowGet);
        }
        
        [HttpPost]
        [Authorize]
        public ActionResult addSubTarea(Sub_tareas subTareas)
        {
            var saved = db.SubTareas.Add(new Sub_tareas
            {
                sub_tareas_nombre = subTareas.sub_tareas_nombre,
                tareas_id = subTareas.tareas_id,
                created_at = DateTime.Now
            });
            db.SaveChanges();
            return Json(saved, JsonRequestBehavior.AllowGet);
        }
        
        [HttpPost]
        [Authorize]
        public JsonResult datosSubTareas(int id)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var odt = db.Odts.GroupJoin(db.OdtSubTareas, od => od.odts_id,
                ost => ost.odts_id,  (od, ost) => new {od, ost}).FirstOrDefault(x => x.od.odts_id == id);
            return Json(odt, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [Authorize]
        public JsonResult modificarSubTarea(Sub_tareas subTareas)
        {
            db.Entry(subTareas).State = EntityState.Modified;
            db.SaveChanges();

            return Json("modified", JsonRequestBehavior.AllowGet);
        }
    }
}