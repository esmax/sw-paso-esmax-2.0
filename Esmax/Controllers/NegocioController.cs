using System.Linq;
using System.Web.Mvc;

using Esmax.Models;

namespace Esmax.Controllers
{
    public class NegocioController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        
       [HttpGet]
       [Authorize]
       public ActionResult GetNegocio()
       {
           db.Configuration.ProxyCreationEnabled = false;
           var negocios = db.Negocios.ToList();
           ViewBag.ListNegocio = negocios;
           return Json(negocios, JsonRequestBehavior.AllowGet);
       } 
    }
}