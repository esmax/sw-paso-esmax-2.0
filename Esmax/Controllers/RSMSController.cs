using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using Esmax.Models;
using Esmax.Models.RSMS;

namespace Esmax.Controllers
{
   
    public class datos
    {
        public int dato { get; set; }
        public int total { get; set; }
    }
    public class informante
    {
        public string nombre_user { get; set; }
        public int total { get; set; }
    }
    public class niveles
    {
        public string nombre { get; set; }
        public int total { get; set; }
    }
    
    
    
    public class RSMSController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        // GET
        [Authorize]
        public ActionResult IngresarRsms()
        {
            return View("IngresarRSMS", "_Layout");
        }
        [Authorize]
        public ActionResult RevisarRSMS()
        {
            return View("RevisarRSMS", "_Layout");
        }
        [HttpGet]
        [Authorize]
        public ActionResult verRsms(int id)
        {
            ViewBag.RsmsId = id;
            
            return View("VerRSMS","_Layout");
        }

        [HttpPost]
        [Authorize]
        public JsonResult actualizarEstado(int id)
        {
            var notificacion = db.Notificaciones.FirstOrDefault(x => x.rsms == id);
            notificacion.estado = "Leido";
            db.SaveChanges();
            return Json("updated", JsonRequestBehavior.AllowGet);
        }
        
        [Authorize]
        public ActionResult GraficosRSMS()
        {
           return View("GraficosRSMS","_Layout");
        }


        [HttpPost]
        [Authorize]
        public ActionResult Load2()
        {
            //CATEGORIZACIÓN
            int seg = Convert.ToInt32(Request.Params["seguridad"]);
            int med = Convert.ToInt32(Request.Params["medioambiente"]);
            int sal = Convert.ToInt32(Request.Params["salud"]);  
            int ain = Convert.ToInt32(Request.Params["ainsegura"]);
            int cin = Convert.ToInt32(Request.Params["cinsegura"]);
            int eve = Convert.ToInt32(Request.Params["evento"]);          
            int lev = Convert.ToInt32(Request.Params["leve"]);
            int mod = Convert.ToInt32(Request.Params["moderado"]);
            int ina = Convert.ToInt32(Request.Params["inaceptable"]);
            //ANTECEDENTES
            int inf = Convert.ToInt32(Request.Params["informante"]);
            int anonimo = Convert.ToInt32(Request.Params["anonimo"]);

            DateTime fec = Convert.ToDateTime(Request.Params["fecharsms1"]);
            DateTime? cum = null;
            int hol = Convert.ToInt32(Request.Params["holding"]);
            int neg = Convert.ToInt32(Request.Params["negocio"]);
            int ger = Convert.ToInt32(Request.Params["gerencia"]);
            int are = Convert.ToInt32(Request.Params["area"]);
            int res = Convert.ToInt32(Request.Params["responsable1"]);
            string emp = Request.Params["empresa"];
            string nom = Request.Params["nombre_informante"];
            string nho = Request.Params["holding_nombre"];
            string nne = Request.Params["negocio_nombre"];
            string nge = Request.Params["gerencia_nombre"];
            string nar = Request.Params["area_nombre"];
            string obs = Request.Params["observado_nombre"];
            //DESCRIPCION
            int cie = Convert.ToInt32(Request.Params["cierre"]);
            string des = Request.Params["desc"];
        
            var count = Request.Files.AllKeys.Length;
            
            int addDays = 0;
            if (lev == 1)
            {
                addDays = 30;
            }else if (mod == 1)
            {
                addDays = 15;
            }else if (ina == 1)
            {
                addDays = 3;
            }
            
//            HttpPostedFileBase file = Request.Files["fileBase"];
            //var categorizacion = Request.Form["categorizacion"];
            
                var c = db.Categorizacion.Add(new Categorizacion
                {
                    seguridad = seg,
                    medio_ambiente = med,
                    salud = sal,
                    accion_insegura = ain,
                    condicion_insegura = cin,
                    evento_positivo = eve,
                    leve = lev,
                    moderado = mod,
                    inaceptable = ina,
                    created_at = DateTime.Now
                });
                db.SaveChanges();
                
                if (eve == 1)
                {
                    cum = null;
                    cie = 1;
                }else if (cie==1)
                {
                    cum = null;
                }else if (eve == 0 && cie == 0)
                {
                    cum = fec.AddDays(addDays);
                }
                var a = db.Antecedentes.Add(new Antecedentes
                {
                    informantes_id = inf,
                    anonimo = 0,
                    fecha = fec,
                    holding_id = hol,
                    negocio_id = neg,
                    gerencia_id = ger,
                    area_id = are,
                    responsable_id = res,
                    empresa_id = emp,
                    fecha_cumplimiento = cum,
                    created_at = DateTime.Now
                });
                db.SaveChanges();

                var d = db.Descripcion.Add(new Descripcion
                {
                    cierre = cie,
                    detalle = des,
                    created_at = DateTime.Now
                    });
                db.SaveChanges();
                

                string path = Server.MapPath("~/Content/Images/");
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
                    
                for (int i = 0; i < count; i++)
                {
                    var data = Request.Files["file" + i];
                    var time = DateTime.Now.Hour + "" + DateTime.Now.Minute + "" + DateTime.Now.Second;
                    var nombre = time + data.FileName;
                    data.SaveAs(path + Path.GetFileName(nombre));
                    db.Archivo.Add(new Archivo
                    {
                        id_descripcion = d.descripcion_id,
                        nombre_archivo = nombre,
                        created_at = DateTime.Now,
                    });
                    db.SaveChanges();
                }
                
                

                var respuesta = db.RespuestaRsms.Add(new RespuestaRsms
                {
                    categorizacion_id = c.categorizacion_id,
                    antecedentes_id = a.antecedentes_id,
                    descripcion_id = d.descripcion_id,
                    
                    seguridad1 = seg,
                    medio_ambiente1 = med,
                    salud1 = sal,
                    condicion_insegura1 = cin,
                    accion_insegura1 = ain,
                    evento_positivo1 = eve,
                    leve1 = lev,
                    moderado1 = mod,
                    inaceptable1 = ina,
                    fecha1 = fec,
                    informante_nombre = nom,
                    anonimo = anonimo,
                    holding_nombre = nho,
                    gerencia_nombre = nge,
                    negocio_nombre = nne,
                    area_nombre = nar,
                    created_at = DateTime.Now
                });
                db.SaveChanges();
                
                var user = db.Users.Where(x => x.Id == res.ToString()).FirstOrDefault();

                if (cie == 0 && eve == 0)
                {
                    var accion = "RSMS ";
                    if (lev == 1) accion += "Leve ";
                    if (mod == 1) accion += "Moderado ";
                    if (ina == 1) accion += "Grave ";
                    var seg1 = "";
                    var med1 = "";
                    var sal1 = "";
                    var cin1 = "";
                    var ain1 = "";
                    if (seg == 0) seg1 = "";
                    if (med == 0) med1 = "";
                    if (sal == 0) sal1 = "";
                    if (cin == 0) cin1 = "";
                    if (ain == 0) ain1 = "";
                    if (seg == 1) seg1 = "Seguridad";
                    if (med == 1) med1 = "Medio ambiente";
                    if (sal == 1) sal1 = "Salud";
                    if (cin == 1) cin1 = "Condicion insegura";
                    if (ain == 1) ain1 = "Accion insegura";
                    
                    
                    
                    var p = db.PlanAccion.Add(new Models.PlanAccion
                    {
                        accion = accion,
                        responsable_id = Convert.ToString(res),
                        cumplimiento = cum,
                        modulo = 2,
                        respuestarsms_id = respuesta.respuestarsms_id,
                        seguridad1 = seg1,
                        medio_ambiente1 = med1,
                        salud1 = sal1,
                        condicion_insegura1 = cin1,
                        accion_insegura1 = ain1,
                        created_at = DateTime.Now
                    });
                    db.SaveChanges();

                   
                    db.Notificaciones.Add(new Notificaciones
                    {
                        descripcion = "Tienes un nuevo plan de accion asociado a RSMS. ''"+ p.accion + "''",
                        modulo = 2,
                        cierre = 0,
                        plan_accion_id = p.plan_accion_id,
                        UserId = p.responsable_id,
                        created_at = DateTime.Now
                    });
                    db.SaveChanges(); 
                    try
                    {
                        MailMessage correo = new MailMessage();
                        correo.From = new MailAddress("paso.esmax@gmail.com"); //correo que usara la app
                        correo.To.Add(user.Email);
                        correo.Subject = "Tienes un nuevo plan de accion asociado";
                        correo.Body = user.UserName + ", <br/><br/> Tiene un nuevo plan de accion asociado a RSMS, Número de folio: " + p.plan_accion_id + " <br/><br/> Haz clic aqui para acceder: http://paso.esmax.cl/MplanAccion/ver/"+p.plan_accion_id+ "<br><br><br> Por favor no responder este correo.";
                        correo.IsBodyHtml = true;
                        correo.Priority = MailPriority.Normal;

                        System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient();
                        /*smtp.Host = "smtp.gmail.com";
                        smtp.Port = 25;
                        
                        smtp.UseDefaultCredentials = false;
                        smtp.Credentials = new NetworkCredential();*/
                        smtp.EnableSsl = true;

                        smtp.Send(correo);
                        ViewBag.Mensaje = "Mensaje enviado correctamente";
                    }
                    catch (Exception e)
                    {
                        ViewBag.Error = e.Message;
                    }
                }

                if (eve == 1 || cie==1)
                {
                    db.Notificaciones.Add(new Notificaciones
                    {
                        descripcion = "Se ha generado un RSMS con cierre inmediato",
                        modulo = 2,
                        cierre = 1,
                        rsms = respuesta.respuestarsms_id,
                        UserId = Convert.ToString(res),
                        created_at = DateTime.Now
                    });
                    db.SaveChanges();
                }
                
                

                return Json("ok");


        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult Load3()
        {
            //CATEGORIZACIÓN
            int seg = Convert.ToInt32(Request.Params["seguridad"]);
            int med = Convert.ToInt32(Request.Params["medioambiente"]);
            int sal = Convert.ToInt32(Request.Params["salud"]);
            int ain = Convert.ToInt32(Request.Params["ainsegura"]);
            int cin = Convert.ToInt32(Request.Params["cinsegura"]);
            int eve = Convert.ToInt32(Request.Params["evento"]);
            int lev = Convert.ToInt32(Request.Params["leve"]);
            int mod = Convert.ToInt32(Request.Params["moderado"]);
            int ina = Convert.ToInt32(Request.Params["inaceptable"]);
            //ANTECEDENTES
            int inf = Convert.ToInt32(Request.Params["informante"]);
            int anonimo = Convert.ToInt32(Request.Params["anonimo"]);

            DateTime fec = Convert.ToDateTime(Request.Params["fecharsms1"]);
            DateTime? cum = null;
            int hol = Convert.ToInt32(Request.Params["holding"]);
            int neg = Convert.ToInt32(Request.Params["negocio"]);
            int ger = Convert.ToInt32(Request.Params["gerencia"]);
            int are = Convert.ToInt32(Request.Params["area"]);
            int res = Convert.ToInt32(Request.Params["responsable1"]);
            string emp = Request.Params["empresa"];
            string nom = Request.Params["nombre_informante"];
            string nho = Request.Params["holding_nombre"];
            string nne = Request.Params["negocio_nombre"];
            string nge = Request.Params["gerencia_nombre"];
            string nar = Request.Params["area_nombre"];
            string obs = Request.Params["observado_nombre"];
            //DESCRIPCION
            int cie = Convert.ToInt32(Request.Params["cierre"]);
            string des = Request.Params["desc"];

            var count = Request.Files.AllKeys.Length;

            int addDays = 0;
            if (lev == 1)
            {
                addDays = 30;
            }
            else if (mod == 1)
            {
                addDays = 15;
            }
            else if (ina == 1)
            {
                addDays = 3;
            }

            //            HttpPostedFileBase file = Request.Files["fileBase"];
            //var categorizacion = Request.Form["categorizacion"];

            var c = db.Categorizacion.Add(new Categorizacion
            {
                seguridad = seg,
                medio_ambiente = med,
                salud = sal,
                accion_insegura = ain,
                condicion_insegura = cin,
                evento_positivo = eve,
                leve = lev,
                moderado = mod,
                inaceptable = ina,
                created_at = DateTime.Now
            });
            db.SaveChanges();

            if (eve == 1)
            {
                cum = null;
                cie = 1;
            }
            else if (cie == 1)
            {
                cum = null;
            }
            else if (eve == 0 && cie == 0)
            {
                cum = fec.AddDays(addDays);
            }
            var a = db.Antecedentes.Add(new Antecedentes
            {
                informantes_id = inf,
                anonimo = 0,
                fecha = fec,
                holding_id = hol,
                negocio_id = neg,
                gerencia_id = ger,
                area_id = are,
                responsable_id = res,
                empresa_id = emp,
                fecha_cumplimiento = cum,
                created_at = DateTime.Now
            });
            db.SaveChanges();

            var d = db.Descripcion.Add(new Descripcion
            {
                cierre = cie,
                detalle = des,
                created_at = DateTime.Now
            });
            db.SaveChanges();


            string path = Server.MapPath("~/Content/Images/");
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            for (int i = 0; i < count; i++)
            {
                var data = Request.Files["file" + i];
                var time = DateTime.Now.Hour + "" + DateTime.Now.Minute + "" + DateTime.Now.Second;
                var nombre = time + data.FileName;
                data.SaveAs(path + Path.GetFileName(nombre));
                db.Archivo.Add(new Archivo
                {
                    id_descripcion = d.descripcion_id,
                    nombre_archivo = nombre,
                    created_at = DateTime.Now,
                });
                db.SaveChanges();
            }



            var respuesta = db.RespuestaRsms.Add(new RespuestaRsms
            {
                categorizacion_id = c.categorizacion_id,
                antecedentes_id = a.antecedentes_id,
                descripcion_id = d.descripcion_id,

                seguridad1 = seg,
                medio_ambiente1 = med,
                salud1 = sal,
                condicion_insegura1 = cin,
                accion_insegura1 = ain,
                evento_positivo1 = eve,
                leve1 = lev,
                moderado1 = mod,
                inaceptable1 = ina,
                fecha1 = fec,
                informante_nombre = nom,
                anonimo = anonimo,
                holding_nombre = nho,
                gerencia_nombre = nge,
                negocio_nombre = nne,
                area_nombre = nar,
                created_at = DateTime.Now
            });
            db.SaveChanges();

            var user = db.Users.Where(x => x.Id == res.ToString()).FirstOrDefault();

            if (cie == 0 && eve == 0)
            {
                var accion = "RSMS ";
                if (lev == 1) accion += "Leve ";
                if (mod == 1) accion += "Moderado ";
                if (ina == 1) accion += "Grave ";
                var seg1 = "";
                var med1 = "";
                var sal1 = "";
                var cin1 = "";
                var ain1 = "";
                if (seg == 0) seg1 = "";
                if (med == 0) med1 = "";
                if (sal == 0) sal1 = "";
                if (cin == 0) cin1 = "";
                if (ain == 0) ain1 = "";
                if (seg == 1) seg1 = "Seguridad";
                if (med == 1) med1 = "Medio ambiente";
                if (sal == 1) sal1 = "Salud";
                if (cin == 1) cin1 = "Condicion insegura";
                if (ain == 1) ain1 = "Accion insegura";



                var p = db.PlanAccion.Add(new Models.PlanAccion
                {
                    accion = accion,
                    responsable_id = Convert.ToString(res),
                    cumplimiento = cum,
                    modulo = 2,
                    respuestarsms_id = respuesta.respuestarsms_id,
                    seguridad1 = seg1,
                    medio_ambiente1 = med1,
                    salud1 = sal1,
                    condicion_insegura1 = cin1,
                    accion_insegura1 = ain1,
                    created_at = DateTime.Now
                });
                db.SaveChanges();


                db.Notificaciones.Add(new Notificaciones
                {
                    descripcion = "Tienes un nuevo plan de accion asociado a RSMS. ''" + p.accion + "''",
                    modulo = 2,
                    cierre = 0,
                    plan_accion_id = p.plan_accion_id,
                    UserId = p.responsable_id,
                    created_at = DateTime.Now
                });
                db.SaveChanges();
                try
                {
                    MailMessage correo = new MailMessage();
                    correo.From = new MailAddress("paso.esmax@gmail.com"); //correo que usara la app
                    correo.To.Add(user.Email);
                    correo.Subject = "Tienes un nuevo plan de accion asociado";
                    correo.Body = user.UserName + ", <br/><br/> Tiene un nuevo plan de accion asociado a RSMS, Número de folio: " + p.plan_accion_id + " <br/><br/> Haz clic aqui para acceder: http://paso.esmax.cl/MplanAccion/ver/" + p.plan_accion_id + "<br><br><br> Por favor no responder este correo.";
                    correo.IsBodyHtml = true;
                    correo.Priority = MailPriority.Normal;

                    SmtpClient smtp = new SmtpClient();
                    /*smtp.Host = "smtp.gmail.com";
                    smtp.Port = 25;
                    smtp.EnableSsl = true;
                    smtp.UseDefaultCredentials = true;
                    string sCuentaCorreo = "paso.esmax@gmail.com";
                    string sPasswordCorreo = "esmax2019";
                    smtp.Credentials = new System.Net.NetworkCredential(sCuentaCorreo, sPasswordCorreo);*/

                    smtp.Send(correo);
                    ViewBag.Mensaje = "Mensaje enviado correctamente";
                }
                catch (Exception e)
                {
                    ViewBag.Error = e.Message;
                }
            }

            if (eve == 1 || cie == 1)
            {
                db.Notificaciones.Add(new Notificaciones
                {
                    descripcion = "Se ha generado un RSMS con cierre inmediato",
                    modulo = 2,
                    cierre = 1,
                    rsms = respuesta.respuestarsms_id,
                    UserId = Convert.ToString(res),
                    created_at = DateTime.Now
                });
                db.SaveChanges();
            }



            return Json("ok");


        }

        string cs = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        
        [HttpPost]
        [Authorize]
        public JsonResult getSeg(string desde, string hasta)
        {
            List<datos> lst = new List<datos>();
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                string query1 = @"select seguridad1, COUNT(seguridad1) as total from RespuestaRsms Where fecha1 >= @desde and fecha1 <= @hasta and seguridad1=1 group by seguridad1"; 
                SqlCommand com = new SqlCommand(query1, con);
                com.Parameters.AddWithValue("@desde", desde);
                com.Parameters.AddWithValue("@hasta", hasta);
                SqlDataReader rdr = com.ExecuteReader();
                while (rdr.Read())
                {
                    lst.Add(new datos
                    {
                        dato = Convert.ToInt32(rdr["seguridad1"] is DBNull ? 0: rdr["seguridad1"]),
                        total = Convert.ToInt32(rdr["total"] is DBNull ? 0: rdr["total"])
                        
                    });
                }
                return Json(lst);
            } 
        }

        [HttpPost]
        [Authorize]
        public JsonResult getMed(string desde, string hasta)
        {
            List<datos> lst = new List<datos>();
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                string query1 = @"select medio_ambiente1, COUNT(medio_ambiente1) as total from RespuestaRsms Where fecha1 >= @desde and fecha1 <= @hasta and medio_ambiente1=1 group by medio_ambiente1"; 
                SqlCommand com = new SqlCommand(query1, con);
                com.Parameters.AddWithValue("@desde", desde);
                com.Parameters.AddWithValue("@hasta", hasta);
                SqlDataReader rdr = com.ExecuteReader();
                while (rdr.Read())
                {
                    lst.Add(new datos
                    {
                        dato = Convert.ToInt32(rdr["medio_ambiente1"] is DBNull ? 0: rdr["medio_ambiente1"]),
                        total = Convert.ToInt32(rdr["total"] is DBNull ? 0: rdr["total"])
                        
                    });
                }
                return Json(lst);
            } 
        }
        
        [HttpPost]
        [Authorize]
        public JsonResult getSal(string desde, string hasta)
        {
            List<datos> lst = new List<datos>();
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                string query1 = @"select salud1, COUNT(salud1) as total from RespuestaRsms Where fecha1 >= @desde and fecha1 <= @hasta and salud1=1 group by salud1"; 
                SqlCommand com = new SqlCommand(query1, con);
                com.Parameters.AddWithValue("@desde", desde);
                com.Parameters.AddWithValue("@hasta", hasta);
                SqlDataReader rdr = com.ExecuteReader();
                while (rdr.Read())
                {
                    lst.Add(new datos
                    {
                        dato = Convert.ToInt32(rdr["salud1"] is DBNull ? 0: rdr["salud1"]),
                        total = Convert.ToInt32(rdr["total"] is DBNull ? 0: rdr["total"])
                        
                    });
                }
                return Json(lst);
            } 
        }
        
        [HttpPost]
        [Authorize]
        public JsonResult getAin(string desde, string hasta)
        {
            List<datos> lst = new List<datos>();
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                string query1 = @"select accion_insegura1, COUNT(accion_insegura1) as total from RespuestaRsms Where fecha1 >= @desde and fecha1 <= @hasta and accion_insegura1=1 group by accion_insegura1"; 
                SqlCommand com = new SqlCommand(query1, con);
                com.Parameters.AddWithValue("@desde", desde);
                com.Parameters.AddWithValue("@hasta", hasta);
                SqlDataReader rdr = com.ExecuteReader();
                while (rdr.Read())
                {
                    lst.Add(new datos
                    {
                        dato = Convert.ToInt32(rdr["accion_insegura1"] is DBNull ? 0: rdr["accion_insegura1"]),
                        total = Convert.ToInt32(rdr["total"] is DBNull ? 0: rdr["total"])
                        
                    });
                }
                return Json(lst);
            } 
        }
        
        [HttpPost]
        [Authorize]
        public JsonResult getCin(string desde, string hasta)
        {
            List<datos> lst = new List<datos>();
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                string query1 = @"select condicion_insegura1, COUNT(condicion_insegura1) as total from RespuestaRsms Where fecha1 >= @desde and fecha1 <= @hasta and condicion_insegura1=1 group by condicion_insegura1"; 
                SqlCommand com = new SqlCommand(query1, con);
                com.Parameters.AddWithValue("@desde", desde);
                com.Parameters.AddWithValue("@hasta", hasta);
                SqlDataReader rdr = com.ExecuteReader();
                while (rdr.Read())
                {
                    lst.Add(new datos
                    {
                        dato = Convert.ToInt32(rdr["condicion_insegura1"] is DBNull ? 0: rdr["condicion_insegura1"]),
                        total = Convert.ToInt32(rdr["total"] is DBNull ? 0: rdr["total"])
                        
                    });
                }
                return Json(lst);
            } 
        }
        
        [HttpPost]
        [Authorize]
        public JsonResult getEve(string desde, string hasta)
        {
            List<datos> lst = new List<datos>();
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                string query1 = @"select evento_positivo1, COUNT(evento_positivo1) as total from RespuestaRsms Where fecha1 >= @desde and fecha1 <= @hasta and evento_positivo1=1 group by evento_positivo1"; 
                SqlCommand com = new SqlCommand(query1, con);
                com.Parameters.AddWithValue("@desde", desde);
                com.Parameters.AddWithValue("@hasta", hasta);
                SqlDataReader rdr = com.ExecuteReader();
                while (rdr.Read())
                {
                    lst.Add(new datos
                    {
                        dato = Convert.ToInt32(rdr["evento_positivo1"] is DBNull ? 0: rdr["evento_positivo1"]),
                        total = Convert.ToInt32(rdr["total"] is DBNull ? 0: rdr["total"])
                        
                    });
                }
                return Json(lst);
            } 
        }
        
        [HttpPost]
        [Authorize]
        public JsonResult getLev(string desde, string hasta)
        {
            List<datos> lst = new List<datos>();
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                string query1 = @"select leve1, COUNT(leve1) as total from RespuestaRsms Where fecha1 >= @desde and fecha1 <= @hasta and leve1=1 group by leve1"; 
                SqlCommand com = new SqlCommand(query1, con);
                com.Parameters.AddWithValue("@desde", desde);
                com.Parameters.AddWithValue("@hasta", hasta);
                SqlDataReader rdr = com.ExecuteReader();
                while (rdr.Read())
                {
                    lst.Add(new datos
                    {
                        dato = Convert.ToInt32(rdr["leve1"] is DBNull ? 0: rdr["leve1"]),
                        total = Convert.ToInt32(rdr["total"] is DBNull ? 0: rdr["total"])
                        
                    });
                }
                return Json(lst);
            } 
        }
        
        [HttpPost]
        [Authorize]
        public JsonResult getMod(string desde, string hasta)
        {
            List<datos> lst = new List<datos>();
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                string query1 = @"select moderado1, COUNT(moderado1) as total from RespuestaRsms Where fecha1 >= @desde and fecha1 <= @hasta and moderado1=1 group by moderado1"; 
                SqlCommand com = new SqlCommand(query1, con);
                com.Parameters.AddWithValue("@desde", desde);
                com.Parameters.AddWithValue("@hasta", hasta);
                SqlDataReader rdr = com.ExecuteReader();
                while (rdr.Read())
                {
                    lst.Add(new datos
                    {
                        dato = Convert.ToInt32(rdr["moderado1"] is DBNull ? 0: rdr["moderado1"]),
                        total = Convert.ToInt32(rdr["total"] is DBNull ? 0: rdr["total"])
                        
                    });
                }
                return Json(lst);
            } 
        }
        
        [HttpPost]
        [Authorize]
        public JsonResult getIna(string desde, string hasta)
        {
            List<datos> lst = new List<datos>();
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                string query1 = @"select inaceptable1, COUNT(inaceptable1) as total from RespuestaRsms Where fecha1 >= @desde and fecha1 <= @hasta and inaceptable1=1 group by inaceptable1"; 
                SqlCommand com = new SqlCommand(query1, con);
                com.Parameters.AddWithValue("@desde", desde);
                com.Parameters.AddWithValue("@hasta", hasta);
                SqlDataReader rdr = com.ExecuteReader();
                while (rdr.Read())
                {
                    lst.Add(new datos
                    {
                        dato = Convert.ToInt32(rdr["inaceptable1"] is DBNull ? 0: rdr["inaceptable1"]),
                        total = Convert.ToInt32(rdr["total"] is DBNull ? 0: rdr["total"])
                        
                    });
                }
                return Json(lst);
            } 
        }
        [HttpPost]
        [Authorize]
        public JsonResult getRespuestaHolding(string desde, string hasta)
        {
            List<niveles> lst = new List<niveles>();
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                string query = @"select holding_nombre, COUNT(holding_nombre) as total from RespuestaRsms where fecha1 >= @desde and fecha1 <= @hasta group by holding_nombre"; 
                SqlCommand com = new SqlCommand(query, con);
                
                com.Parameters.AddWithValue("@desde", desde);
                com.Parameters.AddWithValue("@hasta", hasta);
                SqlDataReader rdr = com.ExecuteReader();
                while (rdr.Read())
                {
                    lst.Add(new niveles
                    {
                        nombre = Convert.ToString(rdr["holding_nombre"] is DBNull ? 0: rdr["holding_nombre"]),
                        total = Convert.ToInt32(rdr["total"] is DBNull ? 0: rdr["total"]),
                        
                    });
                }
                return Json(lst);
            } 
        }
        
        [HttpPost]
        [Authorize]
        public JsonResult getRespuestaNegocio(string desde, string hasta)
        {
            List<niveles> lst = new List<niveles>();
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                string query = @"select negocio_nombre, COUNT(negocio_nombre) as total from RespuestaRsms where fecha1 >= @desde and fecha1 <= @hasta group by negocio_nombre"; 
                SqlCommand com = new SqlCommand(query, con);
                com.Parameters.AddWithValue("@desde", desde);
                com.Parameters.AddWithValue("@hasta", hasta);
                SqlDataReader rdr = com.ExecuteReader();
                while (rdr.Read())
                {
                    lst.Add(new niveles
                    {
                        nombre = Convert.ToString(rdr["negocio_nombre"] is DBNull ? 0: rdr["negocio_nombre"]),
                        total = Convert.ToInt32(rdr["total"] is DBNull ? 0: rdr["total"]),
                    });
                }
                return Json(lst);
            } 
        }
        
        [HttpPost]
        [Authorize]
        public JsonResult getRespuestaGerencia(string desde, string hasta)
        {
            List<niveles> lst = new List<niveles>();
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                string query = @"select gerencia_nombre, COUNT(gerencia_nombre) as total from RespuestaRsms where fecha1 >= @desde and fecha1 <= @hasta group by gerencia_nombre"; 
                SqlCommand com = new SqlCommand(query, con);
                com.Parameters.AddWithValue("@desde", desde);
                com.Parameters.AddWithValue("@hasta", hasta);
                SqlDataReader rdr = com.ExecuteReader();
                while (rdr.Read())
                {
                    lst.Add(new niveles
                    {
                        nombre = Convert.ToString(rdr["gerencia_nombre"] is DBNull ? 0: rdr["gerencia_nombre"]),
                        total = Convert.ToInt32(rdr["total"] is DBNull ? 0: rdr["total"]),
                        
                    });
                }
                return Json(lst);
            } 
        }
      
        [HttpPost]
        [Authorize]
        public JsonResult getRespuestaArea(string desde, string hasta)
        {
            List<niveles> lst = new List<niveles>();
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                string query = @"select area_nombre, COUNT(area_nombre) as total from RespuestaRsms where fecha1 >= @desde and fecha1 <= @hasta group by area_nombre"; 
                SqlCommand com = new SqlCommand(query, con);
                com.Parameters.AddWithValue("@desde", desde);
                com.Parameters.AddWithValue("@hasta", hasta);
                SqlDataReader rdr = com.ExecuteReader();
                while (rdr.Read())
                {
                    lst.Add(new niveles
                    {
                        nombre = Convert.ToString(rdr["area_nombre"] is DBNull ? 0: rdr["area_nombre"]),
                        total = Convert.ToInt32(rdr["total"] is DBNull ? 0: rdr["total"]),
                        
                    });
                }
                return Json(lst);
            } 
        }
        
        [HttpPost]
        [Authorize]
        public JsonResult getUsuariosResponden(string desde, string hasta)
        {
            List<informante> lst = new List<informante>();
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                string query = @"select informante_nombre, COUNT(informante_nombre) as total from RespuestaRsms where fecha1 >= @desde and fecha1 <= @hasta group by informante_nombre order by total"; 
                SqlCommand com = new SqlCommand(query, con);
                com.Parameters.AddWithValue("@desde", desde);
                com.Parameters.AddWithValue("@hasta", hasta);
                SqlDataReader rdr = com.ExecuteReader();
                while (rdr.Read())
                {
                    lst.Add(new informante
                    {
                        nombre_user = Convert.ToString(rdr["informante_nombre"] is DBNull ? 0: rdr["informante_nombre"]),
                        total = Convert.ToInt32(rdr["total"] is DBNull ? 0: rdr["total"]),
                        
                    });
                }
                return Json(lst);
            } 
        }

    }
}