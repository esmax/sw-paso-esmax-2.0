using System.Linq;
using System.Web.Mvc;
using Esmax.Models;

namespace Esmax.Controllers
{
    public class GerenciaController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
                       
       [HttpGet]
       [Authorize]
       public ActionResult GetGerencia()
       {
           db.Configuration.ProxyCreationEnabled = false;
           var gerencias = db.Gerencias.ToList();
           return Json(gerencias, JsonRequestBehavior.AllowGet);
       } 
    }
}