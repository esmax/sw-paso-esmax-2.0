namespace Esmax.Context
{
    public class OdtTareas
    {
        public int OdtTareas_id { get; set; }
        public int odts_id { get; set; }
        public int tareas_id { get; set; }
        public string tareas_titulo { get; set; }
    }
    
    public class OdtSubTareas
    {
        public int OdtSubTareas_id { get; set; }
        public int odts_id { get; set; }
        public int sub_tareas_id { get; set; }
        public int tareas_id { get; set; }
        public string sub_tareas_nombre { get; set; }
    }
    
    public class NivelesOdt
    {
        public int NivelesOdts_id { get; set; }
        public int odts_id { get; set; }
        public int niveles_id { get; set; }
        public string niveles_titulo { get; set; }
        public string niveles_codigo { get; set; }
    }
    
    public class OdtSubNiveles
    {
        public int OdtSubNiveles_id { get; set; }
        public int odts_id { get; set; }
        public int sub_niveles_id { get; set; }
        public int niveles_id { get; set; }
        public string sub_niveles_nombre { get; set; }
        public string sub_niveles_codigo { get; set; }
    }
    public class Users
    {
        public int idUser { get; set; }
        public string UserName { get; set; }
        public string PhoneNumber { get; set; }
        public int holding_id { get; set; }
        public int negocio_id { get; set; }
        public int gerencia_id { get; set; }
        public int area_id { get; set; }
        public string Cargo { get; set; }
        public string Rol { get; set; }
        public string Email { get; set; }
    }

    public class Holding
    {
        public int holding_id { get; set; }
        public string holding_nombre { get; set; }
    }

    public class Negocio
    {
        public int negocio_id { get; set; }
        public string negocio_nombre { get; set; }
        public int holding_id { get; set; }

    }

    public class Gerencia
    {
        public int gerencia_id { get; set; }
        public string gerencia_nombre { get; set; }
        public int negocio_id { get; set; }

    }

    public class Area
    {
        public int area_id { get; set; }
        public string area_nombre { get; set; }
        public int gerencia_id { get; set; }
        public string responsable_id { get; set; }
        public string UserName { get; set; }
        public string Cargo { get; set; }

    }

}