﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Esmax.Models
{
    public class Tareas
    {
        [Key]
        public int tareas_id { get; set; }
        public string tareas_titulo { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? created_at { get; set; }

        public virtual ICollection<Sub_tareas> Sub_tareas { get; set; }
        public virtual ICollection<OdtTareas> OdtTareas { get; set; }
        public virtual ICollection<TareasCondiciones> TareasCondiciones { get; set; }
    }
}