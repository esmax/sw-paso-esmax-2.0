using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Esmax.Models
{
    public class RespuestaOdt
    {
        [Key]
        public int respuesta_odt_id { get; set; }
        public int odts_id { get; set; }
        public string holdingName { get; set; }
        public string odtName { get; set; }
        public string categoriaName { get; set; }
        public int categoriaId { get; set; }
        public string negocioName { get; set; }
        public string gerenciaName { get; set; }
        public string areaName { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString ="{0:dd/MM/yyyy}", ApplyFormatInEditMode =true)]
        public DateTime? created_at { get; set; } 
        
        public virtual ICollection<DatosGenerales> DatosGenerales { get; set; }
        public virtual ICollection<PlanAccion> PlanAccion { get; set; }
        public virtual ICollection<Condiciones> Condiciones { get; set; }
        public virtual ICollection<Analisis> Analisis { get; set; }
        public virtual ICollection<Observaciones> Observaciones { get; set; }
        public virtual ICollection<TareasCondiciones> TareasCondiciones { get; set; }
    }
}