﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Esmax.Models
{
    public class Niveles
    {
        [Key]
        public int niveles_id { get; set; }
        public string niveles_titulo { get; set; }
        public string niveles_codigo { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? created_at { get; set; }

        public virtual ICollection<Sub_niveles> Sub_niveles { get; set; }
        public virtual ICollection<NivelesOdts> NivelesOdts { get; set; }
    }
}