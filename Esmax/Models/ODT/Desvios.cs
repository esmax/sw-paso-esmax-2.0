using System.ComponentModel.DataAnnotations;

namespace Esmax.Models
{
    public class Desvios
    {
        [Key]
        public int desvios_id { get; set; }
        public int NivelesOdts_id { get; set; }
        public int niveles_id { get; set; }
        public int respuesta_odt_id { get; set; }
        
    }
}