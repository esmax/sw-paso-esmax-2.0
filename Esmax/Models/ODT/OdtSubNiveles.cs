using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Esmax.Models
{
    public class OdtSubNiveles
    {
        [Key]
        public int OdtSubNiveles_id { get; set; }
        public int odts_id { get; set; }
        public int sub_niveles_id { get; set; }

        public virtual Odts Odts { get; set; }
        public virtual Sub_niveles Sub_niveles { get; set; }
        public virtual Observaciones Observaciones { get; set; }
        public virtual ICollection<PlanAccion_OdtSubNiveles> PlanAccionOdtSubNiveles { get; set; }
    }
}