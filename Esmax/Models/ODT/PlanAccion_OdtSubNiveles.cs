using System.ComponentModel.DataAnnotations;

namespace Esmax.Models
{
    public class PlanAccion_OdtSubNiveles
    {
       
            [Key]
            public int PlanAccion_OdtSubNiveles_id { get; set; }
            public int OdtSubNiveles_id { get; set; }
            public string sub_niveles_codigo { get; set; }
            public string sub_niveles_nombre { get; set; }
            public int plan_accion_id { get; set; }
            public int NivelesOdts_id { get; set; }

            public OdtSubNiveles OdtSubNiveles { get; set; }
            public PlanAccion PlanAccion { get; set; }
            public NivelesOdts NivelesOdts { get; set; }
        } 
}