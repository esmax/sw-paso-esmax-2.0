using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Esmax.Models.MplanAccion;


namespace Esmax.Models
{
    public class PlanAccion
    {
        [Key]
        public int plan_accion_id { get; set; }
        public int? actividad_id { get; set; }
        public int? NivelesOdts_id { get; set; }
        public int? causa_raiz_id { get; set; }
        public string accion { get; set; }
        public string responsable_id { get; set; }
        public int? respuesta_odt_id { get; set; }
        public int modulo { get; set; }
        public int? respuestarsms_id { get; set; }
        public string seguridad1 { get; set; } 
        public string medio_ambiente1 { get; set; } 
        public string salud1 { get; set; } 
        public string accion_insegura1 { get; set; } 
        public string condicion_insegura1 { get; set; } 
        public string descripcionCierre { get; set; } 
        public int? respuesta_rta_id { get; set; }
        public string tipo_accion_rta { get; set; }
        public string responsable_nombre { get; set; }
        public string tipo_evidencia { get; set; }
        
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString ="{0:dd/MM/yyyy}", ApplyFormatInEditMode =true)]
        public DateTime? cumplimiento { get; set; } 
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString ="{0:dd/MM/yyyy}", ApplyFormatInEditMode =true)]
        public DateTime? created_at { get; set; }

        public string estado { get; set; } = "habilitado";
        public int? CheckListId { get; set; }
        
        public virtual RespuestaOdt RespuestaOdt { get; set; }
        public virtual CausaRaiz CausaRaiz { get; set; }
        public virtual ICollection<Observaciones> Observaciones { get; set; }
        public virtual NivelesOdts NivelesOdts { get; set; }
        public virtual ApplicationUser User { get; set; }
        public virtual ICollection<PlanAccion_OdtSubNiveles> PlanAccionOdtSubNiveles { get; set; }
        public virtual ICollection<TareasPlanAccion> TareasPlanAccions { get; set; }
        public virtual ICollection<Notificaciones> Notificaciones { get; set; }
        public virtual ICollection<EvidenciaTarea> EvidenciaTarea { get; set; }

    }
}