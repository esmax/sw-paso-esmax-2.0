using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Esmax.Models
{
    public class NivelesOdts
    {
        [Key]
        public int NivelesOdts_id { get; set; }
        public int odts_id { get; set; }
        public int niveles_id { get; set; }

        public virtual Odts Odts { get; set; }
        public virtual Niveles Niveles { get; set; }
        public virtual ICollection<PlanAccion> PlanAccion { get; set; }
        public virtual ICollection<PlanAccion_OdtSubNiveles> PlanAccionOdtSubNiveles { get; set; }

    }
}