using System.Collections;
using System.ComponentModel.DataAnnotations;

namespace Esmax.Models
{
    public class OdtTareas
    {
        [Key]
        public int OdtTareas_id { get; set; }
        public int odts_id { get; set; }
        public int tareas_id { get; set; }

        public virtual Odts Odts { get; set; }
        public virtual Tareas Tareas { get; set; }
        
        public virtual ICollection TareasCondiciones { get; set; }
    }
}