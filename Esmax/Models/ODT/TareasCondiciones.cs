using System.ComponentModel.DataAnnotations;

namespace Esmax.Models
{
    public class TareasCondiciones
    {
        [Key]
        public int tareasCondiciones_id { get; set; }
        public int OdtTareas_id { get; set; }
        public int tareas_id { get; set; }
        public string tareas_titulo { get; set; }
        public int respuesta_odt_id { get; set; }
        
        public virtual OdtTareas OdtTareas { get; set; }
        public virtual Tareas Tareas { get; set; }
        public virtual RespuestaOdt RespuestaOdt { get; set; }
    }
}