using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Esmax.Models.RSMS;
using Esmax.Models.RTA;

namespace Esmax.Models
{
    public class Area
    {
        [Key]
        public int area_id { get; set; }
        public string area_nombre { get; set; } 
        public int? responsable_id { get; set; } 
        public int gerencia_id { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString ="{0:dd/MM/yyyy}", ApplyFormatInEditMode =true)]
        public DateTime? created_at { get; set; } 


        public virtual Gerencia Gerencia { get; set; }
        public virtual ICollection<ApplicationUser> ApplicationUser { get; set; }
        public virtual ICollection<Antecedentes> Antecedentes { get; set; }
        public virtual ICollection<DatosGenerales> DatosGenerales { get; set; }
        public virtual ICollection<etapa1> etapa1 { get; set; }
    }
}