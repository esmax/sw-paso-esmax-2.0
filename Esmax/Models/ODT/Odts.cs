﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Esmax.Models
{
    public class Odts
    {
        [Key]
        public int odts_id { get; set; }
        public string odts_tipo { get; set; }
        public string odts_descripcion { get; set; }
        public string estado { get; set; } = "habilitado";
        public int categorias_id { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? created_at { get; set; }

        public virtual Categorias                 Categorias { get; set; }
        public virtual ICollection<OdtTareas>     OdtTareas { get; set; }
        public virtual ICollection<NivelesOdts>   NivelesOdts { get; set; }
        public virtual ICollection<OdtSubNiveles> OdtSubNiveles { get; set; }
        public virtual ICollection<OdtSubTareas>  OdtSubTareas { get; set; }
        public virtual ICollection<RespuestaOdt>  RespuestaOdts { get; set; }
    }
}