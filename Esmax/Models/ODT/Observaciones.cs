using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Esmax.Models
{
    public class Observaciones
    {
        [Key]
        public int observaciones_id { get; set; }
        public int OdtSubNiveles_id { get; set; }  
        public int NA { get; set; }
        public int correcto { get; set; }
        public string sub_niveles_codigo { get; set; }
        public string sub_niveles_nombre { get; set; }
        public int sub_niveles_id { get; set; }
        public int niveles_id { get; set; }
        public int desvio { get; set; }
        public string comentario { get; set; }
        public int respuesta_odt_id { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString ="{0:dd/MM/yyyy}", ApplyFormatInEditMode =true)]
        public DateTime? created_at { get; set; } 
        
        public virtual RespuestaOdt RespuestaOdt { get; set; }
        public virtual PlanAccion PlanAccion { get; set; }
        public virtual ICollection<OdtSubNiveles> OdtSubNiveles { get; set; }
    
    }
}