using System;
using System.ComponentModel.DataAnnotations;

namespace Esmax.Models
{
    public class Analisis
    {
        [Key]
        public int analisis_id { get; set; }
        public string comentarios { get; set; } 
        public string conclusiones { get; set; }
        public int retroalimentacion { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString ="{0:dd/MM/yyyy}", ApplyFormatInEditMode =true)]
        public DateTime? fecha_sesion { get; set; }
        public int respuesta_odt_id { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString ="{0:dd/MM/yyyy}", ApplyFormatInEditMode =true)]
        public DateTime? created_at { get; set; } 

        public virtual RespuestaOdt RespuestaOdt { get; set; }
    }
}