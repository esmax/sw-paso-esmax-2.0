﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Esmax.Models
{
    public class Sub_niveles
    {
        [Key]
        public int sub_niveles_id { get; set; }
        public string sub_niveles_nombre { get; set; }
        
        public string sub_niveles_codigo { get; set; }
        public int niveles_id { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? created_at { get; set; }

        public virtual Niveles Niveles { get; set; }
        
        public virtual ICollection<OdtSubNiveles> OdtSubNiveles { get; set; }
    }
}