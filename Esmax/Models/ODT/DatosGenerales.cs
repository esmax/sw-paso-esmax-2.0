using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Esmax.Controllers;

namespace Esmax.Models
{
    public class DatosGenerales
    {
        [Key]
        public int datos_generales_id { get; set; }
        public int holding_id { get; set; }  
        public int negocio_id { get; set; }
        public int gerencia_id { get; set; }
        public int area_id { get; set; }
        public string observador_id { get; set; }
        public int anonimo { get; set; }
        public string observado_id { get; set; }
        public int respuesta_odt_id { get; set; }
        public string ec { get; set; }
        public string contratista { get; set; }
        public string empresa { get; set; }
        public string trabajo { get; set; }
        public string hora { get; set; }
        public string turno { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString ="{0:dd/MM/yyyy}", ApplyFormatInEditMode =true)]
        public DateTime? fecha { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString ="{0:dd/MM/yyyy}", ApplyFormatInEditMode =true)]
        public DateTime? created_at { get; set; } 
        
        public virtual RespuestaOdt RespuestaOdt { get; set; }
        public virtual Holding Holding { get; set; }
        public virtual Negocio Negocio { get; set; }
        public virtual Gerencia Gerencia { get; set; }
        public virtual Area Area { get; set; }
    }
}