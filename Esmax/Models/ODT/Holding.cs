using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Esmax.Models.RSMS;
using Esmax.Models.RTA;

namespace Esmax.Models
{
    public class Holding
    {
        [Key]
        public int holding_id { get; set; }
        public string holding_nombre { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString ="{0:dd/MM/yyyy}", ApplyFormatInEditMode =true)]
        public DateTime? created_at { get; set; } 
        public virtual ICollection<ApplicationUser> ApplicationUser { get; set; }
        public virtual ICollection<Negocio> Negocios { get; set; }
        public virtual ICollection<Antecedentes> Antecedentes { get; set; }
        public virtual ICollection<DatosGenerales> DatosGenerales { get; set; }
        public virtual ICollection<etapa1> etapa1 { get; set; }


    }

}