using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Esmax.Models.CheckList
{
    public class Actividades
    {
        [Key]
        public int ActividadId { get; set; }
        public string ActividadNombre { get; set; }
        public int CheckListId { get; set; }
        public DateTime? created_at { get; set; }
        public int Porcentaje { get; set; }
        
        
        public virtual CheckList CheckList { get; set; }
        public virtual ICollection<Preguntas> Preguntas { get; set; }
    }
}