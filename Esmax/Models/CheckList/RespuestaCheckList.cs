using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Esmax.Models.CheckList
{
    public class RespuestaCheckList
    {
        [Key]
        public int respuestaCheckListId { get; set; }
        public string holding_nombre { get; set; }
        public string negocio_nombre { get; set; }
        public string gerencia_nombre { get; set; }
        public string area_nombre { get; set; }
        public string observador_nombre { get; set; }
        public string CheckListNombre { get; set; }
        public string categorias_nombre { get; set; }
        public int datosGeneralesCheckListId { get; set; }
        public int CheckListId { get; set; }
        public DateTime? created_at { get; set; }
        public double Porcentaje { get; set; }
        
        public virtual DatosGeneralesCheckList DatosGeneralesCheckList { get; set; }
        public virtual ICollection<rActividades> rActividades { get; set; }
    }
}