using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Esmax.Models.CheckList
{
    public class rActividades
    {
        [Key]
        public int rActividadesId { get; set; }
        public int ActividadId { get; set; }
        public int PreguntaId { get; set; }
        public int na { get; set; }
        public int si { get; set; }
        public int no { get; set; }
        public string comentario { get; set; }
        public string pregunta { get; set; }
        public int respuestaCheckListId { get; set; }
        public DateTime? created_at { get; set; }
        
        public virtual RespuestaCheckList RespuestaCheckLists { get; set; }
    }
}