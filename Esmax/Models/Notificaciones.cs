using System;
using System.ComponentModel.DataAnnotations;

namespace Esmax.Models
{
    public class Notificaciones
    {
        [Key]
        public int notificacionesId { get; set; }
        public string descripcion { get; set; }
        public int modulo { get; set; }
        public int? cierre { get; set; }
        public int? plan_accion_id { get; set; }
        public int? rsms { get; set; }
        public int? rta { get; set; }
        public string UserId { get; set; }
        public string estado { get; set; } = "noLeido";
        public DateTime? created_at { get; set; }
        
        public virtual ApplicationUser User { get; set; }
        public virtual PlanAccion PlanAccion { get; set; }
    }
}