using System;
using System.ComponentModel.DataAnnotations;

namespace Esmax.Models.RSMS
{
    public class RespuestaRsms
    {
        [Key]
        public int respuestarsms_id { get; set; }
        public int categorizacion_id { get; set; }
        public int antecedentes_id { get; set; }
        public int descripcion_id { get; set; }
        public int? seguridad1 { get; set; }  
        public int? medio_ambiente1 { get; set; }  
        public int? salud1 { get; set; }
        public int? accion_insegura1 { get; set; }
        public int? condicion_insegura1 { get; set; }
        public int? evento_positivo1 { get; set; }
        public int? leve1 { get; set; }
        public int? moderado1 { get; set; }
        public int? inaceptable1 { get; set; }
        public DateTime? fecha1 { get; set; }
        public string informante_nombre { get; set; }
        public int anonimo { get; set; }
        public string holding_nombre { get; set; }
        public string negocio_nombre { get; set; }
        public string gerencia_nombre { get; set; }
        public string area_nombre { get; set; }
        
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? created_at { get; set; }
        
        public virtual Categorizacion Categorizacion { get; set; }
        public virtual Antecedentes Antecedentes { get; set; }
        public virtual Descripcion Descripcion { get; set; }
    }
}