using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Net.Mime;

namespace Esmax.Models.RSMS
{
    public class Antecedentes
    {
        [Key]
        public int antecedentes_id { get; set; }
        public int informantes_id { get; set; }
        public int anonimo { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? fecha { get; set; }
        public int holding_id { get; set; }
        public int negocio_id { get; set; }
        public int gerencia_id { get; set; }
        public int area_id { get; set; }
        public int responsable_id { get; set; }
        public string empresa_id { get; set; }
        public string contratista { get; set; }
        
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? fecha_cumplimiento { get; set; }
        
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? created_at { get; set; }
        
        public virtual ApplicationUser ApplicationUser { get; set; }
        public virtual Holding Holding { get; set; }
        public virtual Negocio Negocio { get; set; }
        public virtual Gerencia Gerencia { get; set; }
        public virtual Area Area { get; set; }
        public virtual ICollection<RespuestaRsms> RespuestaRsms { get; set; }
        
    }
}