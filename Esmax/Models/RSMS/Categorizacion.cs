using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Esmax.Models.RSMS
{
    public class Categorizacion
    {
        [Key]
        public int categorizacion_id { get; set; }
        public int? seguridad { get; set; }  
        public int? medio_ambiente { get; set; }  
        public int? salud { get; set; }
        public int? accion_insegura { get; set; }
        public int? condicion_insegura { get; set; }
        public int? evento_positivo { get; set; }
        public int? leve { get; set; }
        public int? moderado { get; set; }
        public int? inaceptable { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString ="{0:dd/MM/yyyy}", ApplyFormatInEditMode =true)]
        public DateTime? created_at { get; set; } 

        public virtual ICollection<RespuestaRsms> RespuestaRsms { get; set; }
    }
}