using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Esmax.Models.RSMS
{
    public class Descripcion
    {
        [Key]
        public int descripcion_id { get; set; }
        public int cierre { get; set; }
        public string detalle { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? created_at { get; set; }
        
        public virtual ICollection<Archivo> Archivo { get; set; }
        public virtual ICollection<RespuestaRsms> RespuestaRsms { get; set; }
    }
}