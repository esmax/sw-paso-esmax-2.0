using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Esmax.Models.RSMS
{
    public class Archivo
    {
        [Key]
        public int id_archivo {get;set;}
        public string nombre_archivo { get; set; }
        public int id_descripcion { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? created_at { get; set; }
        public virtual Descripcion Descripcion { get; set; }
    }
}