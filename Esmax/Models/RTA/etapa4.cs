using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Esmax.Models.RTA
{
    public class etapa4
    {
        [Key]
        public int etapa4_id { get; set; }
        public string responsable5_id { get; set; }
        public string responsable5_nombre { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString ="{0:dd/MM/yyyy}", ApplyFormatInEditMode =true)]
        public DateTime? created_at { get; set; }
        public virtual ICollection<RespuestaRTA> RespuestaRTA { get; set; }
        public virtual ApplicationUser ApplicationUser { get; set; }
    }
}