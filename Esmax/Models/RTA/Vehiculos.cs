using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Esmax.Models.RTA
{
    public class Vehiculos
    {
        [Key]
        public int vehiculos_id { get; set; }
        public string vehiculos_tipo { get; set; } 
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString ="{0:dd/MM/yyyy}", ApplyFormatInEditMode =true)]
        public DateTime? created_at { get; set; }
        public virtual ICollection<etapa1> etapa1 { get; set; }
    }
}