using System;
using System.ComponentModel.DataAnnotations;

namespace Esmax.Models.RTA
{
    public class RespuestaRTA
    {
        [Key] 
        public int id_rta { get; set; }
        public int? etapa1_id { get; set; }
        public int? etapa2_id { get; set; }
        public int? etapa3_id { get; set; }
        public int? etapa4_id { get; set; }
        public int? etapa5_id { get; set; }
        public int? etapa6_id { get; set; }
        public string responsable_actual { get; set; }
        public int? responsable_actual_id { get; set; }
        public string etapa_actual { get; set; }
        public string estado { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString ="{0:dd/MM/yyyy}", ApplyFormatInEditMode =true)]
        public DateTime? created_at { get; set; }
        
        public virtual etapa1 etapa1 { get; set; }
        public virtual etapa2 etapa2 { get; set; }
        public virtual etapa3 etapa3 { get; set; }
        public virtual etapa4 etapa4 { get; set; }
        public virtual etapa5 etapa5 { get; set; }
        public virtual etapa6 etapa6 { get; set; }
    }
}