using System;
using System.ComponentModel.DataAnnotations;
using Esmax.Models.RSMS;

namespace Esmax.Models.RTA
{
    public class ArchivoRTA
    {
        [Key]
        public int id_archivo {get;set;}
        public string nombre_archivo { get; set; }
        public int id_etapa1 { get; set; }
        public int id_etapa3 { get; set; }
        public int id_etapa4 { get; set; }
        public int id_etapa5 { get; set; }
        public int id_etapa6 { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? created_at { get; set; }
        public virtual etapa1 Etapa1 { get; set; }
        public virtual etapa3 Etapa3 { get; set; }
    }
}