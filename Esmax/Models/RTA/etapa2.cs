using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Esmax.Models.RTA
{
    public class etapa2
    {
        [Key]
        public int etapa2_id { get; set; }
        public string gravedad { get; set; }
        public string medio_ambiente_gravedad { get; set; }
        public string patrimonio_gravedad { get; set; }
        public string procesos_productos_gravedad { get; set; }
        public string personas_gravedad { get; set; }
        public string responsable3_id { get; set; }
        public string responsable3_nombre { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString ="{0:dd/MM/yyyy}", ApplyFormatInEditMode =true)]
        public DateTime? created_at { get; set; }
        public virtual ICollection<RespuestaRTA> RespuestaRTA { get; set; }
        public virtual ICollection<comision> Comision { get; set; }
        public virtual ApplicationUser ApplicationUser { get; set; }
    }
}