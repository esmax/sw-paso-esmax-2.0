using System;
using System.ComponentModel.DataAnnotations;
using Esmax.Models.RSMS;

namespace Esmax.Models.RTA
{
    public class Causas_basicas
    {
        [Key] public int Causas_basicas_id { get; set; }
        public string Tipo_causa_basica { get; set; }
        public string Causa { get; set; }
        public string Descripcion { get; set; }
        public int etapa3_id { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? created_at { get; set; }

        public etapa3 etapa3 { get; set; }
    }
}