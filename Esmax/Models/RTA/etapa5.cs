using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Esmax.Models.RTA
{
    public class etapa5
    {
        [Key]
        public int etapa5_id { get; set; }
        public int costos_parada { get; set; }
        public int costos_acciones_inmediatas { get; set; }
        public int costos_acciones_correctivas { get; set; }
        public int costos_acciones_preventivas { get; set; }
        public int otros_costos { get; set; }
        public int total_costos { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString ="{0:dd/MM/yyyy}", ApplyFormatInEditMode =true)]
        public DateTime plazo_verificacion { get; set; }
        public int responsable6_id { get; set; }
        public string responsable6_nombre { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString ="{0:dd/MM/yyyy}", ApplyFormatInEditMode =true)]
        public DateTime? created_at { get; set; }
        public virtual ICollection<RespuestaRTA> RespuestaRTA { get; set; }
        public virtual ApplicationUser ApplicationUser { get; set; }
        
    }
}