using System;
using System.ComponentModel.DataAnnotations;

namespace Esmax.Models.RTA
{
    public class comision
    {
        [Key]
        public int id_comision { get; set; }
        public int etapa2_id { get; set; }
        public int usuario_id { get; set; }
        public string usuario_nombre { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString ="{0:dd/MM/yyyy}", ApplyFormatInEditMode =true)]
        public DateTime? created_at { get; set; }
        
        public virtual etapa2 etapa2 { get; set; }
        public virtual ApplicationUser ApplicationUser { get; set; }
    }
}