using System;
using System.ComponentModel.DataAnnotations;

namespace Esmax.Models.MplanAccion
{
    public class EvidenciaTarea
    {
        [Key] 
        public int EvidenciaTareaId { get; set; }
        public string archivo { get; set; }
        public int datosEvidenciaId { get; set; }
        public DateTime? created_at { get; set; }
        
        
        public virtual DatosEvidencia DatosEvidencia { get; set; }
        

    }
}