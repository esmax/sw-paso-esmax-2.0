﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Security.Claims;
using System.Threading.Tasks;
using Esmax.Models.CheckList;
using Esmax.Models.MplanAccion;
using Esmax.Models.RSMS;
using Esmax.Models.RTA;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Esmax.Models
{
    // Para agregar datos de perfil del usuario, agregue más propiedades a su clase ApplicationUser. Visite https://go.microsoft.com/fwlink/?LinkID=317594 para obtener más información.
    public class ApplicationUser : IdentityUser
    {    
        public string Usuario { get; set; }
      
        [StringLength(10)] 
        [RegularExpression("^(\\d{1}|\\d{2})(\\d{3}\\d{3}-)([a-zA-Z]{1}$|\\d{1}$)", ErrorMessage = "El formato del Rut debe ser 999999999")]
        public string Rut { get; set; }
        public string Estado { get; set; } = "habilitado";
        public string Cargo { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? Fecha_Ingreso { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? Fecha_Nacimiento { get; set; }
        
        public string Rol { get; set; }
        public string Chave { get; set; }
        public int? holding_id { get; set; }
        public int? negocio_id { get; set; }
        public int? gerencia_id { get; set; }
        public int? area_id { get; set; }
        public string Ubicacion { get; set; }
        public string Gerencia { get; set; }
        
        public virtual Holding Holding { get; set; }
        public virtual Negocio Negocio { get; set; }
        public virtual Gerencia Gerencia_id { get; set; }
        public virtual Area Area { get; set; }
        
        
        public virtual ICollection<PlanAccion> PlanAccion { get; set; }
        public virtual ICollection<Antecedentes> Antecedentes { get; set; }
        public virtual ICollection<Notificaciones> Notificaciones { get; set; }
        public virtual ICollection<etapa1> etapa1 { get; set; }
        public virtual ICollection<etapa2> etapa2 { get; set; }
        public virtual ICollection<etapa3> etapa3 { get; set; }
        public virtual ICollection<etapa4> etapa4 { get; set; }
        public virtual ICollection<etapa5> etapa5 { get; set; }
        public virtual ICollection<etapa6> etapa6 { get; set; }
        public virtual ICollection<Causas_basicas> Causas_basicas { get; set; }
        public virtual ICollection<Causas_inmediatas> Causas_inmediatas { get; set; }
        public virtual ICollection<Acciones_corr_prev> Acciones_corr_prev { get; set; }
        
        public virtual ICollection<comision> comision { get; set; }
        

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Tenga en cuenta que el valor de authenticationType debe coincidir con el definido en CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Agregar aquí notificaciones personalizadas de usuario
            return userIdentity;
        }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        
        


        public DbSet<Categorias> Categorias { get; set; }
        public DbSet<Tareas> Tareas { get; set; }
        public DbSet<Odts> Odts { get; set; }
        public DbSet<Niveles> Niveles { get; set; }
        public DbSet<Sub_tareas> SubTareas { get; set; }
        public DbSet<Sub_niveles> SubNiveles { get; set; }
        public DbSet<OdtTareas> OdtTareas { get; set; }
        public DbSet<OdtSubTareas> OdtSubTareas { get; set; }
        public DbSet<NivelesOdts> NivelesOdts { get; set; }
        public DbSet<OdtSubNiveles> OdtSubNiveles { get; set; }
        public DbSet<Holding> Holdings { get; set; }
        public DbSet<Negocio> Negocios { get; set; }
        public DbSet<Gerencia> Gerencias { get; set; }
        public DbSet<Area> Areas { get; set; }
        public DbSet<Analisis> Analisis { get; set; }
        public DbSet<Condiciones> Condiciones { get; set; }
        public DbSet<DatosGenerales> DatosGenerales { get; set; }
        public DbSet<Observaciones> Observaciones { get; set; }
        public DbSet<PlanAccion> PlanAccion { get; set; }
        public DbSet<CausaRaiz> CausaRaiz { get; set; }
        public DbSet<RespuestaOdt> RespuestaOdts { get; set; }
        public DbSet<Categorizacion> Categorizacion { get; set; }
        public DbSet<RespuestaRsms> RespuestaRsms { get; set; }

        public DbSet<Antecedentes> Antecedentes { get; set; }
        public DbSet<Descripcion> Descripcion { get; set; }
        public DbSet<Archivo> Archivo { get; set; }
        public DbSet<PlanAccion_OdtSubNiveles> PlanAccion_OdtSubNiveles { get; set; }
        public DbSet<Desvios> Desvios { get; set; }
        public DbSet<TareasCondiciones> TareasCondiciones { get; set; }
        public DbSet<TareasPlanAccion> TareasPlanAccions { get; set; }
        public DbSet<EvidenciaTarea> EvidenciaTareas { get; set; }
        public DbSet<Notificaciones> Notificaciones { get; set; }
        public DbSet<CheckList.CheckList> CheckList { get; set; }
        public DbSet<Actividades> Actividades { get; set; }
        public DbSet<Preguntas> Preguntas { get; set; }
        public DbSet<CategoriasC> CategoriasC { get; set; }
        public DbSet<RespuestaCheckList> RespuestaCheckList { get; set; }
        public DbSet<DatosGeneralesCheckList> DatosGeneralesCheckList { get; set; }
        public DbSet<rActividades> rActividades { get; set; }
        public DbSet<DatosEvidencia> DatosEvidencia { get; set; }
        
        public DbSet<Instalaciones> Instalaciones { get; set; }
        public DbSet<Vehiculos> Vehiculos { get; set; }
        public DbSet<ArchivoRTA> ArchivoRta { get; set; }
        public DbSet<etapa1> etapa1 { get; set; }
        public DbSet<etapa2> etapa2 { get; set; }
        public DbSet<etapa3> etapa3 { get; set; }
        public DbSet<etapa4> etapa4 { get; set; }
        public DbSet<etapa5> etapa5 { get; set; }
        public DbSet<etapa6> etapa6 { get; set; }
        public DbSet<comision> comision { get; set; }
        public DbSet<RespuestaRTA> RespuestaRta { get; set; }
        public DbSet<Causas_basicas> CausasBasicas { get; set; }
        public DbSet<Causas_inmediatas> Causasinmediatas { get; set; }
        public DbSet<Acciones_corr_prev> Acciones_corr_prev { get; set; }
        


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Entity<Tareas>()
                .HasMany(e => e.Sub_tareas);
            modelBuilder.Entity<OdtTareas>().HasRequired(x => x.Odts).WithMany().HasForeignKey(x => x.odts_id);
            modelBuilder.Entity<OdtTareas>().HasRequired(x => x.Tareas).WithMany().HasForeignKey(x => x.tareas_id);
            modelBuilder.Entity<OdtSubTareas>().HasRequired(x => x.Odts).WithMany().HasForeignKey(x => x.odts_id);
            modelBuilder.Entity<NivelesOdts>().HasRequired(x => x.Odts).WithMany().HasForeignKey(x => x.odts_id);
            modelBuilder.Entity<OdtSubNiveles>().HasRequired(x => x.Odts).WithMany().HasForeignKey(x => x.odts_id);

            modelBuilder.Entity<IdentityUserRole>()
                .HasKey(r => new {r.UserId, r.RoleId})
                .ToTable("AspNetUserRoles");

            modelBuilder.Entity<IdentityUserLogin>()
                .HasKey(l => new {l.LoginProvider, l.ProviderKey, l.UserId})
                .ToTable("AspNetUserLogins");

            modelBuilder.Entity<IdentityUserClaim>()
                .HasKey(c => new {c.Id, c.UserId, c.ClaimType, c.ClaimValue})
                .ToTable("AspNetUserClaims");
        }
    }
}